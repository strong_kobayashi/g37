/**
 */
package G37.G37_Device;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see G37.G37_Device.G37_DeviceFactory
 * @model kind="package"
 * @generated
 */
public interface G37_DevicePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "G37_Device";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://homepage.univie.ac.at/sergeyb73/G37D";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "G37D_Namespace";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	G37_DevicePackage eINSTANCE = G37.G37_Device.impl.G37_DevicePackageImpl.init();

	/**
	 * The meta object id for the '{@link G37.G37_Device.impl.G37DImpl <em>G37D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Device.impl.G37DImpl
	 * @see G37.G37_Device.impl.G37_DevicePackageImpl#getG37D()
	 * @generated
	 */
	int G37D = 0;

	/**
	 * The feature id for the '<em><b>Device</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37D__DEVICE = 0;

	/**
	 * The number of structural features of the '<em>G37D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37D_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>G37D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37D_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Device.impl.DeviceImpl <em>Device</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Device.impl.DeviceImpl
	 * @see G37.G37_Device.impl.G37_DevicePackageImpl#getDevice()
	 * @generated
	 */
	int DEVICE = 1;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE__PROPERTIES = 0;

	/**
	 * The number of structural features of the '<em>Device</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Device</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Device.impl.DevicePropertyImpl <em>Device Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Device.impl.DevicePropertyImpl
	 * @see G37.G37_Device.impl.G37_DevicePackageImpl#getDeviceProperty()
	 * @generated
	 */
	int DEVICE_PROPERTY = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_PROPERTY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Prop Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_PROPERTY__PROP_VALUE = 1;

	/**
	 * The feature id for the '<em><b>Prop Value Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_PROPERTY__PROP_VALUE_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Kind Of Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_PROPERTY__KIND_OF_DATA = 3;

	/**
	 * The feature id for the '<em><b>Is Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_PROPERTY__IS_READ_ONLY = 4;

	/**
	 * The number of structural features of the '<em>Device Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_PROPERTY_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Device Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Device.KindOfData <em>Kind Of Data</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Device.KindOfData
	 * @see G37.G37_Device.impl.G37_DevicePackageImpl#getKindOfData()
	 * @generated
	 */
	int KIND_OF_DATA = 3;


	/**
	 * Returns the meta object for class '{@link G37.G37_Device.G37D <em>G37D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>G37D</em>'.
	 * @see G37.G37_Device.G37D
	 * @generated
	 */
	EClass getG37D();

	/**
	 * Returns the meta object for the containment reference '{@link G37.G37_Device.G37D#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Device</em>'.
	 * @see G37.G37_Device.G37D#getDevice()
	 * @see #getG37D()
	 * @generated
	 */
	EReference getG37D_Device();

	/**
	 * Returns the meta object for class '{@link G37.G37_Device.Device <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Device</em>'.
	 * @see G37.G37_Device.Device
	 * @generated
	 */
	EClass getDevice();

	/**
	 * Returns the meta object for the containment reference list '{@link G37.G37_Device.Device#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see G37.G37_Device.Device#getProperties()
	 * @see #getDevice()
	 * @generated
	 */
	EReference getDevice_Properties();

	/**
	 * Returns the meta object for class '{@link G37.G37_Device.DeviceProperty <em>Device Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Device Property</em>'.
	 * @see G37.G37_Device.DeviceProperty
	 * @generated
	 */
	EClass getDeviceProperty();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Device.DeviceProperty#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see G37.G37_Device.DeviceProperty#getName()
	 * @see #getDeviceProperty()
	 * @generated
	 */
	EAttribute getDeviceProperty_Name();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Device.DeviceProperty#getPropValue <em>Prop Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prop Value</em>'.
	 * @see G37.G37_Device.DeviceProperty#getPropValue()
	 * @see #getDeviceProperty()
	 * @generated
	 */
	EAttribute getDeviceProperty_PropValue();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Device.DeviceProperty#getPropValueType <em>Prop Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prop Value Type</em>'.
	 * @see G37.G37_Device.DeviceProperty#getPropValueType()
	 * @see #getDeviceProperty()
	 * @generated
	 */
	EAttribute getDeviceProperty_PropValueType();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Device.DeviceProperty#getKindOfData <em>Kind Of Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind Of Data</em>'.
	 * @see G37.G37_Device.DeviceProperty#getKindOfData()
	 * @see #getDeviceProperty()
	 * @generated
	 */
	EAttribute getDeviceProperty_KindOfData();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Device.DeviceProperty#isIsReadOnly <em>Is Read Only</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Read Only</em>'.
	 * @see G37.G37_Device.DeviceProperty#isIsReadOnly()
	 * @see #getDeviceProperty()
	 * @generated
	 */
	EAttribute getDeviceProperty_IsReadOnly();

	/**
	 * Returns the meta object for enum '{@link G37.G37_Device.KindOfData <em>Kind Of Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Kind Of Data</em>'.
	 * @see G37.G37_Device.KindOfData
	 * @generated
	 */
	EEnum getKindOfData();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	G37_DeviceFactory getG37_DeviceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link G37.G37_Device.impl.G37DImpl <em>G37D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Device.impl.G37DImpl
		 * @see G37.G37_Device.impl.G37_DevicePackageImpl#getG37D()
		 * @generated
		 */
		EClass G37D = eINSTANCE.getG37D();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference G37D__DEVICE = eINSTANCE.getG37D_Device();

		/**
		 * The meta object literal for the '{@link G37.G37_Device.impl.DeviceImpl <em>Device</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Device.impl.DeviceImpl
		 * @see G37.G37_Device.impl.G37_DevicePackageImpl#getDevice()
		 * @generated
		 */
		EClass DEVICE = eINSTANCE.getDevice();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEVICE__PROPERTIES = eINSTANCE.getDevice_Properties();

		/**
		 * The meta object literal for the '{@link G37.G37_Device.impl.DevicePropertyImpl <em>Device Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Device.impl.DevicePropertyImpl
		 * @see G37.G37_Device.impl.G37_DevicePackageImpl#getDeviceProperty()
		 * @generated
		 */
		EClass DEVICE_PROPERTY = eINSTANCE.getDeviceProperty();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEVICE_PROPERTY__NAME = eINSTANCE.getDeviceProperty_Name();

		/**
		 * The meta object literal for the '<em><b>Prop Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEVICE_PROPERTY__PROP_VALUE = eINSTANCE.getDeviceProperty_PropValue();

		/**
		 * The meta object literal for the '<em><b>Prop Value Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEVICE_PROPERTY__PROP_VALUE_TYPE = eINSTANCE.getDeviceProperty_PropValueType();

		/**
		 * The meta object literal for the '<em><b>Kind Of Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEVICE_PROPERTY__KIND_OF_DATA = eINSTANCE.getDeviceProperty_KindOfData();

		/**
		 * The meta object literal for the '<em><b>Is Read Only</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEVICE_PROPERTY__IS_READ_ONLY = eINSTANCE.getDeviceProperty_IsReadOnly();

		/**
		 * The meta object literal for the '{@link G37.G37_Device.KindOfData <em>Kind Of Data</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Device.KindOfData
		 * @see G37.G37_Device.impl.G37_DevicePackageImpl#getKindOfData()
		 * @generated
		 */
		EEnum KIND_OF_DATA = eINSTANCE.getKindOfData();

	}

} //G37_DevicePackage
