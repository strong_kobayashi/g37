/**
 */
package G37.G37_Device;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Kind Of Data</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see G37.G37_Device.G37_DevicePackage#getKindOfData()
 * @model
 * @generated
 */
public enum KindOfData implements Enumerator {
	/**
	 * The '<em><b>SYSTEM DATA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYSTEM_DATA_VALUE
	 * @generated
	 * @ordered
	 */
	SYSTEM_DATA(0, "SYSTEM_DATA", "SYSTEM_DATA"),

	/**
	 * The '<em><b>MAINTENANCE DATA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MAINTENANCE_DATA_VALUE
	 * @generated
	 * @ordered
	 */
	MAINTENANCE_DATA(1, "MAINTENANCE_DATA", "MAINTENANCE_DATA"),

	/**
	 * The '<em><b>USAGE DATA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USAGE_DATA_VALUE
	 * @generated
	 * @ordered
	 */
	USAGE_DATA(2, "USAGE_DATA", "USAGE_DATA"),

	/**
	 * The '<em><b>SENSOR DATA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SENSOR_DATA_VALUE
	 * @generated
	 * @ordered
	 */
	SENSOR_DATA(3, "SENSOR_DATA", "SENSOR_DATA"),

	/**
	 * The '<em><b>OPERATIONAL SETTING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATIONAL_SETTING_VALUE
	 * @generated
	 * @ordered
	 */
	OPERATIONAL_SETTING(4, "OPERATIONAL_SETTING", "OPERATIONAL_SETTING"),

	/**
	 * The '<em><b>RESERVED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESERVED_VALUE
	 * @generated
	 * @ordered
	 */
	RESERVED(99, "RESERVED", "RESERVED");

	/**
	 * The '<em><b>SYSTEM DATA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYSTEM_DATA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SYSTEM_DATA_VALUE = 0;

	/**
	 * The '<em><b>MAINTENANCE DATA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MAINTENANCE_DATA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAINTENANCE_DATA_VALUE = 1;

	/**
	 * The '<em><b>USAGE DATA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USAGE_DATA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int USAGE_DATA_VALUE = 2;

	/**
	 * The '<em><b>SENSOR DATA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SENSOR_DATA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SENSOR_DATA_VALUE = 3;

	/**
	 * The '<em><b>OPERATIONAL SETTING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATIONAL_SETTING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OPERATIONAL_SETTING_VALUE = 4;

	/**
	 * The '<em><b>RESERVED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESERVED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RESERVED_VALUE = 99;

	/**
	 * An array of all the '<em><b>Kind Of Data</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final KindOfData[] VALUES_ARRAY =
		new KindOfData[] {
			SYSTEM_DATA,
			MAINTENANCE_DATA,
			USAGE_DATA,
			SENSOR_DATA,
			OPERATIONAL_SETTING,
			RESERVED,
		};

	/**
	 * A public read-only list of all the '<em><b>Kind Of Data</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<KindOfData> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Kind Of Data</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static KindOfData get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			KindOfData result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Kind Of Data</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static KindOfData getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			KindOfData result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Kind Of Data</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static KindOfData get(int value) {
		switch (value) {
			case SYSTEM_DATA_VALUE: return SYSTEM_DATA;
			case MAINTENANCE_DATA_VALUE: return MAINTENANCE_DATA;
			case USAGE_DATA_VALUE: return USAGE_DATA;
			case SENSOR_DATA_VALUE: return SENSOR_DATA;
			case OPERATIONAL_SETTING_VALUE: return OPERATIONAL_SETTING;
			case RESERVED_VALUE: return RESERVED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private KindOfData(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //KindOfData
