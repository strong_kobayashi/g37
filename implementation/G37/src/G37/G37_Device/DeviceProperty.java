/**
 */
package G37.G37_Device;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Device Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Device.DeviceProperty#getName <em>Name</em>}</li>
 *   <li>{@link G37.G37_Device.DeviceProperty#getPropValue <em>Prop Value</em>}</li>
 *   <li>{@link G37.G37_Device.DeviceProperty#getPropValueType <em>Prop Value Type</em>}</li>
 *   <li>{@link G37.G37_Device.DeviceProperty#getKindOfData <em>Kind Of Data</em>}</li>
 *   <li>{@link G37.G37_Device.DeviceProperty#isIsReadOnly <em>Is Read Only</em>}</li>
 * </ul>
 *
 * @see G37.G37_Device.G37_DevicePackage#getDeviceProperty()
 * @model
 * @generated
 */
public interface DeviceProperty extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see G37.G37_Device.G37_DevicePackage#getDeviceProperty_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link G37.G37_Device.DeviceProperty#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Prop Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prop Value</em>' attribute.
	 * @see #setPropValue(Object)
	 * @see G37.G37_Device.G37_DevicePackage#getDeviceProperty_PropValue()
	 * @model required="true"
	 * @generated
	 */
	Object getPropValue();

	/**
	 * Sets the value of the '{@link G37.G37_Device.DeviceProperty#getPropValue <em>Prop Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prop Value</em>' attribute.
	 * @see #getPropValue()
	 * @generated
	 */
	void setPropValue(Object value);

	/**
	 * Returns the value of the '<em><b>Prop Value Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prop Value Type</em>' attribute.
	 * @see #setPropValueType(Class)
	 * @see G37.G37_Device.G37_DevicePackage#getDeviceProperty_PropValueType()
	 * @model required="true"
	 * @generated
	 */
	Class<?> getPropValueType();

	/**
	 * Sets the value of the '{@link G37.G37_Device.DeviceProperty#getPropValueType <em>Prop Value Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prop Value Type</em>' attribute.
	 * @see #getPropValueType()
	 * @generated
	 */
	void setPropValueType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Kind Of Data</b></em>' attribute.
	 * The literals are from the enumeration {@link G37.G37_Device.KindOfData}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind Of Data</em>' attribute.
	 * @see G37.G37_Device.KindOfData
	 * @see #setKindOfData(KindOfData)
	 * @see G37.G37_Device.G37_DevicePackage#getDeviceProperty_KindOfData()
	 * @model required="true"
	 * @generated
	 */
	KindOfData getKindOfData();

	/**
	 * Sets the value of the '{@link G37.G37_Device.DeviceProperty#getKindOfData <em>Kind Of Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind Of Data</em>' attribute.
	 * @see G37.G37_Device.KindOfData
	 * @see #getKindOfData()
	 * @generated
	 */
	void setKindOfData(KindOfData value);

	/**
	 * Returns the value of the '<em><b>Is Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Read Only</em>' attribute.
	 * @see #setIsReadOnly(boolean)
	 * @see G37.G37_Device.G37_DevicePackage#getDeviceProperty_IsReadOnly()
	 * @model required="true"
	 * @generated
	 */
	boolean isIsReadOnly();

	/**
	 * Sets the value of the '{@link G37.G37_Device.DeviceProperty#isIsReadOnly <em>Is Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Read Only</em>' attribute.
	 * @see #isIsReadOnly()
	 * @generated
	 */
	void setIsReadOnly(boolean value);

} // DeviceProperty
