/**
 */
package G37.G37_Device.impl;

import G37.G37_Device.DeviceProperty;
import G37.G37_Device.G37_DevicePackage;
import G37.G37_Device.KindOfData;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Device Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Device.impl.DevicePropertyImpl#getName <em>Name</em>}</li>
 *   <li>{@link G37.G37_Device.impl.DevicePropertyImpl#getPropValue <em>Prop Value</em>}</li>
 *   <li>{@link G37.G37_Device.impl.DevicePropertyImpl#getPropValueType <em>Prop Value Type</em>}</li>
 *   <li>{@link G37.G37_Device.impl.DevicePropertyImpl#getKindOfData <em>Kind Of Data</em>}</li>
 *   <li>{@link G37.G37_Device.impl.DevicePropertyImpl#isIsReadOnly <em>Is Read Only</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DevicePropertyImpl extends MinimalEObjectImpl.Container implements DeviceProperty {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPropValue() <em>Prop Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropValue()
	 * @generated
	 * @ordered
	 */
	protected static final Object PROP_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPropValue() <em>Prop Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropValue()
	 * @generated
	 * @ordered
	 */
	protected Object propValue = PROP_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPropValueType() <em>Prop Value Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropValueType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> propValueType;

	/**
	 * The default value of the '{@link #getKindOfData() <em>Kind Of Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindOfData()
	 * @generated
	 * @ordered
	 */
	protected static final KindOfData KIND_OF_DATA_EDEFAULT = KindOfData.SYSTEM_DATA;

	/**
	 * The cached value of the '{@link #getKindOfData() <em>Kind Of Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindOfData()
	 * @generated
	 * @ordered
	 */
	protected KindOfData kindOfData = KIND_OF_DATA_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsReadOnly() <em>Is Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsReadOnly()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_READ_ONLY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsReadOnly() <em>Is Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsReadOnly()
	 * @generated
	 * @ordered
	 */
	protected boolean isReadOnly = IS_READ_ONLY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DevicePropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_DevicePackage.Literals.DEVICE_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_DevicePackage.DEVICE_PROPERTY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getPropValue() {
		return propValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPropValue(Object newPropValue) {
		Object oldPropValue = propValue;
		propValue = newPropValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE, oldPropValue, propValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Class<?> getPropValueType() {
		return propValueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPropValueType(Class<?> newPropValueType) {
		Class<?> oldPropValueType = propValueType;
		propValueType = newPropValueType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE_TYPE, oldPropValueType, propValueType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KindOfData getKindOfData() {
		return kindOfData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKindOfData(KindOfData newKindOfData) {
		KindOfData oldKindOfData = kindOfData;
		kindOfData = newKindOfData == null ? KIND_OF_DATA_EDEFAULT : newKindOfData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_DevicePackage.DEVICE_PROPERTY__KIND_OF_DATA, oldKindOfData, kindOfData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isIsReadOnly() {
		return isReadOnly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIsReadOnly(boolean newIsReadOnly) {
		boolean oldIsReadOnly = isReadOnly;
		isReadOnly = newIsReadOnly;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_DevicePackage.DEVICE_PROPERTY__IS_READ_ONLY, oldIsReadOnly, isReadOnly));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE_PROPERTY__NAME:
				return getName();
			case G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE:
				return getPropValue();
			case G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE_TYPE:
				return getPropValueType();
			case G37_DevicePackage.DEVICE_PROPERTY__KIND_OF_DATA:
				return getKindOfData();
			case G37_DevicePackage.DEVICE_PROPERTY__IS_READ_ONLY:
				return isIsReadOnly();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE_PROPERTY__NAME:
				setName((String)newValue);
				return;
			case G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE:
				setPropValue(newValue);
				return;
			case G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE_TYPE:
				setPropValueType((Class<?>)newValue);
				return;
			case G37_DevicePackage.DEVICE_PROPERTY__KIND_OF_DATA:
				setKindOfData((KindOfData)newValue);
				return;
			case G37_DevicePackage.DEVICE_PROPERTY__IS_READ_ONLY:
				setIsReadOnly((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE_PROPERTY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE:
				setPropValue(PROP_VALUE_EDEFAULT);
				return;
			case G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE_TYPE:
				setPropValueType((Class<?>)null);
				return;
			case G37_DevicePackage.DEVICE_PROPERTY__KIND_OF_DATA:
				setKindOfData(KIND_OF_DATA_EDEFAULT);
				return;
			case G37_DevicePackage.DEVICE_PROPERTY__IS_READ_ONLY:
				setIsReadOnly(IS_READ_ONLY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE_PROPERTY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE:
				return PROP_VALUE_EDEFAULT == null ? propValue != null : !PROP_VALUE_EDEFAULT.equals(propValue);
			case G37_DevicePackage.DEVICE_PROPERTY__PROP_VALUE_TYPE:
				return propValueType != null;
			case G37_DevicePackage.DEVICE_PROPERTY__KIND_OF_DATA:
				return kindOfData != KIND_OF_DATA_EDEFAULT;
			case G37_DevicePackage.DEVICE_PROPERTY__IS_READ_ONLY:
				return isReadOnly != IS_READ_ONLY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", propValue: ");
		result.append(propValue);
		result.append(", propValueType: ");
		result.append(propValueType);
		result.append(", kindOfData: ");
		result.append(kindOfData);
		result.append(", isReadOnly: ");
		result.append(isReadOnly);
		result.append(')');
		return result.toString();
	}

} //DevicePropertyImpl
