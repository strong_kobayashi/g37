/**
 */
package G37.G37_Device.impl;

import com.google.gson.*;

import G37.Auxiliary.*;
import G37.G37_Device.Device;
import G37.G37_Device.DeviceProperty;
import G37.G37_Device.G37D;
import G37.G37_Device.G37_DeviceFactory;
import G37.G37_Device.G37_DevicePackage;
import G37.G37_Device.util.ClassTypeAdapter;

import java.io.*;
import java.net.*;  // For the sockets server.
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>G37D</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Device.impl.G37DImpl#getDevice <em>Device</em>}</li>
 * </ul>
 *
 * @generated
 */
public class G37DImpl extends MinimalEObjectImpl.Container implements G37D {
	/**
	 * The cached value of the '{@link #getDevice() <em>Device</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevice()
	 * @generated
	 * @ordered
	 */
	protected Device device;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected G37DImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_DevicePackage.Literals.G37D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Device getDevice() {
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDevice(Device newDevice, NotificationChain msgs) {
		Device oldDevice = device;
		device = newDevice;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, G37_DevicePackage.G37D__DEVICE, oldDevice, newDevice);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDevice(Device newDevice) {
		if (newDevice != device) {
			NotificationChain msgs = null;
			if (device != null)
				msgs = ((InternalEObject)device).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - G37_DevicePackage.G37D__DEVICE, null, msgs);
			if (newDevice != null)
				msgs = ((InternalEObject)newDevice).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - G37_DevicePackage.G37D__DEVICE, null, msgs);
			msgs = basicSetDevice(newDevice, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_DevicePackage.G37D__DEVICE, newDevice, newDevice));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case G37_DevicePackage.G37D__DEVICE:
				return basicSetDevice(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_DevicePackage.G37D__DEVICE:
				return getDevice();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_DevicePackage.G37D__DEVICE:
				setDevice((Device)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_DevicePackage.G37D__DEVICE:
				setDevice((Device)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_DevicePackage.G37D__DEVICE:
				return device != null;
		}
		return super.eIsSet(featureID);
	}
	
	
	// This method accepts a file URI, opens it as a JSON array of DeviceProperty objects,
	// and creates a new object of the type Device with the properties from the JSON file.
	public Device createDeviceFromJSON(URI fileName) {
		G37_DeviceFactory deviceFactory = G37_DeviceFactory.eINSTANCE;
		Device dev = deviceFactory.createDevice();
		DeviceProperty devPropsBuffer[];
		
		try (Reader reader = new FileReader(fileName.toFileString())) {
			GsonBuilder gsonBuilder = new GsonBuilder();  
			gsonBuilder.setLenient();  // Allow comments, among other things.
			gsonBuilder.registerTypeAdapter(Class.class, new ClassTypeAdapter()); // See https://stackoverflow.com/questions/8119138/gson-not-parsing-class-variable
			Gson gson = gsonBuilder.create();
			
			// Populate the list of properties from the JSON file
			devPropsBuffer = gson.fromJson(reader, DevicePropertyImpl[].class);
			Resp.log("Found "+devPropsBuffer.length+" properties:");
			for (int i = 0; i < devPropsBuffer.length; i++) {
				dev.getProperties().add(devPropsBuffer[i]);
				System.out.printf("Property %d:\t%-20s\t%-35s\t%-30s\t%-20s\t%b\n",
									i,
									devPropsBuffer[i].getName(),
									devPropsBuffer[i].getPropValue(),
									devPropsBuffer[i].getPropValueType(),
									devPropsBuffer[i].getKindOfData(),
									devPropsBuffer[i].isIsReadOnly());
			}			
			
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		dev.setUptime(Long.parseLong(dev.peek(Const.Uptime, Const.EU))/1000); // Set the time of registration, in seconds.
		dev.updateFields();
		return dev;
	} // createDeviceFromJSON
	
	// This is is main loop of G37D.
	public void run(int portNumber) {
		
		String reply = "", inputLine;
		
		Resp.log("\nListening on port "+portNumber+"...");
		
		while (true) {  // Device's main loop.
			// Let the device update its fields and properties, e.g. update its uptime field or copy the values 
        	// from the hardware registers into properties like Temperature, Movement etc
        	getDevice().updateFields(); 
        	// Now see if there is a command and if yes, process it.
			try (
		            ServerSocket serverSocket = new ServerSocket(portNumber);
		            Socket clientSocket = serverSocket.accept();     
		            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);                   
		            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		        ) {
		            while ((inputLine = in.readLine()) != null) {
		            	String tokens[] = inputLine.split(Const.DivCh);
		            	String command = tokens[0]; // Get the command from the "head" of the input line.
		            	String propName = (tokens.length > 1) ? Resp.trimString(tokens[1], "'") : "";
		            	String arg2 = (tokens.length > 2) ? Resp.trimString(tokens[2], "'") : "";  // These two can be user type or new value, 
		            	String arg3 = (tokens.length > 3) ? Resp.trimString(tokens[3], "'") : "";  // depending on whether this is a peek or a poke.
		            	switch (command) {
		            		case "PING" :
		            			reply = Const.Alive;
		            			break;
		            		case "PEEK" :
		            			Resp.log("Received a command to PEEK at the property \"" + propName + "\"", true);
		            			reply = getDevice().peek(propName, arg2);
		            			break;
		            		case "POKE" : 
		            			Resp.log("Received a command to POKE the value \"" + arg2 +"\" into the property \"" + propName + "\"", true);
		            			reply = getDevice().poke(propName,  arg2,  arg3);
		            			// Poking into this property means updating firmware, hence the special treatment.
		            			if (propName.equals(Const.FirmwareBuffer) && reply.equals(Resp.response(R_CODE.OK))) getDevice().updateFirmware(arg2); 
		            			break;
		            		case "SHUTDOWN" :
		            			reply = "Received a SHUTDOWN command.  Shutting the device down...";
		            			Resp.log(reply);
		            			break;
		            		default :
		            			System.err.println(Resp.response(R_CODE.E_UNKNOWNCOMMAND, command));
		            	}
		                // Return result.
		                out.println(reply);
		                if (command.equals("SHUTDOWN")) {
		                	Resp.delay(2);
		                	System.exit(0);
		                }
		            }
		        } catch (IOException e) {
		            System.err.println("Exception caught when trying to listen on port " + portNumber + " or listening for a connection\n" + e.getMessage());
		        }			
		}
	} // Run
	
} //G37DImpl
