/**
 * 
 */
package G37.G37_Device.impl;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.emf.common.util.URI;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Device.G37D;
import G37.G37_Device.G37_DeviceFactory;

/**
 * The main (launcher) class of G37D
 * @author sergeyb
 *
 */
public class G37D_Main {

	public static void main(String args[]) {
		URI propJSONfileName = URI.createFileURI(Const.PropertiesJSONdefaultFileName);
		String pathToResponsesJSON = "";
		int portNumber = Const.DefaultDevicePortNumber;
		G37_DeviceFactory deviceFactory = G37_DeviceFactory.eINSTANCE;
		G37D g37d = deviceFactory.createG37D();  // The main object.
		
		Resp.log("=== G37 Device launched ===\n");		
		
		// Initialize the static "responses" array in class Resp.
		// If there is an argument that follows -R switch, use it as a path to responses JSON; otherwise, assume responses JSON in CWD. 	
		for (int i=0; i <= args.length-1; i++) {
			if (args[i].toUpperCase().equals("-R")) {
				pathToResponsesJSON = (args[++i] != null) ? args[i] : pathToResponsesJSON;
				break;
			}
		}
		
		InputStream inputStream = null;
		try {
			System.out.println("Opening resources file " + pathToResponsesJSON + "\\" + Const.ResponsesJSONdefaultFileName);
			URL resource = new URL(pathToResponsesJSON +  "\\" + Const.ResponsesJSONdefaultFileName);
			inputStream = resource.openConnection().getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Resp.responses = Resp.loadResponsesFromJSON(inputStream); 
		
		// Parse other arguments
		for (int i=0; i < args.length; i++) {
			switch (args[i].toUpperCase()) {
				case "-F" : // The argument after -F, if such switch exists, must be the path and file name of the properties JSON.
					propJSONfileName = (args[++i] != null) ? URI.createFileURI(args[i]) : propJSONfileName; 
					// If no "json" extension, add one.
					if (propJSONfileName.fileExtension() == null) propJSONfileName = propJSONfileName.appendFileExtension("json");
					System.out.println("Opening properties file " + propJSONfileName);
					break;
				case "-P" : // The argument after -P, if such switch exists, must be the port number.
					try {
						portNumber = (args[++i] != null) ? Integer.parseInt(args[i]) : portNumber;
						break;
					} catch (java.lang.NumberFormatException e) {
						Resp.log(Resp.response(R_CODE.E_INVCLIARG, "-p " + args[i]));
						System.exit(1);
					}
				case "-R" :
					i++; // This was the Responses JSON file that we already processed above, so just skip it.
					break;
				case "-D" :  // -D on the command line means we're in debug mode.
					Resp.setDebug();
					break;
				default : 
					System.err.println(Resp.response(R_CODE.E_INVCLIARG, args[i]));
					System.exit(1);
			}
		}
		

		Resp.log("Opening JSON file "+propJSONfileName.toFileString());
		
		// Set up the g37d and run it on the provided port number.
		g37d.setDevice(g37d.createDeviceFromJSON(propJSONfileName));
		g37d.run(portNumber);
		
    }	// main
}
