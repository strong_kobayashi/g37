/**
 */
package G37.G37_Device.impl;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Device.Device;
import G37.G37_Device.DeviceProperty;
import G37.G37_Device.G37_DevicePackage;
import G37.G37_Device.KindOfData;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Device</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Device.impl.DeviceImpl#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeviceImpl extends MinimalEObjectImpl.Container implements Device {
	

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<DeviceProperty> properties;
	
	long uptime;  
	long timeOfLastUpdate = 0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_DevicePackage.Literals.DEVICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DeviceProperty> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<DeviceProperty>(DeviceProperty.class, this, G37_DevicePackage.DEVICE__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE__PROPERTIES:
				return getProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends DeviceProperty>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE__PROPERTIES:
				getProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_DevicePackage.DEVICE__PROPERTIES:
				return properties != null && !properties.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	// Set the uptime field.
	public void setUptime (long ut) {
		uptime = ut;
	}
	
	//This function returns the property as identified by its name.
	public DeviceProperty getPropertyByName(String name) {
		for (DeviceProperty p : properties) {
			if (p.getName().equals(name)) return p;
		}
		return null;
	}
	
	/*
	 * This function returns the property value as identified by its name
	 * while performing various checks as required by our business logic.
	 * The userType is the type of the user (EU, DS, SP) who is asking for 
	 * this value.
	 * Author: sergeyb
	 */
	public String peek(String propName, String userType) {
		String reply = "";
		if (propName.isEmpty()) reply = Resp.response(R_CODE.E_EMPTYPROP);
		else {
			DeviceProperty prop = getPropertyByName(propName);
			if (prop == null) reply = Resp.response(R_CODE.E_PROPNOTFOUND, propName);
			else if (userType.equals(Const.DS) && prop.getKindOfData() != KindOfData.MAINTENANCE_DATA) reply = Resp.response(R_CODE.E_DSACCESS);
			else {
				reply = prop.getPropValue().toString();
				reply = (reply != null) ? reply : Resp.response(R_CODE.E_INTPROPNULL, propName);
    		}
		}
		return reply;
	}
	
	/*
	 * This method sets the property value as identified by its name
	 * while performing various checks as required by our business logic.
	 * The userType is the type of the user (EU, DS, SP) who is asking for 
	 * this value.
	 * Author: sergeyb
	 */
	public String poke(String propName, String newValue, String userType) {
		String reply = "";
		if (propName == "") reply = Resp.response(R_CODE.E_EMPTYPROP);
		else {
			DeviceProperty prop = getPropertyByName(propName);
			if (prop == null) reply = Resp.response(R_CODE.E_PROPNOTFOUND, propName);
			else if (prop.isIsReadOnly()) reply = Resp.response(R_CODE.E_ISREADONLY, propName);
			else if (userType.equals(Const.DS) && prop.getKindOfData() != KindOfData.MAINTENANCE_DATA) reply = Resp.response(R_CODE.E_DSACCESS); 
			else {
				Class propClass = prop.getPropValueType(); 
				Constructor con;
				Object val = null;
				try {
					con = propClass.getConstructor(String.class);
					val = con.newInstance(newValue);
				} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					System.err.println ("Failed to converted the string \"" + newValue + "\" to " + propClass + ", exception " + e.getMessage());
					reply = Resp.response(R_CODE.E_FAILEDCAST, newValue + " to " + propClass);
					return reply;
					//e.printStackTrace();
				} 
				Resp.log("--- Converted the string \"" + newValue + "\" to " + propClass + ", obtained " + val.toString(), true);
				prop.setPropValue(val);
				reply = Resp.response(R_CODE.OK);
			}
		}
		return reply;
	}

	
	/*
	 * This method is a placeholder for firmware update functionality. 
	 * If this were a real-life home automation system, this function would decode the given string and push it
	 * to the actual device as a firmware update.
	 * But because we are only doing a mock-up, this method simply prints the appropriate message to the console.
	 * Author: sergeyb
	 */
	public void updateFirmware (String update) {
		Resp.log("Updating firmware, please stand by.....");
		Resp.delay(4);
		// This is where the actual update code must be.
		Resp.log(" done.");
	}
	
	/*
	 * This method is called periodically in order to let the device update its fields and properties, e.g.
	 * update its uptime field or copy the values from the hardware registers into properties like Temperature,
	 * Movement etc (in out mock-up environment these will be randomly generated).
	 * Author: sergeyb
	 */
	public void updateFields() {
		// Update uptime
		Date now = new Date();
		if (peek(Const.IsOn, Const.EU).equals("true") 
			|| (Resp.lookUpCode(peek(Const.Speed, Const.EU)) != R_CODE.E_PROPNOTFOUND && !peek(Const.Speed, Const.EU).equals("0"))) { // The device is running.  
			timeOfLastUpdate = (timeOfLastUpdate == 0) ? now.getTime()/1000 : timeOfLastUpdate; // In case this is our first update since creation of the object.
			uptime = uptime + (now.getTime()/1000 - timeOfLastUpdate);
			timeOfLastUpdate = now.getTime()/1000;
			poke (Const.Uptime, String.valueOf(uptime), Const.EU);  // Update the property
		}

		for (DeviceProperty prop : getProperties()) {
			if (prop.getName().equals("Temperature")) prop.setPropValue((int)((Math.random() * ((Const.maxTemp - Const.minTemp) + 1)) + Const.minTemp));
			if (prop.getName().equals("Movement")) prop.setPropValue((Math.random() > 0.5) ? true : false);
			if (prop.getName().equals("Filter needs cleaning")) prop.setPropValue((Math.random() > 0.95) ? true : false); // We make this a rare event because we'll be checking it with a rule.
			// et cetera...
		}
	}
	
} //DeviceImpl
