/**
 */
package G37.G37_Device.impl;

import G37.G37_Controller.G37_ControllerPackage;

import G37.G37_Controller.impl.G37_ControllerPackageImpl;

import G37.G37_Device.Device;
import G37.G37_Device.DeviceProperty;
import G37.G37_Device.G37_DeviceFactory;
import G37.G37_Device.G37_DevicePackage;
import G37.G37_Device.KindOfData;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class G37_DevicePackageImpl extends EPackageImpl implements G37_DevicePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass g37DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deviceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass devicePropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum kindOfDataEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see G37.G37_Device.G37_DevicePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private G37_DevicePackageImpl() {
		super(eNS_URI, G37_DeviceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link G37_DevicePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static G37_DevicePackage init() {
		if (isInited) return (G37_DevicePackage)EPackage.Registry.INSTANCE.getEPackage(G37_DevicePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredG37_DevicePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		G37_DevicePackageImpl theG37_DevicePackage = registeredG37_DevicePackage instanceof G37_DevicePackageImpl ? (G37_DevicePackageImpl)registeredG37_DevicePackage : new G37_DevicePackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(G37_ControllerPackage.eNS_URI);
		G37_ControllerPackageImpl theG37_ControllerPackage = (G37_ControllerPackageImpl)(registeredPackage instanceof G37_ControllerPackageImpl ? registeredPackage : G37_ControllerPackage.eINSTANCE);

		// Create package meta-data objects
		theG37_DevicePackage.createPackageContents();
		theG37_ControllerPackage.createPackageContents();

		// Initialize created meta-data
		theG37_DevicePackage.initializePackageContents();
		theG37_ControllerPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theG37_DevicePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(G37_DevicePackage.eNS_URI, theG37_DevicePackage);
		return theG37_DevicePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getG37D() {
		return g37DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getG37D_Device() {
		return (EReference)g37DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDevice() {
		return deviceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDevice_Properties() {
		return (EReference)deviceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDeviceProperty() {
		return devicePropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDeviceProperty_Name() {
		return (EAttribute)devicePropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDeviceProperty_PropValue() {
		return (EAttribute)devicePropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDeviceProperty_PropValueType() {
		return (EAttribute)devicePropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDeviceProperty_KindOfData() {
		return (EAttribute)devicePropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDeviceProperty_IsReadOnly() {
		return (EAttribute)devicePropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getKindOfData() {
		return kindOfDataEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public G37_DeviceFactory getG37_DeviceFactory() {
		return (G37_DeviceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		g37DEClass = createEClass(G37D);
		createEReference(g37DEClass, G37D__DEVICE);

		deviceEClass = createEClass(DEVICE);
		createEReference(deviceEClass, DEVICE__PROPERTIES);

		devicePropertyEClass = createEClass(DEVICE_PROPERTY);
		createEAttribute(devicePropertyEClass, DEVICE_PROPERTY__NAME);
		createEAttribute(devicePropertyEClass, DEVICE_PROPERTY__PROP_VALUE);
		createEAttribute(devicePropertyEClass, DEVICE_PROPERTY__PROP_VALUE_TYPE);
		createEAttribute(devicePropertyEClass, DEVICE_PROPERTY__KIND_OF_DATA);
		createEAttribute(devicePropertyEClass, DEVICE_PROPERTY__IS_READ_ONLY);

		// Create enums
		kindOfDataEEnum = createEEnum(KIND_OF_DATA);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(g37DEClass, G37.G37_Device.G37D.class, "G37D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getG37D_Device(), this.getDevice(), null, "device", null, 1, 1, G37.G37_Device.G37D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deviceEClass, Device.class, "Device", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDevice_Properties(), this.getDeviceProperty(), null, "properties", null, 0, -1, Device.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(devicePropertyEClass, DeviceProperty.class, "DeviceProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeviceProperty_Name(), ecorePackage.getEString(), "name", null, 1, 1, DeviceProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeviceProperty_PropValue(), ecorePackage.getEJavaObject(), "propValue", null, 1, 1, DeviceProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getDeviceProperty_PropValueType(), g1, "propValueType", null, 1, 1, DeviceProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeviceProperty_KindOfData(), this.getKindOfData(), "kindOfData", null, 1, 1, DeviceProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeviceProperty_IsReadOnly(), ecorePackage.getEBoolean(), "isReadOnly", null, 1, 1, DeviceProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(kindOfDataEEnum, KindOfData.class, "KindOfData");
		addEEnumLiteral(kindOfDataEEnum, KindOfData.SYSTEM_DATA);
		addEEnumLiteral(kindOfDataEEnum, KindOfData.MAINTENANCE_DATA);
		addEEnumLiteral(kindOfDataEEnum, KindOfData.USAGE_DATA);
		addEEnumLiteral(kindOfDataEEnum, KindOfData.SENSOR_DATA);
		addEEnumLiteral(kindOfDataEEnum, KindOfData.OPERATIONAL_SETTING);
		addEEnumLiteral(kindOfDataEEnum, KindOfData.RESERVED);

		// Create resource
		createResource(eNS_URI);
	}

} //G37_DevicePackageImpl
