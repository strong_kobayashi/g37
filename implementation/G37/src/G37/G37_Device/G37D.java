/**
 */
package G37.G37_Device;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>G37D</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Device.G37D#getDevice <em>Device</em>}</li>
 * </ul>
 *
 * @see G37.G37_Device.G37_DevicePackage#getG37D()
 * @model
 * @generated
 */
public interface G37D extends EObject {
	/**
	 * Returns the value of the '<em><b>Device</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device</em>' containment reference.
	 * @see #setDevice(Device)
	 * @see G37.G37_Device.G37_DevicePackage#getG37D_Device()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Device getDevice();

	/**
	 * Sets the value of the '{@link G37.G37_Device.G37D#getDevice <em>Device</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device</em>' containment reference.
	 * @see #getDevice()
	 * @generated
	 */
	void setDevice(Device value);
	
	// Added by sergeyb
	Device createDeviceFromJSON(URI fileName);
	
	void run(int portNumber);

} // G37D
