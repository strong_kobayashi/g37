/**
 */
package G37.G37_Device;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Device</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Device.Device#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @see G37.G37_Device.G37_DevicePackage#getDevice()
 * @model
 * @generated
 */
public interface Device extends EObject {
	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link G37.G37_Device.DeviceProperty}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see G37.G37_Device.G37_DevicePackage#getDevice_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<DeviceProperty> getProperties();
	
	/*
	 * This function returns the property as identified by its name.
	 * Author: sergeyb
	 */
	DeviceProperty getPropertyByName(String name);
	
	/*
	 * This function returns the property value as identified by its name
	 * while performing various checks as required by our business logic.
	 * The userType is the type of the user (EU, DS, SP) who is asking for 
	 * this value.
	 * Author: sergeyb
	 */
	String peek(String propName, String userType);
	
	/*
	 * This function sets the property value as identified by its name
	 * while performing various checks as required by our business logic.
	 * The userType is the type of the user (EU, DS, SP) who is asking for 
	 * this value.
	 * Author: sergeyb
	 */
	String poke(String propName, String newValue, String userType);
	
	/*
	 * This method is a placeholder for firmware update functionality. 
	 * If this were a real-life home automation system, this function would decode the given string and push it
	 * to the actual device as a firmware update.
	 * But because we are only doing a mock-up, this method simply prints the appropriate message to the console.
	 * Author: sergeyb
	 */
	void updateFirmware(String update);
	
	/*
	 * This method is called periodically in order to let the device update its fields and properties, e.g.
	 * update its uptime field or copy the values from the hardware registers into properties like Temperature,
	 * Movement et cetera (in out mock-up environment these will be randomly generated).
	 * Author: sergeyb
	 */
	public void updateFields();
	
	// Set the uptime field.
	public void setUptime (long ut);

} // Device
