/**
 */
package G37.G37_Controller;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see G37.G37_Controller.G37_ControllerPackage
 * @generated
 */
public interface G37_ControllerFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	G37_ControllerFactory eINSTANCE = G37.G37_Controller.impl.G37_ControllerFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>G37C</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>G37C</em>'.
	 * @generated
	 */
	G37C createG37C();

	/**
	 * Returns a new object of class '<em>End User</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>End User</em>'.
	 * @generated
	 */
	EndUser createEndUser();

	/**
	 * Returns a new object of class '<em>Service Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Provider</em>'.
	 * @generated
	 */
	ServiceProvider createServiceProvider();

	/**
	 * Returns a new object of class '<em>Device Supplier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Device Supplier</em>'.
	 * @generated
	 */
	DeviceSupplier createDeviceSupplier();

	/**
	 * Returns a new object of class '<em>Device Account</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Device Account</em>'.
	 * @generated
	 */
	DeviceAccount createDeviceAccount();

	/**
	 * Returns a new object of class '<em>Service Subscription</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Subscription</em>'.
	 * @generated
	 */
	ServiceSubscription createServiceSubscription();

	/**
	 * Returns a new object of class '<em>Supplier Subscription</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Supplier Subscription</em>'.
	 * @generated
	 */
	SupplierSubscription createSupplierSubscription();

	/**
	 * Returns a new object of class '<em>Room</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Room</em>'.
	 * @generated
	 */
	Room createRoom();

	/**
	 * Returns a new object of class '<em>Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule</em>'.
	 * @generated
	 */
	Rule createRule();

	/**
	 * Returns a new object of class '<em>Event Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Condition</em>'.
	 * @generated
	 */
	EventCondition createEventCondition();

	/**
	 * Returns a new object of class '<em>User Accounts Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Accounts Manager</em>'.
	 * @generated
	 */
	UserAccountsManager createUserAccountsManager();

	/**
	 * Returns a new object of class '<em>Rooms Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rooms Manager</em>'.
	 * @generated
	 */
	RoomsManager createRoomsManager();

	/**
	 * Returns a new object of class '<em>Device Accounts Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Device Accounts Manager</em>'.
	 * @generated
	 */
	DeviceAccountsManager createDeviceAccountsManager();

	/**
	 * Returns a new object of class '<em>Subscriptions Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subscriptions Manager</em>'.
	 * @generated
	 */
	SubscriptionsManager createSubscriptionsManager();

	/**
	 * Returns a new object of class '<em>Rules Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rules Manager</em>'.
	 * @generated
	 */
	RulesManager createRulesManager();

	/**
	 * Returns a new object of class '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action</em>'.
	 * @generated
	 */
	Action createAction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	G37_ControllerPackage getG37_ControllerPackage();

} //G37_ControllerFactory
