/**
 */
package G37.G37_Controller;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rules Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.RulesManager#getRules <em>Rules</em>}</li>
 *   <li>{@link G37.G37_Controller.RulesManager#getDaManager <em>Da Manager</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getRulesManager()
 * @model
 * @generated
 */
public interface RulesManager extends EObject {
	/**
	 * Returns the value of the '<em><b>Rules</b></em>' containment reference list.
	 * The list contents are of type {@link G37.G37_Controller.Rule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules</em>' containment reference list.
	 * @see G37.G37_Controller.G37_ControllerPackage#getRulesManager_Rules()
	 * @model containment="true"
	 * @generated
	 */
	EList<Rule> getRules();

	/**
	 * Returns the value of the '<em><b>Da Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Da Manager</em>' reference.
	 * @see #setDaManager(DeviceAccountsManager)
	 * @see G37.G37_Controller.G37_ControllerPackage#getRulesManager_DaManager()
	 * @model required="true"
	 * @generated
	 */
	DeviceAccountsManager getDaManager();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.RulesManager#getDaManager <em>Da Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Da Manager</em>' reference.
	 * @see #getDaManager()
	 * @generated
	 */
	void setDaManager(DeviceAccountsManager value);
	
	// Sets the value of the dslStandaloneSetup member
	//void setDslStandaloneSetup(DslStandaloneSetup setup);
	// Gets the value of the dslStandaloneSetup member
	//DslStandaloneSetup getDslStandaloneSetup();
	// Sets the value of the parseHelper member
	//void setParseHelper(ParseHelper<Model> ps);
	
	// This method returns the rule as identified by the rule ID.
	Rule getRuleByID(String ruleID);
	
	// This method sets the controller factory member of this object.
	void setControllerFactory(G37_ControllerFactory cf);
	
	// Implementation of ADDRULE command.  Adds a new rule to our home automaton controller.
	String addRule(String ruleID, String eca, UserAccount callingUser);
	
	// Another implementation of ADDRULE command.  This is just a legacy signature for compatibility with other team members.
	String addRule(String ruleID, String eventCond, String action, UserAccount callingUser);
	
	// Implementation of LSRULES command. Lists the rules accessible to the calling user.
	String lsRules(UserAccount callingUser);
	
	// Implementation of rmRule command.  Deletes a rule with the given ID.
	String rmRule (String ruleID, UserAccount callingUser);
	
	// This method goes through the list of rules and executes them all.
	// Returns OK if all executed without errors, or the first error otherwise.
	String executeAll();

} // RulesManager
