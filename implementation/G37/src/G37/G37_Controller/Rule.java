/**
 */
package G37.G37_Controller;

import org.eclipse.emf.ecore.EObject;

import g37.dsl.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.Rule#getId <em>Id</em>}</li>
 *   <li>{@link G37.G37_Controller.Rule#getEventCondition <em>Event Condition</em>}</li>
 *   <li>{@link G37.G37_Controller.Rule#getAction <em>Action</em>}</li>
 *   <li>{@link G37.G37_Controller.Rule#getCreator <em>Creator</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getRule()
 * @model
 * @generated
 */
public interface Rule extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see G37.G37_Controller.G37_ControllerPackage#getRule_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Rule#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Event Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Condition</em>' containment reference.
	 * @see #setEventCondition(EventCondition)
	 * @see G37.G37_Controller.G37_ControllerPackage#getRule_EventCondition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EventCondition getEventCondition();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Rule#getEventCondition <em>Event Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Condition</em>' containment reference.
	 * @see #getEventCondition()
	 * @generated
	 */
	void setEventCondition(EventCondition value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' reference.
	 * @see #setAction(Action)
	 * @see G37.G37_Controller.G37_ControllerPackage#getRule_Action()
	 * @model required="true"
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Rule#getAction <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

	/**
	 * Returns the value of the '<em><b>Creator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Creator</em>' reference.
	 * @see #setCreator(UserAccount)
	 * @see G37.G37_Controller.G37_ControllerPackage#getRule_Creator()
	 * @model required="true"
	 * @generated
	 */
	UserAccount getCreator();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Rule#getCreator <em>Creator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Creator</em>' reference.
	 * @see #getCreator()
	 * @generated
	 */
	void setCreator(UserAccount value);
	
	String initFromDSLmodel(Model ast);
	
	// Execute the rule
	String execute();
	
	// Get /Set the ECA string, which is used only for listing (lsrules)
	void setEca(String eca);
	String getEca();

} // Rule
