/**
 */
package G37.G37_Controller;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Subscription</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.GenericSubscription#getId <em>Id</em>}</li>
 *   <li>{@link G37.G37_Controller.GenericSubscription#getDevClass <em>Dev Class</em>}</li>
 *   <li>{@link G37.G37_Controller.GenericSubscription#getRooms <em>Rooms</em>}</li>
 *   <li>{@link G37.G37_Controller.GenericSubscription#getUser <em>User</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getGenericSubscription()
 * @model abstract="true"
 * @generated
 */
public interface GenericSubscription extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see G37.G37_Controller.G37_ControllerPackage#getGenericSubscription_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.GenericSubscription#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Dev Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dev Class</em>' attribute.
	 * @see #setDevClass(String)
	 * @see G37.G37_Controller.G37_ControllerPackage#getGenericSubscription_DevClass()
	 * @model required="true"
	 * @generated
	 */
	String getDevClass();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.GenericSubscription#getDevClass <em>Dev Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dev Class</em>' attribute.
	 * @see #getDevClass()
	 * @generated
	 */
	void setDevClass(String value);

	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' reference list.
	 * The list contents are of type {@link G37.G37_Controller.Room}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' reference list.
	 * @see G37.G37_Controller.G37_ControllerPackage#getGenericSubscription_Rooms()
	 * @model
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * Returns the value of the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' reference.
	 * @see #setUser(UserAccount)
	 * @see G37.G37_Controller.G37_ControllerPackage#getGenericSubscription_User()
	 * @model required="true"
	 * @generated
	 */
	UserAccount getUser();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.GenericSubscription#getUser <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' reference.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(UserAccount value);

} // GenericSubscription
