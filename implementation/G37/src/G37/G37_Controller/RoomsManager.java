/**
 */
package G37.G37_Controller;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rooms Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.RoomsManager#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getRoomsManager()
 * @model
 * @generated
 */
public interface RoomsManager extends EObject {
	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' containment reference list.
	 * The list contents are of type {@link G37.G37_Controller.Room}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' containment reference list.
	 * @see G37.G37_Controller.G37_ControllerPackage#getRoomsManager_Rooms()
	 * @model containment="true"
	 * @generated
	 */
	EList<Room> getRooms();
	
	// This method returns the room as identified by the room ID.
	Room getRoomByID(String roomID);
	
	// This method sets the controller factory member of this class.
	void setControllerFactory(G37_ControllerFactory cf);
	
	// Implementation of ADDROOM command.  Adds a new room to our home automaton controller.
	String addRoom(String roomID);
	
	// Implementation of LSROOMS command. Lists all existing rooms.
	String lsRooms();
	
	// Implementation of RMROOM command.  Deletes a room with the given ID.
	String rmRoom(String roomID);

} // RoomsManager
