/**
 */
package G37.G37_Controller;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see G37.G37_Controller.G37_ControllerFactory
 * @model kind="package"
 * @generated
 */
public interface G37_ControllerPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "G37_Controller";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://homepage.univie.ac.at/sergeyb73/G37C";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "G37C_Namespace";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	G37_ControllerPackage eINSTANCE = G37.G37_Controller.impl.G37_ControllerPackageImpl.init();

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.G37CImpl <em>G37C</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.G37CImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getG37C()
	 * @generated
	 */
	int G37C = 0;

	/**
	 * The feature id for the '<em><b>Ua Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37C__UA_MANAGER = 0;

	/**
	 * The feature id for the '<em><b>Da Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37C__DA_MANAGER = 1;

	/**
	 * The feature id for the '<em><b>Rooms Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37C__ROOMS_MANAGER = 2;

	/**
	 * The feature id for the '<em><b>Subscr Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37C__SUBSCR_MANAGER = 3;

	/**
	 * The feature id for the '<em><b>Rules Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37C__RULES_MANAGER = 4;

	/**
	 * The number of structural features of the '<em>G37C</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37C_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>G37C</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G37C_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.UserAccountImpl <em>User Account</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.UserAccountImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getUserAccount()
	 * @generated
	 */
	int USER_ACCOUNT = 1;

	/**
	 * The feature id for the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACCOUNT__USERNAME = 0;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACCOUNT__PASSWORD = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACCOUNT__DESCRIPTION = 2;

	/**
	 * The number of structural features of the '<em>User Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACCOUNT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>User Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACCOUNT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.EndUserImpl <em>End User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.EndUserImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getEndUser()
	 * @generated
	 */
	int END_USER = 2;

	/**
	 * The feature id for the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_USER__USERNAME = USER_ACCOUNT__USERNAME;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_USER__PASSWORD = USER_ACCOUNT__PASSWORD;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_USER__DESCRIPTION = USER_ACCOUNT__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>End User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_USER_FEATURE_COUNT = USER_ACCOUNT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>End User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_USER_OPERATION_COUNT = USER_ACCOUNT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.ServiceProviderImpl <em>Service Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.ServiceProviderImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getServiceProvider()
	 * @generated
	 */
	int SERVICE_PROVIDER = 3;

	/**
	 * The feature id for the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROVIDER__USERNAME = USER_ACCOUNT__USERNAME;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROVIDER__PASSWORD = USER_ACCOUNT__PASSWORD;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROVIDER__DESCRIPTION = USER_ACCOUNT__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Service Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROVIDER_FEATURE_COUNT = USER_ACCOUNT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Service Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROVIDER_OPERATION_COUNT = USER_ACCOUNT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.DeviceSupplierImpl <em>Device Supplier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.DeviceSupplierImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getDeviceSupplier()
	 * @generated
	 */
	int DEVICE_SUPPLIER = 4;

	/**
	 * The feature id for the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_SUPPLIER__USERNAME = USER_ACCOUNT__USERNAME;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_SUPPLIER__PASSWORD = USER_ACCOUNT__PASSWORD;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_SUPPLIER__DESCRIPTION = USER_ACCOUNT__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Device Supplier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_SUPPLIER_FEATURE_COUNT = USER_ACCOUNT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Device Supplier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_SUPPLIER_OPERATION_COUNT = USER_ACCOUNT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.DeviceAccountImpl <em>Device Account</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.DeviceAccountImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getDeviceAccount()
	 * @generated
	 */
	int DEVICE_ACCOUNT = 5;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_ACCOUNT__URL = 0;

	/**
	 * The feature id for the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_ACCOUNT__ROOM = 1;

	/**
	 * The number of structural features of the '<em>Device Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_ACCOUNT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Device Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_ACCOUNT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.GenericSubscriptionImpl <em>Generic Subscription</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.GenericSubscriptionImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getGenericSubscription()
	 * @generated
	 */
	int GENERIC_SUBSCRIPTION = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_SUBSCRIPTION__ID = 0;

	/**
	 * The feature id for the '<em><b>Dev Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_SUBSCRIPTION__DEV_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_SUBSCRIPTION__ROOMS = 2;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_SUBSCRIPTION__USER = 3;

	/**
	 * The number of structural features of the '<em>Generic Subscription</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_SUBSCRIPTION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Generic Subscription</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_SUBSCRIPTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.ServiceSubscriptionImpl <em>Service Subscription</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.ServiceSubscriptionImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getServiceSubscription()
	 * @generated
	 */
	int SERVICE_SUBSCRIPTION = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_SUBSCRIPTION__ID = GENERIC_SUBSCRIPTION__ID;

	/**
	 * The feature id for the '<em><b>Dev Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_SUBSCRIPTION__DEV_CLASS = GENERIC_SUBSCRIPTION__DEV_CLASS;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_SUBSCRIPTION__ROOMS = GENERIC_SUBSCRIPTION__ROOMS;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_SUBSCRIPTION__USER = GENERIC_SUBSCRIPTION__USER;

	/**
	 * The number of structural features of the '<em>Service Subscription</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_SUBSCRIPTION_FEATURE_COUNT = GENERIC_SUBSCRIPTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Service Subscription</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_SUBSCRIPTION_OPERATION_COUNT = GENERIC_SUBSCRIPTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.SupplierSubscriptionImpl <em>Supplier Subscription</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.SupplierSubscriptionImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getSupplierSubscription()
	 * @generated
	 */
	int SUPPLIER_SUBSCRIPTION = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPPLIER_SUBSCRIPTION__ID = GENERIC_SUBSCRIPTION__ID;

	/**
	 * The feature id for the '<em><b>Dev Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPPLIER_SUBSCRIPTION__DEV_CLASS = GENERIC_SUBSCRIPTION__DEV_CLASS;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPPLIER_SUBSCRIPTION__ROOMS = GENERIC_SUBSCRIPTION__ROOMS;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPPLIER_SUBSCRIPTION__USER = GENERIC_SUBSCRIPTION__USER;

	/**
	 * The number of structural features of the '<em>Supplier Subscription</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPPLIER_SUBSCRIPTION_FEATURE_COUNT = GENERIC_SUBSCRIPTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Supplier Subscription</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPPLIER_SUBSCRIPTION_OPERATION_COUNT = GENERIC_SUBSCRIPTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.RoomImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ID = 0;

	/**
	 * The feature id for the '<em><b>Device Accounts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__DEVICE_ACCOUNTS = 1;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.RuleImpl <em>Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.RuleImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getRule()
	 * @generated
	 */
	int RULE = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__ID = 0;

	/**
	 * The feature id for the '<em><b>Event Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__EVENT_CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__ACTION = 2;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__CREATOR = 3;

	/**
	 * The number of structural features of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.EventConditionImpl <em>Event Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.EventConditionImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getEventCondition()
	 * @generated
	 */
	int EVENT_CONDITION = 11;

	/**
	 * The feature id for the '<em><b>Da Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION__DA_MANAGER = 0;

	/**
	 * The number of structural features of the '<em>Event Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Event Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.UserAccountsManagerImpl <em>User Accounts Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.UserAccountsManagerImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getUserAccountsManager()
	 * @generated
	 */
	int USER_ACCOUNTS_MANAGER = 12;

	/**
	 * The feature id for the '<em><b>User Accounts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACCOUNTS_MANAGER__USER_ACCOUNTS = 0;

	/**
	 * The number of structural features of the '<em>User Accounts Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACCOUNTS_MANAGER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>User Accounts Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACCOUNTS_MANAGER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.RoomsManagerImpl <em>Rooms Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.RoomsManagerImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getRoomsManager()
	 * @generated
	 */
	int ROOMS_MANAGER = 13;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOMS_MANAGER__ROOMS = 0;

	/**
	 * The number of structural features of the '<em>Rooms Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOMS_MANAGER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Rooms Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOMS_MANAGER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.DeviceAccountsManagerImpl <em>Device Accounts Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.DeviceAccountsManagerImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getDeviceAccountsManager()
	 * @generated
	 */
	int DEVICE_ACCOUNTS_MANAGER = 14;

	/**
	 * The feature id for the '<em><b>Device Accounts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_ACCOUNTS_MANAGER__DEVICE_ACCOUNTS = 0;

	/**
	 * The number of structural features of the '<em>Device Accounts Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_ACCOUNTS_MANAGER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Device Accounts Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_ACCOUNTS_MANAGER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.SubscriptionsManagerImpl <em>Subscriptions Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.SubscriptionsManagerImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getSubscriptionsManager()
	 * @generated
	 */
	int SUBSCRIPTIONS_MANAGER = 15;

	/**
	 * The feature id for the '<em><b>Subscriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTIONS_MANAGER__SUBSCRIPTIONS = 0;

	/**
	 * The number of structural features of the '<em>Subscriptions Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTIONS_MANAGER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Subscriptions Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTIONS_MANAGER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.RulesManagerImpl <em>Rules Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.RulesManagerImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getRulesManager()
	 * @generated
	 */
	int RULES_MANAGER = 16;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULES_MANAGER__RULES = 0;

	/**
	 * The feature id for the '<em><b>Da Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULES_MANAGER__DA_MANAGER = 1;

	/**
	 * The number of structural features of the '<em>Rules Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULES_MANAGER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Rules Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULES_MANAGER_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link G37.G37_Controller.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see G37.G37_Controller.impl.ActionImpl
	 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 17;

	/**
	 * The feature id for the '<em><b>Da</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DA = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__VALUE = 2;

	/**
	 * The feature id for the '<em><b>Da Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DA_MANAGER = 3;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.G37C <em>G37C</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>G37C</em>'.
	 * @see G37.G37_Controller.G37C
	 * @generated
	 */
	EClass getG37C();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.G37C#getUaManager <em>Ua Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ua Manager</em>'.
	 * @see G37.G37_Controller.G37C#getUaManager()
	 * @see #getG37C()
	 * @generated
	 */
	EReference getG37C_UaManager();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.G37C#getDaManager <em>Da Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Da Manager</em>'.
	 * @see G37.G37_Controller.G37C#getDaManager()
	 * @see #getG37C()
	 * @generated
	 */
	EReference getG37C_DaManager();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.G37C#getRoomsManager <em>Rooms Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rooms Manager</em>'.
	 * @see G37.G37_Controller.G37C#getRoomsManager()
	 * @see #getG37C()
	 * @generated
	 */
	EReference getG37C_RoomsManager();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.G37C#getSubscrManager <em>Subscr Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Subscr Manager</em>'.
	 * @see G37.G37_Controller.G37C#getSubscrManager()
	 * @see #getG37C()
	 * @generated
	 */
	EReference getG37C_SubscrManager();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.G37C#getRulesManager <em>Rules Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Rules Manager</em>'.
	 * @see G37.G37_Controller.G37C#getRulesManager()
	 * @see #getG37C()
	 * @generated
	 */
	EReference getG37C_RulesManager();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.UserAccount <em>User Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Account</em>'.
	 * @see G37.G37_Controller.UserAccount
	 * @generated
	 */
	EClass getUserAccount();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.UserAccount#getUsername <em>Username</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Username</em>'.
	 * @see G37.G37_Controller.UserAccount#getUsername()
	 * @see #getUserAccount()
	 * @generated
	 */
	EAttribute getUserAccount_Username();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.UserAccount#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see G37.G37_Controller.UserAccount#getPassword()
	 * @see #getUserAccount()
	 * @generated
	 */
	EAttribute getUserAccount_Password();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.UserAccount#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see G37.G37_Controller.UserAccount#getDescription()
	 * @see #getUserAccount()
	 * @generated
	 */
	EAttribute getUserAccount_Description();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.EndUser <em>End User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>End User</em>'.
	 * @see G37.G37_Controller.EndUser
	 * @generated
	 */
	EClass getEndUser();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.ServiceProvider <em>Service Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Provider</em>'.
	 * @see G37.G37_Controller.ServiceProvider
	 * @generated
	 */
	EClass getServiceProvider();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.DeviceSupplier <em>Device Supplier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Device Supplier</em>'.
	 * @see G37.G37_Controller.DeviceSupplier
	 * @generated
	 */
	EClass getDeviceSupplier();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.DeviceAccount <em>Device Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Device Account</em>'.
	 * @see G37.G37_Controller.DeviceAccount
	 * @generated
	 */
	EClass getDeviceAccount();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.DeviceAccount#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see G37.G37_Controller.DeviceAccount#getUrl()
	 * @see #getDeviceAccount()
	 * @generated
	 */
	EAttribute getDeviceAccount_Url();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.DeviceAccount#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room</em>'.
	 * @see G37.G37_Controller.DeviceAccount#getRoom()
	 * @see #getDeviceAccount()
	 * @generated
	 */
	EReference getDeviceAccount_Room();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.GenericSubscription <em>Generic Subscription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic Subscription</em>'.
	 * @see G37.G37_Controller.GenericSubscription
	 * @generated
	 */
	EClass getGenericSubscription();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.GenericSubscription#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see G37.G37_Controller.GenericSubscription#getId()
	 * @see #getGenericSubscription()
	 * @generated
	 */
	EAttribute getGenericSubscription_Id();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.GenericSubscription#getDevClass <em>Dev Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dev Class</em>'.
	 * @see G37.G37_Controller.GenericSubscription#getDevClass()
	 * @see #getGenericSubscription()
	 * @generated
	 */
	EAttribute getGenericSubscription_DevClass();

	/**
	 * Returns the meta object for the reference list '{@link G37.G37_Controller.GenericSubscription#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see G37.G37_Controller.GenericSubscription#getRooms()
	 * @see #getGenericSubscription()
	 * @generated
	 */
	EReference getGenericSubscription_Rooms();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.GenericSubscription#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User</em>'.
	 * @see G37.G37_Controller.GenericSubscription#getUser()
	 * @see #getGenericSubscription()
	 * @generated
	 */
	EReference getGenericSubscription_User();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.ServiceSubscription <em>Service Subscription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Subscription</em>'.
	 * @see G37.G37_Controller.ServiceSubscription
	 * @generated
	 */
	EClass getServiceSubscription();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.SupplierSubscription <em>Supplier Subscription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Supplier Subscription</em>'.
	 * @see G37.G37_Controller.SupplierSubscription
	 * @generated
	 */
	EClass getSupplierSubscription();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see G37.G37_Controller.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.Room#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see G37.G37_Controller.Room#getId()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Id();

	/**
	 * Returns the meta object for the reference list '{@link G37.G37_Controller.Room#getDeviceAccounts <em>Device Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Device Accounts</em>'.
	 * @see G37.G37_Controller.Room#getDeviceAccounts()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_DeviceAccounts();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.Rule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule</em>'.
	 * @see G37.G37_Controller.Rule
	 * @generated
	 */
	EClass getRule();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.Rule#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see G37.G37_Controller.Rule#getId()
	 * @see #getRule()
	 * @generated
	 */
	EAttribute getRule_Id();

	/**
	 * Returns the meta object for the containment reference '{@link G37.G37_Controller.Rule#getEventCondition <em>Event Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event Condition</em>'.
	 * @see G37.G37_Controller.Rule#getEventCondition()
	 * @see #getRule()
	 * @generated
	 */
	EReference getRule_EventCondition();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.Rule#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see G37.G37_Controller.Rule#getAction()
	 * @see #getRule()
	 * @generated
	 */
	EReference getRule_Action();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.Rule#getCreator <em>Creator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Creator</em>'.
	 * @see G37.G37_Controller.Rule#getCreator()
	 * @see #getRule()
	 * @generated
	 */
	EReference getRule_Creator();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.EventCondition <em>Event Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Condition</em>'.
	 * @see G37.G37_Controller.EventCondition
	 * @generated
	 */
	EClass getEventCondition();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.EventCondition#getDaManager <em>Da Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Da Manager</em>'.
	 * @see G37.G37_Controller.EventCondition#getDaManager()
	 * @see #getEventCondition()
	 * @generated
	 */
	EReference getEventCondition_DaManager();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.UserAccountsManager <em>User Accounts Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Accounts Manager</em>'.
	 * @see G37.G37_Controller.UserAccountsManager
	 * @generated
	 */
	EClass getUserAccountsManager();

	/**
	 * Returns the meta object for the containment reference list '{@link G37.G37_Controller.UserAccountsManager#getUserAccounts <em>User Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>User Accounts</em>'.
	 * @see G37.G37_Controller.UserAccountsManager#getUserAccounts()
	 * @see #getUserAccountsManager()
	 * @generated
	 */
	EReference getUserAccountsManager_UserAccounts();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.RoomsManager <em>Rooms Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rooms Manager</em>'.
	 * @see G37.G37_Controller.RoomsManager
	 * @generated
	 */
	EClass getRoomsManager();

	/**
	 * Returns the meta object for the containment reference list '{@link G37.G37_Controller.RoomsManager#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rooms</em>'.
	 * @see G37.G37_Controller.RoomsManager#getRooms()
	 * @see #getRoomsManager()
	 * @generated
	 */
	EReference getRoomsManager_Rooms();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.DeviceAccountsManager <em>Device Accounts Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Device Accounts Manager</em>'.
	 * @see G37.G37_Controller.DeviceAccountsManager
	 * @generated
	 */
	EClass getDeviceAccountsManager();

	/**
	 * Returns the meta object for the containment reference list '{@link G37.G37_Controller.DeviceAccountsManager#getDeviceAccounts <em>Device Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Device Accounts</em>'.
	 * @see G37.G37_Controller.DeviceAccountsManager#getDeviceAccounts()
	 * @see #getDeviceAccountsManager()
	 * @generated
	 */
	EReference getDeviceAccountsManager_DeviceAccounts();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.SubscriptionsManager <em>Subscriptions Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subscriptions Manager</em>'.
	 * @see G37.G37_Controller.SubscriptionsManager
	 * @generated
	 */
	EClass getSubscriptionsManager();

	/**
	 * Returns the meta object for the containment reference list '{@link G37.G37_Controller.SubscriptionsManager#getSubscriptions <em>Subscriptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subscriptions</em>'.
	 * @see G37.G37_Controller.SubscriptionsManager#getSubscriptions()
	 * @see #getSubscriptionsManager()
	 * @generated
	 */
	EReference getSubscriptionsManager_Subscriptions();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.RulesManager <em>Rules Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rules Manager</em>'.
	 * @see G37.G37_Controller.RulesManager
	 * @generated
	 */
	EClass getRulesManager();

	/**
	 * Returns the meta object for the containment reference list '{@link G37.G37_Controller.RulesManager#getRules <em>Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rules</em>'.
	 * @see G37.G37_Controller.RulesManager#getRules()
	 * @see #getRulesManager()
	 * @generated
	 */
	EReference getRulesManager_Rules();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.RulesManager#getDaManager <em>Da Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Da Manager</em>'.
	 * @see G37.G37_Controller.RulesManager#getDaManager()
	 * @see #getRulesManager()
	 * @generated
	 */
	EReference getRulesManager_DaManager();

	/**
	 * Returns the meta object for class '{@link G37.G37_Controller.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see G37.G37_Controller.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.Action#getDa <em>Da</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Da</em>'.
	 * @see G37.G37_Controller.Action#getDa()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Da();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.Action#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property</em>'.
	 * @see G37.G37_Controller.Action#getProperty()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Property();

	/**
	 * Returns the meta object for the attribute '{@link G37.G37_Controller.Action#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see G37.G37_Controller.Action#getValue()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Value();

	/**
	 * Returns the meta object for the reference '{@link G37.G37_Controller.Action#getDaManager <em>Da Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Da Manager</em>'.
	 * @see G37.G37_Controller.Action#getDaManager()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_DaManager();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	G37_ControllerFactory getG37_ControllerFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.G37CImpl <em>G37C</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.G37CImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getG37C()
		 * @generated
		 */
		EClass G37C = eINSTANCE.getG37C();

		/**
		 * The meta object literal for the '<em><b>Ua Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference G37C__UA_MANAGER = eINSTANCE.getG37C_UaManager();

		/**
		 * The meta object literal for the '<em><b>Da Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference G37C__DA_MANAGER = eINSTANCE.getG37C_DaManager();

		/**
		 * The meta object literal for the '<em><b>Rooms Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference G37C__ROOMS_MANAGER = eINSTANCE.getG37C_RoomsManager();

		/**
		 * The meta object literal for the '<em><b>Subscr Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference G37C__SUBSCR_MANAGER = eINSTANCE.getG37C_SubscrManager();

		/**
		 * The meta object literal for the '<em><b>Rules Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference G37C__RULES_MANAGER = eINSTANCE.getG37C_RulesManager();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.UserAccountImpl <em>User Account</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.UserAccountImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getUserAccount()
		 * @generated
		 */
		EClass USER_ACCOUNT = eINSTANCE.getUserAccount();

		/**
		 * The meta object literal for the '<em><b>Username</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_ACCOUNT__USERNAME = eINSTANCE.getUserAccount_Username();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_ACCOUNT__PASSWORD = eINSTANCE.getUserAccount_Password();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_ACCOUNT__DESCRIPTION = eINSTANCE.getUserAccount_Description();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.EndUserImpl <em>End User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.EndUserImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getEndUser()
		 * @generated
		 */
		EClass END_USER = eINSTANCE.getEndUser();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.ServiceProviderImpl <em>Service Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.ServiceProviderImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getServiceProvider()
		 * @generated
		 */
		EClass SERVICE_PROVIDER = eINSTANCE.getServiceProvider();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.DeviceSupplierImpl <em>Device Supplier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.DeviceSupplierImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getDeviceSupplier()
		 * @generated
		 */
		EClass DEVICE_SUPPLIER = eINSTANCE.getDeviceSupplier();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.DeviceAccountImpl <em>Device Account</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.DeviceAccountImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getDeviceAccount()
		 * @generated
		 */
		EClass DEVICE_ACCOUNT = eINSTANCE.getDeviceAccount();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEVICE_ACCOUNT__URL = eINSTANCE.getDeviceAccount_Url();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEVICE_ACCOUNT__ROOM = eINSTANCE.getDeviceAccount_Room();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.GenericSubscriptionImpl <em>Generic Subscription</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.GenericSubscriptionImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getGenericSubscription()
		 * @generated
		 */
		EClass GENERIC_SUBSCRIPTION = eINSTANCE.getGenericSubscription();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERIC_SUBSCRIPTION__ID = eINSTANCE.getGenericSubscription_Id();

		/**
		 * The meta object literal for the '<em><b>Dev Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERIC_SUBSCRIPTION__DEV_CLASS = eINSTANCE.getGenericSubscription_DevClass();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERIC_SUBSCRIPTION__ROOMS = eINSTANCE.getGenericSubscription_Rooms();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERIC_SUBSCRIPTION__USER = eINSTANCE.getGenericSubscription_User();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.ServiceSubscriptionImpl <em>Service Subscription</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.ServiceSubscriptionImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getServiceSubscription()
		 * @generated
		 */
		EClass SERVICE_SUBSCRIPTION = eINSTANCE.getServiceSubscription();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.SupplierSubscriptionImpl <em>Supplier Subscription</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.SupplierSubscriptionImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getSupplierSubscription()
		 * @generated
		 */
		EClass SUPPLIER_SUBSCRIPTION = eINSTANCE.getSupplierSubscription();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.RoomImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ID = eINSTANCE.getRoom_Id();

		/**
		 * The meta object literal for the '<em><b>Device Accounts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__DEVICE_ACCOUNTS = eINSTANCE.getRoom_DeviceAccounts();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.RuleImpl <em>Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.RuleImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getRule()
		 * @generated
		 */
		EClass RULE = eINSTANCE.getRule();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE__ID = eINSTANCE.getRule_Id();

		/**
		 * The meta object literal for the '<em><b>Event Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE__EVENT_CONDITION = eINSTANCE.getRule_EventCondition();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE__ACTION = eINSTANCE.getRule_Action();

		/**
		 * The meta object literal for the '<em><b>Creator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE__CREATOR = eINSTANCE.getRule_Creator();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.EventConditionImpl <em>Event Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.EventConditionImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getEventCondition()
		 * @generated
		 */
		EClass EVENT_CONDITION = eINSTANCE.getEventCondition();

		/**
		 * The meta object literal for the '<em><b>Da Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_CONDITION__DA_MANAGER = eINSTANCE.getEventCondition_DaManager();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.UserAccountsManagerImpl <em>User Accounts Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.UserAccountsManagerImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getUserAccountsManager()
		 * @generated
		 */
		EClass USER_ACCOUNTS_MANAGER = eINSTANCE.getUserAccountsManager();

		/**
		 * The meta object literal for the '<em><b>User Accounts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER_ACCOUNTS_MANAGER__USER_ACCOUNTS = eINSTANCE.getUserAccountsManager_UserAccounts();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.RoomsManagerImpl <em>Rooms Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.RoomsManagerImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getRoomsManager()
		 * @generated
		 */
		EClass ROOMS_MANAGER = eINSTANCE.getRoomsManager();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOMS_MANAGER__ROOMS = eINSTANCE.getRoomsManager_Rooms();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.DeviceAccountsManagerImpl <em>Device Accounts Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.DeviceAccountsManagerImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getDeviceAccountsManager()
		 * @generated
		 */
		EClass DEVICE_ACCOUNTS_MANAGER = eINSTANCE.getDeviceAccountsManager();

		/**
		 * The meta object literal for the '<em><b>Device Accounts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEVICE_ACCOUNTS_MANAGER__DEVICE_ACCOUNTS = eINSTANCE.getDeviceAccountsManager_DeviceAccounts();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.SubscriptionsManagerImpl <em>Subscriptions Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.SubscriptionsManagerImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getSubscriptionsManager()
		 * @generated
		 */
		EClass SUBSCRIPTIONS_MANAGER = eINSTANCE.getSubscriptionsManager();

		/**
		 * The meta object literal for the '<em><b>Subscriptions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSCRIPTIONS_MANAGER__SUBSCRIPTIONS = eINSTANCE.getSubscriptionsManager_Subscriptions();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.RulesManagerImpl <em>Rules Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.RulesManagerImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getRulesManager()
		 * @generated
		 */
		EClass RULES_MANAGER = eINSTANCE.getRulesManager();

		/**
		 * The meta object literal for the '<em><b>Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULES_MANAGER__RULES = eINSTANCE.getRulesManager_Rules();

		/**
		 * The meta object literal for the '<em><b>Da Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULES_MANAGER__DA_MANAGER = eINSTANCE.getRulesManager_DaManager();

		/**
		 * The meta object literal for the '{@link G37.G37_Controller.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see G37.G37_Controller.impl.ActionImpl
		 * @see G37.G37_Controller.impl.G37_ControllerPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Da</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__DA = eINSTANCE.getAction_Da();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__PROPERTY = eINSTANCE.getAction_Property();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__VALUE = eINSTANCE.getAction_Value();

		/**
		 * The meta object literal for the '<em><b>Da Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__DA_MANAGER = eINSTANCE.getAction_DaManager();

	}

} //G37_ControllerPackage
