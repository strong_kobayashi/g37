/**
 */
package G37.G37_Controller.util;

import G37.G37_Controller.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see G37.G37_Controller.G37_ControllerPackage
 * @generated
 */
public class G37_ControllerSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static G37_ControllerPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public G37_ControllerSwitch() {
		if (modelPackage == null) {
			modelPackage = G37_ControllerPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case G37_ControllerPackage.G37C: {
				G37C g37C = (G37C)theEObject;
				T result = caseG37C(g37C);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.USER_ACCOUNT: {
				UserAccount userAccount = (UserAccount)theEObject;
				T result = caseUserAccount(userAccount);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.END_USER: {
				EndUser endUser = (EndUser)theEObject;
				T result = caseEndUser(endUser);
				if (result == null) result = caseUserAccount(endUser);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.SERVICE_PROVIDER: {
				ServiceProvider serviceProvider = (ServiceProvider)theEObject;
				T result = caseServiceProvider(serviceProvider);
				if (result == null) result = caseUserAccount(serviceProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.DEVICE_SUPPLIER: {
				DeviceSupplier deviceSupplier = (DeviceSupplier)theEObject;
				T result = caseDeviceSupplier(deviceSupplier);
				if (result == null) result = caseUserAccount(deviceSupplier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.DEVICE_ACCOUNT: {
				DeviceAccount deviceAccount = (DeviceAccount)theEObject;
				T result = caseDeviceAccount(deviceAccount);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION: {
				GenericSubscription genericSubscription = (GenericSubscription)theEObject;
				T result = caseGenericSubscription(genericSubscription);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.SERVICE_SUBSCRIPTION: {
				ServiceSubscription serviceSubscription = (ServiceSubscription)theEObject;
				T result = caseServiceSubscription(serviceSubscription);
				if (result == null) result = caseGenericSubscription(serviceSubscription);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.SUPPLIER_SUBSCRIPTION: {
				SupplierSubscription supplierSubscription = (SupplierSubscription)theEObject;
				T result = caseSupplierSubscription(supplierSubscription);
				if (result == null) result = caseGenericSubscription(supplierSubscription);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.ROOM: {
				Room room = (Room)theEObject;
				T result = caseRoom(room);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.RULE: {
				Rule rule = (Rule)theEObject;
				T result = caseRule(rule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.EVENT_CONDITION: {
				EventCondition eventCondition = (EventCondition)theEObject;
				T result = caseEventCondition(eventCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.USER_ACCOUNTS_MANAGER: {
				UserAccountsManager userAccountsManager = (UserAccountsManager)theEObject;
				T result = caseUserAccountsManager(userAccountsManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.ROOMS_MANAGER: {
				RoomsManager roomsManager = (RoomsManager)theEObject;
				T result = caseRoomsManager(roomsManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.DEVICE_ACCOUNTS_MANAGER: {
				DeviceAccountsManager deviceAccountsManager = (DeviceAccountsManager)theEObject;
				T result = caseDeviceAccountsManager(deviceAccountsManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.SUBSCRIPTIONS_MANAGER: {
				SubscriptionsManager subscriptionsManager = (SubscriptionsManager)theEObject;
				T result = caseSubscriptionsManager(subscriptionsManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.RULES_MANAGER: {
				RulesManager rulesManager = (RulesManager)theEObject;
				T result = caseRulesManager(rulesManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case G37_ControllerPackage.ACTION: {
				Action action = (Action)theEObject;
				T result = caseAction(action);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>G37C</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>G37C</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseG37C(G37C object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Account</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Account</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserAccount(UserAccount object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>End User</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>End User</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEndUser(EndUser object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceProvider(ServiceProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Device Supplier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Device Supplier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeviceSupplier(DeviceSupplier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Device Account</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Device Account</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeviceAccount(DeviceAccount object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Subscription</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Subscription</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericSubscription(GenericSubscription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Subscription</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Subscription</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceSubscription(ServiceSubscription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Supplier Subscription</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Supplier Subscription</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSupplierSubscription(SupplierSubscription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoom(Room object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRule(Rule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventCondition(EventCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Accounts Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Accounts Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserAccountsManager(UserAccountsManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rooms Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rooms Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomsManager(RoomsManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Device Accounts Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Device Accounts Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeviceAccountsManager(DeviceAccountsManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subscriptions Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subscriptions Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubscriptionsManager(SubscriptionsManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rules Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rules Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRulesManager(RulesManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //G37_ControllerSwitch
