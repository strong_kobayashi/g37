/**
 */
package G37.G37_Controller.util;

import G37.G37_Controller.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see G37.G37_Controller.G37_ControllerPackage
 * @generated
 */
public class G37_ControllerAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static G37_ControllerPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public G37_ControllerAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = G37_ControllerPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected G37_ControllerSwitch<Adapter> modelSwitch =
		new G37_ControllerSwitch<Adapter>() {
			@Override
			public Adapter caseG37C(G37C object) {
				return createG37CAdapter();
			}
			@Override
			public Adapter caseUserAccount(UserAccount object) {
				return createUserAccountAdapter();
			}
			@Override
			public Adapter caseEndUser(EndUser object) {
				return createEndUserAdapter();
			}
			@Override
			public Adapter caseServiceProvider(ServiceProvider object) {
				return createServiceProviderAdapter();
			}
			@Override
			public Adapter caseDeviceSupplier(DeviceSupplier object) {
				return createDeviceSupplierAdapter();
			}
			@Override
			public Adapter caseDeviceAccount(DeviceAccount object) {
				return createDeviceAccountAdapter();
			}
			@Override
			public Adapter caseGenericSubscription(GenericSubscription object) {
				return createGenericSubscriptionAdapter();
			}
			@Override
			public Adapter caseServiceSubscription(ServiceSubscription object) {
				return createServiceSubscriptionAdapter();
			}
			@Override
			public Adapter caseSupplierSubscription(SupplierSubscription object) {
				return createSupplierSubscriptionAdapter();
			}
			@Override
			public Adapter caseRoom(Room object) {
				return createRoomAdapter();
			}
			@Override
			public Adapter caseRule(Rule object) {
				return createRuleAdapter();
			}
			@Override
			public Adapter caseEventCondition(EventCondition object) {
				return createEventConditionAdapter();
			}
			@Override
			public Adapter caseUserAccountsManager(UserAccountsManager object) {
				return createUserAccountsManagerAdapter();
			}
			@Override
			public Adapter caseRoomsManager(RoomsManager object) {
				return createRoomsManagerAdapter();
			}
			@Override
			public Adapter caseDeviceAccountsManager(DeviceAccountsManager object) {
				return createDeviceAccountsManagerAdapter();
			}
			@Override
			public Adapter caseSubscriptionsManager(SubscriptionsManager object) {
				return createSubscriptionsManagerAdapter();
			}
			@Override
			public Adapter caseRulesManager(RulesManager object) {
				return createRulesManagerAdapter();
			}
			@Override
			public Adapter caseAction(Action object) {
				return createActionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.G37C <em>G37C</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.G37C
	 * @generated
	 */
	public Adapter createG37CAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.UserAccount <em>User Account</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.UserAccount
	 * @generated
	 */
	public Adapter createUserAccountAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.EndUser <em>End User</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.EndUser
	 * @generated
	 */
	public Adapter createEndUserAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.ServiceProvider <em>Service Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.ServiceProvider
	 * @generated
	 */
	public Adapter createServiceProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.DeviceSupplier <em>Device Supplier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.DeviceSupplier
	 * @generated
	 */
	public Adapter createDeviceSupplierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.DeviceAccount <em>Device Account</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.DeviceAccount
	 * @generated
	 */
	public Adapter createDeviceAccountAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.GenericSubscription <em>Generic Subscription</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.GenericSubscription
	 * @generated
	 */
	public Adapter createGenericSubscriptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.ServiceSubscription <em>Service Subscription</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.ServiceSubscription
	 * @generated
	 */
	public Adapter createServiceSubscriptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.SupplierSubscription <em>Supplier Subscription</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.SupplierSubscription
	 * @generated
	 */
	public Adapter createSupplierSubscriptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.Room
	 * @generated
	 */
	public Adapter createRoomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.Rule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.Rule
	 * @generated
	 */
	public Adapter createRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.EventCondition <em>Event Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.EventCondition
	 * @generated
	 */
	public Adapter createEventConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.UserAccountsManager <em>User Accounts Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.UserAccountsManager
	 * @generated
	 */
	public Adapter createUserAccountsManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.RoomsManager <em>Rooms Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.RoomsManager
	 * @generated
	 */
	public Adapter createRoomsManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.DeviceAccountsManager <em>Device Accounts Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.DeviceAccountsManager
	 * @generated
	 */
	public Adapter createDeviceAccountsManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.SubscriptionsManager <em>Subscriptions Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.SubscriptionsManager
	 * @generated
	 */
	public Adapter createSubscriptionsManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.RulesManager <em>Rules Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.RulesManager
	 * @generated
	 */
	public Adapter createRulesManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link G37.G37_Controller.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see G37.G37_Controller.Action
	 * @generated
	 */
	public Adapter createActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //G37_ControllerAdapterFactory
