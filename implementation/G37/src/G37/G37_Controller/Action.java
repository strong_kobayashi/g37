/**
 */
package G37.G37_Controller;

import org.eclipse.emf.ecore.EObject;
/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.Action#getDa <em>Da</em>}</li>
 *   <li>{@link G37.G37_Controller.Action#getProperty <em>Property</em>}</li>
 *   <li>{@link G37.G37_Controller.Action#getValue <em>Value</em>}</li>
 *   <li>{@link G37.G37_Controller.Action#getDaManager <em>Da Manager</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getAction()
 * @model
 * @generated
 */
public interface Action extends EObject {
	/**
	 * Returns the value of the '<em><b>Da</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Da</em>' reference.
	 * @see #setDa(DeviceAccount)
	 * @see G37.G37_Controller.G37_ControllerPackage#getAction_Da()
	 * @model required="true"
	 * @generated
	 */
	DeviceAccount getDa();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Action#getDa <em>Da</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Da</em>' reference.
	 * @see #getDa()
	 * @generated
	 */
	void setDa(DeviceAccount value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' attribute.
	 * @see #setProperty(String)
	 * @see G37.G37_Controller.G37_ControllerPackage#getAction_Property()
	 * @model required="true"
	 * @generated
	 */
	String getProperty();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Action#getProperty <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' attribute.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see G37.G37_Controller.G37_ControllerPackage#getAction_Value()
	 * @model required="true"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Action#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);
	
	/**
	 * Returns the value of the '<em><b>Da Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Da Manager</em>' reference.
	 * @see #setDaManager(DeviceAccountsManager)
	 * @see G37.G37_Controller.G37_ControllerPackage#getAction_DaManager()
	 * @model required="true"
	 * @generated
	 */
	DeviceAccountsManager getDaManager();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Action#getDaManager <em>Da Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Da Manager</em>' reference.
	 * @see #getDaManager()
	 * @generated
	 */
	void setDaManager(DeviceAccountsManager value);

	// Parse the string and populate the da, property, value fields.
	// Example: 192.168.0.1:8111 "Is On" true get converted to
	// the reference to the device account identified by 192.168.0.1:8111,
	// property string "Is On" (without quotes), value string "true" 
	// (without quotes).
	String initFromActionString(String actionType, String action);
	
	// Execute this action under the given user account
	String execute(UserAccount ua);

} // Action
