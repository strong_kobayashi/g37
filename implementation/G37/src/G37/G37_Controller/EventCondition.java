/**
 */
package G37.G37_Controller;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import g37.dsl.ElementalEventCondition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.EventCondition#getDaManager <em>Da Manager</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getEventCondition()
 * @model
 * @generated
 */
public interface EventCondition extends EObject {
	
	/**
	 * Returns the value of the '<em><b>Da Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Da Manager</em>' reference.
	 * @see #setDaManager(DeviceAccountsManager)
	 * @see G37.G37_Controller.G37_ControllerPackage#getEventCondition_DaManager()
	 * @model required="true"
	 * @generated
	 */
	DeviceAccountsManager getDaManager();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.EventCondition#getDaManager <em>Da Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Da Manager</em>' reference.
	 * @see #getDaManager()
	 * @generated
	 */
	void setDaManager(DeviceAccountsManager value);
	
	void setCallingUser(UserAccount cu);
	
	// Set the members that contain the list of elementalEventConditions and the list of boolean operations in between
	void setElementalConditions(ElementalEventCondition eecFirst, List<ElementalEventCondition> eecList, List<String> opBinaryList);

	// If the even condition is satisfied, return true; false otherwise.
	boolean isSatisfied();
	
	// Evaluate elementary even condition
	boolean evaluateElementaryEC (String devUrl, String propName, boolean negation, String opStrComp, String opFloatComp, String propValue, UserAccount callingUser) throws Exception;
	
} // EventCondition
