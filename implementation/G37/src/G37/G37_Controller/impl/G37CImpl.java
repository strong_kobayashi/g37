/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccount;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.G37C;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.RoomsManager;
import G37.G37_Controller.RulesManager;
import G37.G37_Controller.SubscriptionsManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>G37C</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.G37CImpl#getUaManager <em>Ua Manager</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.G37CImpl#getDaManager <em>Da Manager</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.G37CImpl#getRoomsManager <em>Rooms Manager</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.G37CImpl#getSubscrManager <em>Subscr Manager</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.G37CImpl#getRulesManager <em>Rules Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class G37CImpl extends MinimalEObjectImpl.Container implements G37C {
	
	protected G37_ControllerFactory controllerFactory;
	
	/**
	 * The cached value of the '{@link #getUaManager() <em>Ua Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUaManager()
	 * @generated
	 * @ordered
	 */
	protected UserAccountsManager uaManager;

	/**
	 * The cached value of the '{@link #getDaManager() <em>Da Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDaManager()
	 * @generated
	 * @ordered
	 */
	protected DeviceAccountsManager daManager;

	/**
	 * The cached value of the '{@link #getRoomsManager() <em>Rooms Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomsManager()
	 * @generated
	 * @ordered
	 */
	protected RoomsManager roomsManager;

	/**
	 * The cached value of the '{@link #getSubscrManager() <em>Subscr Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubscrManager()
	 * @generated
	 * @ordered
	 */
	protected SubscriptionsManager subscrManager;

	/**
	 * The cached value of the '{@link #getRulesManager() <em>Rules Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRulesManager()
	 * @generated
	 * @ordered
	 */
	protected RulesManager rulesManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected G37CImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.G37C;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UserAccountsManager getUaManager() {
		if (uaManager != null && uaManager.eIsProxy()) {
			InternalEObject oldUaManager = (InternalEObject)uaManager;
			uaManager = (UserAccountsManager)eResolveProxy(oldUaManager);
			if (uaManager != oldUaManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.G37C__UA_MANAGER, oldUaManager, uaManager));
			}
		}
		return uaManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserAccountsManager basicGetUaManager() {
		return uaManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUaManager(UserAccountsManager newUaManager) {
		UserAccountsManager oldUaManager = uaManager;
		uaManager = newUaManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.G37C__UA_MANAGER, oldUaManager, uaManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeviceAccountsManager getDaManager() {
		if (daManager != null && daManager.eIsProxy()) {
			InternalEObject oldDaManager = (InternalEObject)daManager;
			daManager = (DeviceAccountsManager)eResolveProxy(oldDaManager);
			if (daManager != oldDaManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.G37C__DA_MANAGER, oldDaManager, daManager));
			}
		}
		return daManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceAccountsManager basicGetDaManager() {
		return daManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDaManager(DeviceAccountsManager newDaManager) {
		DeviceAccountsManager oldDaManager = daManager;
		daManager = newDaManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.G37C__DA_MANAGER, oldDaManager, daManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RoomsManager getRoomsManager() {
		if (roomsManager != null && roomsManager.eIsProxy()) {
			InternalEObject oldRoomsManager = (InternalEObject)roomsManager;
			roomsManager = (RoomsManager)eResolveProxy(oldRoomsManager);
			if (roomsManager != oldRoomsManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.G37C__ROOMS_MANAGER, oldRoomsManager, roomsManager));
			}
		}
		return roomsManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomsManager basicGetRoomsManager() {
		return roomsManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRoomsManager(RoomsManager newRoomsManager) {
		RoomsManager oldRoomsManager = roomsManager;
		roomsManager = newRoomsManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.G37C__ROOMS_MANAGER, oldRoomsManager, roomsManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SubscriptionsManager getSubscrManager() {
		if (subscrManager != null && subscrManager.eIsProxy()) {
			InternalEObject oldSubscrManager = (InternalEObject)subscrManager;
			subscrManager = (SubscriptionsManager)eResolveProxy(oldSubscrManager);
			if (subscrManager != oldSubscrManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.G37C__SUBSCR_MANAGER, oldSubscrManager, subscrManager));
			}
		}
		return subscrManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubscriptionsManager basicGetSubscrManager() {
		return subscrManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSubscrManager(SubscriptionsManager newSubscrManager) {
		SubscriptionsManager oldSubscrManager = subscrManager;
		subscrManager = newSubscrManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.G37C__SUBSCR_MANAGER, oldSubscrManager, subscrManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RulesManager getRulesManager() {
		if (rulesManager != null && rulesManager.eIsProxy()) {
			InternalEObject oldRulesManager = (InternalEObject)rulesManager;
			rulesManager = (RulesManager)eResolveProxy(oldRulesManager);
			if (rulesManager != oldRulesManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.G37C__RULES_MANAGER, oldRulesManager, rulesManager));
			}
		}
		return rulesManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RulesManager basicGetRulesManager() {
		return rulesManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRulesManager(RulesManager newRulesManager) {
		RulesManager oldRulesManager = rulesManager;
		rulesManager = newRulesManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.G37C__RULES_MANAGER, oldRulesManager, rulesManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.G37C__UA_MANAGER:
				if (resolve) return getUaManager();
				return basicGetUaManager();
			case G37_ControllerPackage.G37C__DA_MANAGER:
				if (resolve) return getDaManager();
				return basicGetDaManager();
			case G37_ControllerPackage.G37C__ROOMS_MANAGER:
				if (resolve) return getRoomsManager();
				return basicGetRoomsManager();
			case G37_ControllerPackage.G37C__SUBSCR_MANAGER:
				if (resolve) return getSubscrManager();
				return basicGetSubscrManager();
			case G37_ControllerPackage.G37C__RULES_MANAGER:
				if (resolve) return getRulesManager();
				return basicGetRulesManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.G37C__UA_MANAGER:
				setUaManager((UserAccountsManager)newValue);
				return;
			case G37_ControllerPackage.G37C__DA_MANAGER:
				setDaManager((DeviceAccountsManager)newValue);
				return;
			case G37_ControllerPackage.G37C__ROOMS_MANAGER:
				setRoomsManager((RoomsManager)newValue);
				return;
			case G37_ControllerPackage.G37C__SUBSCR_MANAGER:
				setSubscrManager((SubscriptionsManager)newValue);
				return;
			case G37_ControllerPackage.G37C__RULES_MANAGER:
				setRulesManager((RulesManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.G37C__UA_MANAGER:
				setUaManager((UserAccountsManager)null);
				return;
			case G37_ControllerPackage.G37C__DA_MANAGER:
				setDaManager((DeviceAccountsManager)null);
				return;
			case G37_ControllerPackage.G37C__ROOMS_MANAGER:
				setRoomsManager((RoomsManager)null);
				return;
			case G37_ControllerPackage.G37C__SUBSCR_MANAGER:
				setSubscrManager((SubscriptionsManager)null);
				return;
			case G37_ControllerPackage.G37C__RULES_MANAGER:
				setRulesManager((RulesManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.G37C__UA_MANAGER:
				return uaManager != null;
			case G37_ControllerPackage.G37C__DA_MANAGER:
				return daManager != null;
			case G37_ControllerPackage.G37C__ROOMS_MANAGER:
				return roomsManager != null;
			case G37_ControllerPackage.G37C__SUBSCR_MANAGER:
				return subscrManager != null;
			case G37_ControllerPackage.G37C__RULES_MANAGER:
				return rulesManager != null;
		}
		return super.eIsSet(featureID);
	}

	
	public void setup(String args[]) {
		
		// Initialize the static "responses" array in class Resp. If there is an argument, use it as a path to JSON; otherwise, assume JSON in CWD. 
		InputStream inputStream = null;
		try {	URL resource = new URL("file://.\\" + Const.ResponsesJSONdefaultFileName);
				for (int i = 0; i < args.length; i++) {
					if (args[i].toUpperCase().equals("-D")) Resp.setDebug(); // -D on the command line means we are in debug mode.
					else {
						resource = new URL(args[i] + "\\" + Const.ResponsesJSONdefaultFileName);
						inputStream = resource.openConnection().getInputStream();
					}
				} 
		} 
		catch (MalformedURLException e) {
				e.printStackTrace();
		} 
		catch (IOException e) {
				e.printStackTrace();
		}
		Resp.responses = Resp.loadResponsesFromJSON(inputStream); 
				
		controllerFactory = G37_ControllerFactory.eINSTANCE;
		// Initialize the managers.  The order is important!
		uaManager = controllerFactory.createUserAccountsManager();
		uaManager.setControllerFactory(controllerFactory);
		roomsManager = controllerFactory.createRoomsManager();
		roomsManager.setControllerFactory(controllerFactory);
		daManager = controllerFactory.createDeviceAccountsManager();
		daManager.setControllerFactory(controllerFactory);
		daManager.setRoomsManager(roomsManager);
		// Subscriptions manager
		subscrManager = controllerFactory.createSubscriptionsManager();
		subscrManager.setControllerFactory(controllerFactory);
		subscrManager.setUserAccountsManager(uaManager);
		subscrManager.setRoomsManager(roomsManager);
		subscrManager.setUserAccountsManager(uaManager);
		daManager.setSubscrManager(subscrManager);
		// Rules manager
		rulesManager = controllerFactory.createRulesManager();
		rulesManager.setControllerFactory(controllerFactory);
		rulesManager.setDaManager(daManager);
	}
	
	// The 2 versions of the doCommand method execute a command (provided in 2 possible forms) by calling the appropriate command of the appropriate manager.
	public String doCommand(String command, String... args) {
		UserAccount user;
		DeviceAccount da;
		if (command.isEmpty()) return Resp.response(R_CODE.E_EMPTYCOMMANDSTRING);
		switch (command.toUpperCase()) {
			case "ADDUSER" : 
				if (args.length < 4) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return uaManager.addUser(args[0], args[1], args[2], args[3]);
			case "LSUSERS" :
				return uaManager.lsUsers();
			case "RMUSER" :
				if (args.length < 1) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return uaManager.rmUser(args[0]);
			case "ADDROOM" :
				if (args.length < 1) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return roomsManager.addRoom(args[0]);
			case "LSROOMS" :
				return roomsManager.lsRooms();
			case "RMROOM" :
				if (args.length < 1) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return roomsManager.rmRoom(args[0]);
			case "REG" :
				if (args.length < 2) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return daManager.reg(args[0], args[1]);
			case "LSDEV" :
				if (args.length < 2) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				user = uaManager.getUserAccountByUserName(args[1]);
				return daManager.lsDev(args[0], user);
			case "UNREG" :
				if (args.length < 1) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return daManager.unreg(args[0]);
			case "MV" :
				if (args.length < 2) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return daManager.mv(args[0], args[1]);
			case "ADDSUBSCR" :
				if (args.length < 4) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return subscrManager.addSubscr(args[0], args[1], args[2], args[3]);
			case "LSSUBSCR" :
				if (args.length < 1) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				user = uaManager.getUserAccountByUserName(args[0]);
				return 	subscrManager.lsSubscr(user);
			case "RMSUBSCR" :
				if (args.length < 1) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				return subscrManager.rmSubscr(args[0]);
			case "ADDRULE" :
				if (args.length < 3) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				if (args.length == 3) {
					user = uaManager.getUserAccountByUserName(args[2]);
					return rulesManager.addRule(args[0], args[1], user);
				}
				else {
					user = uaManager.getUserAccountByUserName(args[3]);
					return rulesManager.addRule(args[0], args[1], args[2], user);
				}
			case "LSRULES" :
				if (args.length < 1) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				user = uaManager.getUserAccountByUserName(args[0]);
				return rulesManager.lsRules(user);
			case "RMRULE" :
				if (args.length < 2) return Resp.response(R_CODE.E_INTWRONGNUMARG, command);
				user = uaManager.getUserAccountByUserName(args[1]);
				return rulesManager.rmRule(args[0], user);
			case "PEEK" :
				if (args.length < 3) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				user = uaManager.getUserAccountByUserName(args[2]);
				da = daManager.getDeviceAccountByURL(args[0]);
				return daManager.peek(da, args[1], user);
			case "POKE" :
				if (args.length < 4) return Resp.response(R_CODE.E_INTWRONGNUMARG, command); 
				user = uaManager.getUserAccountByUserName(args[3]);
				da = daManager.getDeviceAccountByURL(args[0]);
				return daManager.poke(da, args[1], args[2], user);
			case "END" : case "SHUTDOWN" :
				Resp.log("=== G37 controller shutdown ===");
				System.exit(0);
			default :
				return Resp.response(R_CODE.E_INTINVCOMMAND, command);
		}
	}
	
	//Arguments that have spaces must be in DOUBLE quotes!  
	// But fragments INSIDE arguments that have spaces, e.g. property name Is On, must be in SINGLE quotes. 
	public String doCommand(String commandWithArgs) {
		if (commandWithArgs.isEmpty()) return Resp.response(R_CODE.E_EMPTYCOMMANDSTRING);
		ArrayList<String> tokens = new ArrayList<String>();
		int i = 0;
		commandWithArgs = commandWithArgs.trim();
		// Parse the command string into a list of tokens.
		Pattern pattern = Pattern.compile(Const.CLIparsingPatternWithDoubleQuotes);
		Matcher matcher = pattern.matcher(commandWithArgs);
		while (matcher.find()) tokens.add(i++, Resp.trimString(matcher.group(), "\"").trim());
		// Debug-print the original string and the tokens.
		Resp.log("Received request to execute string <" + commandWithArgs + ">, parsed into:", true);
		for (String s : tokens) Resp.log(s, true);
		// Process one special case with AddRule command, because it has 2 signatures for compatibility and this may create an error at the bottom of this method.
		if (tokens.get(0).toUpperCase().equals("ADDRULE") && i == 4) return doCommand (tokens.get(0), tokens.get(1), tokens.get(2), tokens.get(3));
		// Fill the remainder of the tokens array so we don't get an out-of-bound error below at the very end of this method.
		for (;i < 5;i++) tokens.add(i, "");
		
		// Use the other form of this method to do the actual job.
		return doCommand (tokens.get(0), tokens.get(1), tokens.get(2), tokens.get(3), tokens.get(4)); // We don't have commands with more than 4 arguments
	}
	
	// This method executes all rules. Returns OK if all executed without errors, or the first error otherwise.
	public String executeAllRules() {
		return rulesManager.executeAll();
	}
	
	// This method provides controller's main loop for the _fallback_scenario_ of using sockets for user<->controller communication.
	public void RunOnSockets(int portNumber) {
		String reply = "", inputLine;
		Resp.log("\nListening on port "+portNumber+"...");
		while (true) {  // Device's main loop.
        	// Now see if there is a command and if yes, process it.
			try (
		            ServerSocket serverSocket = new ServerSocket(portNumber);
		            Socket clientSocket = serverSocket.accept();     
		            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);  
		            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		        ) {
		            while ((inputLine = in.readLine()) != null) {
		            	reply = doCommand(inputLine);
		                // Return result.
		                out.println(reply);
		                // Do housework.
		                executeAllRules();
		                }
		        } catch (IOException e) {
		            System.err.println("Exception caught when trying to listen on port " + portNumber + " or listening for a connection\n" + e.getMessage());
		        }			
		}
	} // RunOnSockets
	
} //G37CImpl
