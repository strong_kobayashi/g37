/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.Const;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccount;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.EventCondition;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.UserAccount;
import g37.dsl.ElementalEventCondition;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.EventConditionImpl#getDaManager <em>Da Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventConditionImpl extends MinimalEObjectImpl.Container implements EventCondition {
	/**
	 * The cached value of the '{@link #getDaManager() <em>Da Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDaManager()
	 * @generated
	 * @ordered
	 */
	protected DeviceAccountsManager daManager;
	
	protected UserAccount callingUser;
	protected ElementalEventCondition eecFirst;
	protected List<ElementalEventCondition> eecList;
	protected List<String> opBinaryList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.EVENT_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeviceAccountsManager getDaManager() {
		if (daManager != null && daManager.eIsProxy()) {
			InternalEObject oldDaManager = (InternalEObject)daManager;
			daManager = (DeviceAccountsManager)eResolveProxy(oldDaManager);
			if (daManager != oldDaManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.EVENT_CONDITION__DA_MANAGER, oldDaManager, daManager));
			}
		}
		return daManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceAccountsManager basicGetDaManager() {
		return daManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDaManager(DeviceAccountsManager newDaManager) {
		DeviceAccountsManager oldDaManager = daManager;
		daManager = newDaManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.EVENT_CONDITION__DA_MANAGER, oldDaManager, daManager));
	}
	
	public void setCallingUser (UserAccount cu) {
		callingUser = cu;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.EVENT_CONDITION__DA_MANAGER:
				if (resolve) return getDaManager();
				return basicGetDaManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.EVENT_CONDITION__DA_MANAGER:
				setDaManager((DeviceAccountsManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.EVENT_CONDITION__DA_MANAGER:
				setDaManager((DeviceAccountsManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.EVENT_CONDITION__DA_MANAGER:
				return daManager != null;
		}
		return super.eIsSet(featureID);
	}

	public void setElementalConditions (ElementalEventCondition first, List<ElementalEventCondition> list1, List<String> list2) {
		eecFirst = first;
		eecList = list1;
		opBinaryList = list2;
	};
	
	// If the even condition is satisfied, return true; false otherwise.
	public boolean isSatisfied() {
		boolean result = false;
		int i = 0;
		try {
			Resp.log("  Evaluating " + eecFirst + ".... ", true);
			result = evaluateElementaryEC(eecFirst.getDevUrl(), eecFirst.getPropName(), eecFirst.isNegation(), eecFirst.getOpStrComp(), eecFirst.getOpFloatComp(), eecFirst.getPropValue(), callingUser);
			Resp.log("  " + result, true);
		} catch (Exception e) {
			System.err.println("Problem evaluating: " + e.getMessage() + " cause: " + e.getCause());
			return false;
		};
		for (ElementalEventCondition eec : eecList) {
			Resp.log("  Evaluating " + eec + ".... ", true);
			boolean eecResult;
			try {
				eecResult = evaluateElementaryEC(eec.getDevUrl(), eec.getPropName(), eec.isNegation(), eec.getOpStrComp(), eec.getOpFloatComp(), eec.getPropValue(), callingUser);
			} catch (Exception e) {
				System.err.println("Problem evaluating: " + e.getMessage() + " cause: " + e.getCause());
				return false;
			};
			Resp.log("  " + eecResult, true);
			String opBinary = opBinaryList.get(i++).toUpperCase();
			switch (opBinary) {
			case "AND" :
				Resp.log("      = " + result + " && " + eecResult, true); 
				result = result && eecResult; 
				break;
			case "OR" :
				Resp.log("      = " + result + " || " + eecResult, true); 
				result = result || eecResult;
				break;
			default :
				System.err.println("Unhandled binary operation type " + opBinary);
				return false;
			}
		}
		return result;  
	}
	
	// Evaluate elementary even condition
	public boolean evaluateElementaryEC (String devUrl, String propName, boolean negation, String opStrComp, String opFloatComp, String propValue, UserAccount user) throws Exception {
		DeviceAccount da = null;
		float left, right;
		String r  = "n/a";
		
		// This line of code is needed to support our testing processes. It can eventually be deleted.
		if (devUrl.equals(Const.DevForTestingPurposes)) return propName.equals(propValue);
		
		if (daManager == null) throw new Exception("Can't find Device Accounts Manager in evaluateElementaryEC");
		da = daManager.getDeviceAccountByURL(devUrl);
		if (da == null) throw new Exception("Can't find device in evaluateElementaryEC: " + devUrl); 
		r = daManager.peek(da, propName, user);
		// If opStrComp is not null, we're comparing two strings, otherwise two floats
		if (opStrComp != null) {
			switch (opStrComp) {
				case "==" :
					return negation ^ r.equals(propValue);
				case "contains" :
					return negation ^ (r.contains(propValue));
				default :
					throw new Exception("Invalid string comparison operation in evaluateElementaryEC: " + opStrComp);
			}
		}
		else if (opFloatComp != null) {
			switch (opFloatComp) {
				case "==" :
					left = Float.parseFloat(r);
					right = Float.parseFloat(propValue);
					return negation ^ (left == right);
				case ">" : 
					left = Float.parseFloat(r);  
					right = Float.parseFloat(propValue);
					return negation ^ (left > right);
				case "<" :
					left = Float.parseFloat(r);
					right = Float.parseFloat(propValue);
					return negation ^ (left < right);
				case ">=" :  
					left = Float.parseFloat(r);
					right = Float.parseFloat(propValue);
					return negation ^ (left >= right);
				case "<=" :  
					left = Float.parseFloat(r);
					right = Float.parseFloat(propValue);
					return negation ^ (left <= right);
				default :
					throw new Exception("Invalid float comparison operation in evaluateElementaryEC: " + opFloatComp);
			}	
		}
		else throw new Exception("Both comparison operations passed to evaluateElementaryEC are null");
	}	
} //EventConditionImpl
