/**
 */
package G37.G37_Controller.impl;

import G37.G37_Controller.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class G37_ControllerFactoryImpl extends EFactoryImpl implements G37_ControllerFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static G37_ControllerFactory init() {
		try {
			G37_ControllerFactory theG37_ControllerFactory = (G37_ControllerFactory)EPackage.Registry.INSTANCE.getEFactory(G37_ControllerPackage.eNS_URI);
			if (theG37_ControllerFactory != null) {
				return theG37_ControllerFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new G37_ControllerFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public G37_ControllerFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case G37_ControllerPackage.G37C: return createG37C();
			case G37_ControllerPackage.END_USER: return createEndUser();
			case G37_ControllerPackage.SERVICE_PROVIDER: return createServiceProvider();
			case G37_ControllerPackage.DEVICE_SUPPLIER: return createDeviceSupplier();
			case G37_ControllerPackage.DEVICE_ACCOUNT: return createDeviceAccount();
			case G37_ControllerPackage.SERVICE_SUBSCRIPTION: return createServiceSubscription();
			case G37_ControllerPackage.SUPPLIER_SUBSCRIPTION: return createSupplierSubscription();
			case G37_ControllerPackage.ROOM: return createRoom();
			case G37_ControllerPackage.RULE: return createRule();
			case G37_ControllerPackage.EVENT_CONDITION: return createEventCondition();
			case G37_ControllerPackage.USER_ACCOUNTS_MANAGER: return createUserAccountsManager();
			case G37_ControllerPackage.ROOMS_MANAGER: return createRoomsManager();
			case G37_ControllerPackage.DEVICE_ACCOUNTS_MANAGER: return createDeviceAccountsManager();
			case G37_ControllerPackage.SUBSCRIPTIONS_MANAGER: return createSubscriptionsManager();
			case G37_ControllerPackage.RULES_MANAGER: return createRulesManager();
			case G37_ControllerPackage.ACTION: return createAction();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public G37C createG37C() {
		G37CImpl g37C = new G37CImpl();
		return g37C;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EndUser createEndUser() {
		EndUserImpl endUser = new EndUserImpl();
		return endUser;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceProvider createServiceProvider() {
		ServiceProviderImpl serviceProvider = new ServiceProviderImpl();
		return serviceProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeviceSupplier createDeviceSupplier() {
		DeviceSupplierImpl deviceSupplier = new DeviceSupplierImpl();
		return deviceSupplier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeviceAccount createDeviceAccount() {
		DeviceAccountImpl deviceAccount = new DeviceAccountImpl();
		return deviceAccount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceSubscription createServiceSubscription() {
		ServiceSubscriptionImpl serviceSubscription = new ServiceSubscriptionImpl();
		return serviceSubscription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SupplierSubscription createSupplierSubscription() {
		SupplierSubscriptionImpl supplierSubscription = new SupplierSubscriptionImpl();
		return supplierSubscription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Room createRoom() {
		RoomImpl room = new RoomImpl();
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Rule createRule() {
		RuleImpl rule = new RuleImpl();
		return rule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EventCondition createEventCondition() {
		EventConditionImpl eventCondition = new EventConditionImpl();
		return eventCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UserAccountsManager createUserAccountsManager() {
		UserAccountsManagerImpl userAccountsManager = new UserAccountsManagerImpl();
		return userAccountsManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RoomsManager createRoomsManager() {
		RoomsManagerImpl roomsManager = new RoomsManagerImpl();
		return roomsManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeviceAccountsManager createDeviceAccountsManager() {
		DeviceAccountsManagerImpl deviceAccountsManager = new DeviceAccountsManagerImpl();
		return deviceAccountsManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SubscriptionsManager createSubscriptionsManager() {
		SubscriptionsManagerImpl subscriptionsManager = new SubscriptionsManagerImpl();
		return subscriptionsManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RulesManager createRulesManager() {
		RulesManagerImpl rulesManager = new RulesManagerImpl();
		return rulesManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Action createAction() {
		ActionImpl action = new ActionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public G37_ControllerPackage getG37_ControllerPackage() {
		return (G37_ControllerPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static G37_ControllerPackage getPackage() {
		return G37_ControllerPackage.eINSTANCE;
	}

} //G37_ControllerFactoryImpl
