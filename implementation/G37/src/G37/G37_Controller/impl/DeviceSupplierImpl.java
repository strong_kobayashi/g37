/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.Const;
import G37.G37_Controller.DeviceSupplier;
import G37.G37_Controller.G37_ControllerPackage;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Device Supplier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DeviceSupplierImpl extends UserAccountImpl implements DeviceSupplier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceSupplierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.DEVICE_SUPPLIER;
	}

	public String getUserType() {
		return Const.DS;
	}
	
} //DeviceSupplierImpl
