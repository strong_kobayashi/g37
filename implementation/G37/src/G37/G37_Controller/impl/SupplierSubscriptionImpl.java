/**
 */
package G37.G37_Controller.impl;

import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.SupplierSubscription;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Supplier Subscription</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SupplierSubscriptionImpl extends GenericSubscriptionImpl implements SupplierSubscription {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SupplierSubscriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.SUPPLIER_SUBSCRIPTION;
	}

} //SupplierSubscriptionImpl
