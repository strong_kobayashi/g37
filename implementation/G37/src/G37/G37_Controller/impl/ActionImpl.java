/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.Action;
import G37.G37_Controller.DeviceAccount;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.ACTION_TYPE;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.regex.Matcher;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.ActionImpl#getDa <em>Da</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.ActionImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.ActionImpl#getValue <em>Value</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.ActionImpl#getDaManager <em>Da Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActionImpl extends MinimalEObjectImpl.Container implements Action {
	/**
	 * The cached value of the '{@link #getDa() <em>Da</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDa()
	 * @generated
	 * @ordered
	 */
	protected DeviceAccount da;

	/**
	 * The default value of the '{@link #getProperty() <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected static final String PROPERTY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected String property = PROPERTY_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;
	
	/**
	 * The cached value of the '{@link #getDaManager() <em>Da Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDaManager()
	 * @generated
	 * @ordered
	 */
	protected DeviceAccountsManager daManager;

	protected ACTION_TYPE type;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeviceAccount getDa() {
		if (da != null && da.eIsProxy()) {
			InternalEObject oldDa = (InternalEObject)da;
			da = (DeviceAccount)eResolveProxy(oldDa);
			if (da != oldDa) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.ACTION__DA, oldDa, da));
			}
		}
		return da;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceAccount basicGetDa() {
		return da;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDa(DeviceAccount newDa) {
		DeviceAccount oldDa = da;
		da = newDa;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.ACTION__DA, oldDa, da));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProperty(String newProperty) {
		String oldProperty = property;
		property = newProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.ACTION__PROPERTY, oldProperty, property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.ACTION__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeviceAccountsManager getDaManager() {
		if (daManager != null && daManager.eIsProxy()) {
			InternalEObject oldDaManager = (InternalEObject)daManager;
			daManager = (DeviceAccountsManager)eResolveProxy(oldDaManager);
			if (daManager != oldDaManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.ACTION__DA_MANAGER, oldDaManager, daManager));
			}
		}
		return daManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceAccountsManager basicGetDaManager() {
		return daManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDaManager(DeviceAccountsManager newDaManager) {
		DeviceAccountsManager oldDaManager = daManager;
		daManager = newDaManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.ACTION__DA_MANAGER, oldDaManager, daManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.ACTION__DA:
				if (resolve) return getDa();
				return basicGetDa();
			case G37_ControllerPackage.ACTION__PROPERTY:
				return getProperty();
			case G37_ControllerPackage.ACTION__VALUE:
				return getValue();
			case G37_ControllerPackage.ACTION__DA_MANAGER:
				if (resolve) return getDaManager();
				return basicGetDaManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.ACTION__DA:
				setDa((DeviceAccount)newValue);
				return;
			case G37_ControllerPackage.ACTION__PROPERTY:
				setProperty((String)newValue);
				return;
			case G37_ControllerPackage.ACTION__VALUE:
				setValue((String)newValue);
				return;
			case G37_ControllerPackage.ACTION__DA_MANAGER:
				setDaManager((DeviceAccountsManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.ACTION__DA:
				setDa((DeviceAccount)null);
				return;
			case G37_ControllerPackage.ACTION__PROPERTY:
				setProperty(PROPERTY_EDEFAULT);
				return;
			case G37_ControllerPackage.ACTION__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case G37_ControllerPackage.ACTION__DA_MANAGER:
				setDaManager((DeviceAccountsManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.ACTION__DA:
				return da != null;
			case G37_ControllerPackage.ACTION__PROPERTY:
				return PROPERTY_EDEFAULT == null ? property != null : !PROPERTY_EDEFAULT.equals(property);
			case G37_ControllerPackage.ACTION__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case G37_ControllerPackage.ACTION__DA_MANAGER:
				return daManager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (property: ");
		result.append(property);
		result.append(", value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}
	
	// Parse the string and populate the da, property, value fields.
	// Example: 192.168.0.1:8111 "Is On" true get converted to
	// the reference to the device account identified by 192.168.0.1:8111,
	// property string "Is On" (without quotes), value string "true" 
	// (without quotes).
	public String initFromActionString(String actionType, String action) {
		ArrayList<String> tokens = new ArrayList<String>();
		int i = 0;
		action = action.trim();
		action = action.startsWith("[") ? action.substring(1, action.length()) : action;  // Remove leading [
		action = action.endsWith("]") ? action.substring(0, action.length()-1) : action;  // Remove trailing ]
		// Determine the type.
		if (ACTION_TYPE.valueOf(actionType)==ACTION_TYPE.POKE) {
			type = ACTION_TYPE.POKE;
			// Parse the action string into a list of tokens.
			Pattern pattern = Pattern.compile(Const.CLIparsingPatternWithSingleQuotes);
			Matcher matcher = pattern.matcher(action);
			while (matcher.find()) tokens.add(Resp.trimString(matcher.group(),"'"));
			// Set the device account, property, value members.
			da = daManager.getDeviceAccountByURL(tokens.get(0));
			if (da == null)	return Resp.response(R_CODE.E_CANTPARSEACTION, action + ", reason: no such device " + tokens.get(0));	
			property = tokens.get(1);
			if (property.isEmpty())	return Resp.response(R_CODE.E_CANTPARSEACTION, action + ", reason: empty property name");
			value = tokens.get(2);	
			return Resp.response(R_CODE.OK);
		}
		if (ACTION_TYPE.valueOf(actionType)==ACTION_TYPE.EMAIL) {
			type = ACTION_TYPE.EMAIL;
			property = "This value should not ever be used. If you see it pop up anywhere other than the initial assignment, something is wrong.";
			value = action;
			return Resp.response(R_CODE.OK);
		}
		else return Resp.response(R_CODE.E_INTUNKNOWNACTIONTYPE, actionType);
		
	}
	
	// Execute this action under the given user account
	public String execute(UserAccount ua) {
		if (type == ACTION_TYPE.POKE) {
			Resp.log("  Action: POKING to device " + da + " property " + property + " value " + value, true);
			return daManager.poke(da, property, value, ua);
		}
		else {
			Resp.log("  Action: Sending an email to " + ua.getUsername() + " with the text \"" + value + "\"");
			return Resp.response(R_CODE.OK);
		}
	}

} //ActionImpl
