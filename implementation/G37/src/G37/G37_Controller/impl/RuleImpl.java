/**
 */
package G37.G37_Controller.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.Action;
import G37.G37_Controller.EventCondition;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.Rule;
import G37.G37_Controller.UserAccount;
import g37.dsl.Model;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.RuleImpl#getId <em>Id</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.RuleImpl#getEventCondition <em>Event Condition</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.RuleImpl#getAction <em>Action</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.RuleImpl#getCreator <em>Creator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RuleImpl extends MinimalEObjectImpl.Container implements Rule {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEventCondition() <em>Event Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventCondition()
	 * @generated
	 * @ordered
	 */
	protected EventCondition eventCondition;

	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected Action action;

	/**
	 * The cached value of the '{@link #getCreator() <em>Creator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreator()
	 * @generated
	 * @ordered
	 */
	protected UserAccount creator;
	
	// The parsed DSL model of this rule
	protected Model ecaAST;
	
	// The ECA string of this rule, just for the listing
	protected String eca;

	protected RuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.RULE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EventCondition getEventCondition() {
		return eventCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEventCondition(EventCondition newEventCondition, NotificationChain msgs) {
		EventCondition oldEventCondition = eventCondition;
		eventCondition = newEventCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.RULE__EVENT_CONDITION, oldEventCondition, newEventCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEventCondition(EventCondition newEventCondition) {
		if (newEventCondition != eventCondition) {
			NotificationChain msgs = null;
			if (eventCondition != null)
				msgs = ((InternalEObject)eventCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - G37_ControllerPackage.RULE__EVENT_CONDITION, null, msgs);
			if (newEventCondition != null)
				msgs = ((InternalEObject)newEventCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - G37_ControllerPackage.RULE__EVENT_CONDITION, null, msgs);
			msgs = basicSetEventCondition(newEventCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.RULE__EVENT_CONDITION, newEventCondition, newEventCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Action getAction() {
		if (action != null && action.eIsProxy()) {
			InternalEObject oldAction = (InternalEObject)action;
			action = (Action)eResolveProxy(oldAction);
			if (action != oldAction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.RULE__ACTION, oldAction, action));
			}
		}
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action basicGetAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAction(Action newAction) {
		Action oldAction = action;
		action = newAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.RULE__ACTION, oldAction, action));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UserAccount getCreator() {
		if (creator != null && creator.eIsProxy()) {
			InternalEObject oldCreator = (InternalEObject)creator;
			creator = (UserAccount)eResolveProxy(oldCreator);
			if (creator != oldCreator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.RULE__CREATOR, oldCreator, creator));
			}
		}
		return creator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserAccount basicGetCreator() {
		return creator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCreator(UserAccount newCreator) {
		UserAccount oldCreator = creator;
		creator = newCreator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.RULE__CREATOR, oldCreator, creator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case G37_ControllerPackage.RULE__EVENT_CONDITION:
				return basicSetEventCondition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.RULE__ID:
				return getId();
			case G37_ControllerPackage.RULE__EVENT_CONDITION:
				return getEventCondition();
			case G37_ControllerPackage.RULE__ACTION:
				if (resolve) return getAction();
				return basicGetAction();
			case G37_ControllerPackage.RULE__CREATOR:
				if (resolve) return getCreator();
				return basicGetCreator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.RULE__ID:
				setId((String)newValue);
				return;
			case G37_ControllerPackage.RULE__EVENT_CONDITION:
				setEventCondition((EventCondition)newValue);
				return;
			case G37_ControllerPackage.RULE__ACTION:
				setAction((Action)newValue);
				return;
			case G37_ControllerPackage.RULE__CREATOR:
				setCreator((UserAccount)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.RULE__ID:
				setId(ID_EDEFAULT);
				return;
			case G37_ControllerPackage.RULE__EVENT_CONDITION:
				setEventCondition((EventCondition)null);
				return;
			case G37_ControllerPackage.RULE__ACTION:
				setAction((Action)null);
				return;
			case G37_ControllerPackage.RULE__CREATOR:
				setCreator((UserAccount)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.RULE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case G37_ControllerPackage.RULE__EVENT_CONDITION:
				return eventCondition != null;
			case G37_ControllerPackage.RULE__ACTION:
				return action != null;
			case G37_ControllerPackage.RULE__CREATOR:
				return creator != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}
	
	// Get /Set the ECA string, which is used only for listing (lsrules)
	public void setEca(String s) {
		eca = s;
	};
	
	public String getEca() {
		return eca;
	};
	
	public String initFromDSLmodel (Model ast) {
		eventCondition.setElementalConditions(ast.getFirst(), ast.getAdditional(), ast.getOpBinary());
		eventCondition.setCallingUser(creator);
		return action.initFromActionString(ast.getAction(), ast.getArg());
	}
	
	// Execute the rule.
	public String execute() {
		Resp.log("Executing rule " + id, true);
		if (eventCondition.isSatisfied()) return action.execute(creator);
		return Resp.response(R_CODE.OK); // If the event condition is not satisfied, this is an OK situation
	}

} //RuleImpl
