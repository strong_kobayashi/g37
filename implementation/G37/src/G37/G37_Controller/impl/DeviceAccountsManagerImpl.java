/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccount;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.EndUser;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.Room;
import G37.G37_Controller.RoomsManager;
import G37.G37_Controller.SubscriptionsManager;
import G37.G37_Controller.UserAccount;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Device Accounts Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.DeviceAccountsManagerImpl#getDeviceAccounts <em>Device Accounts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeviceAccountsManagerImpl extends MinimalEObjectImpl.Container implements DeviceAccountsManager {
	/**
	 * The cached value of the '{@link #getDeviceAccounts() <em>Device Accounts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceAccounts()
	 * @generated
	 * @ordered
	 */
	protected static EList<DeviceAccount> deviceAccounts;
	
	private G37_ControllerFactory controllerFactory;
	private RoomsManager roomsManager;
	private SubscriptionsManager subscrManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceAccountsManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.DEVICE_ACCOUNTS_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DeviceAccount> getDeviceAccounts() {
		if (deviceAccounts == null) {
			deviceAccounts = new EObjectContainmentEList<DeviceAccount>(DeviceAccount.class, this, G37_ControllerPackage.DEVICE_ACCOUNTS_MANAGER__DEVICE_ACCOUNTS);
		}
		return deviceAccounts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case G37_ControllerPackage.DEVICE_ACCOUNTS_MANAGER__DEVICE_ACCOUNTS:
				return ((InternalEList<?>)getDeviceAccounts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.DEVICE_ACCOUNTS_MANAGER__DEVICE_ACCOUNTS:
				return getDeviceAccounts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.DEVICE_ACCOUNTS_MANAGER__DEVICE_ACCOUNTS:
				getDeviceAccounts().clear();
				getDeviceAccounts().addAll((Collection<? extends DeviceAccount>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.DEVICE_ACCOUNTS_MANAGER__DEVICE_ACCOUNTS:
				getDeviceAccounts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.DEVICE_ACCOUNTS_MANAGER__DEVICE_ACCOUNTS:
				return deviceAccounts != null && !deviceAccounts.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	// Sets the controllerFactory member of this object.
	public void setControllerFactory(G37_ControllerFactory cf) {
		controllerFactory = cf;
	}
	
	// Sets the roomsManager member of this object.
	public void setRoomsManager(RoomsManager rMan) {
		roomsManager = rMan;
	}
	
	// Set the subscrManager member of this object.
	public void setSubscrManager(SubscriptionsManager sM) {
		subscrManager = sM;
	}

	// Returns the DeviceAccount object as identified by the device account's URL.
	public DeviceAccount getDeviceAccountByURL(String url) {
		DeviceAccount deviceAccount = null;
		for (DeviceAccount da : getDeviceAccounts()) {
			if (da.getUrl().equals(url)) {
	            deviceAccount = da;
	            break;
			}
		}
		return deviceAccount;
	}
	
	// Implementation of reg command.  Adds a new device account to our home automaton controller.
	public String reg(String devUrl, String roomID) {
		if (devUrl == "") return Resp.response(R_CODE.E_URLEMPTY);
		
		// Check if such device already exists
		DeviceAccount da = getDeviceAccountByURL(devUrl);
		if (da != null) return Resp.response(R_CODE.E_URLEXISTS, devUrl);
		
		// Check the destination room
		Room room = roomsManager.getRoomByID(roomID);
		if (room != null) { // Then create a new device account
			da = controllerFactory.createDeviceAccount();
			da.setUrl(devUrl);
			da.setRoom(room);
			getDeviceAccounts().add(da);
			return Resp.response(R_CODE.OK);
		}
		else return Resp.response(R_CODE.E_ROOMNOTFOUND, roomID);
	}
	
	// Implementation of lsDev command. Lists the devices (all, or in a given room).
	public String lsDev(String roomID, UserAccount callingUser) { 
		String result = "";
		Room room = null;
		int i = 1;
		
		// If we want to list devices in a particular room, then let's find it first
		if (!roomID.isEmpty()) {
			room = roomsManager.getRoomByID(roomID);
			if (room == null) return Resp.response(R_CODE.E_ROOMNOTFOUND, roomID);
		}
		
		// Build the list
		for (DeviceAccount da : getDeviceAccounts()) {
			// IF (we're listing devices in all rooms anyway, OR this is the right room) 
			// AND the calling user has the right to see this device, 
			// THEN add this device to the listing.
			if ((roomID.isEmpty() || da.getRoom().equals(room)) 
			   && (callingUser instanceof EndUser || subscrManager.subscriptionExists(da, callingUser)))
	            result += "Device " + i++ + "\t" + da.getUrl() + "\t" + da.getRoom().getId() + "\n";
		}
		
		return result;
	}
	
	// Implementation of unreg command.  Deletes a device account with the given URL.
	public String unreg(String devUrl) {
		DeviceAccount deviceAccount = getDeviceAccountByURL(devUrl);
		if (deviceAccount != null) {
			getDeviceAccounts().remove(deviceAccount);
			return Resp.response(R_CODE.OK);
		}
		return Resp.response(R_CODE.E_URLNOTFOUND, devUrl);
	}
	
	// Implementation of mv command. Reassigns the device to another room.
	public String mv(String devUrl, String roomID) {
		// Find the destination room first.
		Room room = roomsManager.getRoomByID(roomID);
		if (room == null) return Resp.response(R_CODE.E_ROOMNOTFOUND, roomID);
		//	Find the device account.
		DeviceAccount deviceAccount = getDeviceAccountByURL(devUrl);
		if (deviceAccount == null) return Resp.response(R_CODE.E_URLNOTFOUND, devUrl);
		deviceAccount.setRoom(room);
		return Resp.response(R_CODE.OK);
	}
		
	// This method sends a string to the device as identified by its device account.
	public String sendStringToG37D(DeviceAccount devAcc, String strToSend) {
		String reply = null;
		URL devUrl = null;
		try {
			devUrl = new URL("http://" + devAcc.getUrl()); // Adding http:// is a bit of a kludge
		} catch (MalformedURLException e) {
			System.err.println(Resp.response(R_CODE.E_BADURL, devAcc.getUrl()));
			//e1.printStackTrace();
			return "Error";
		}
		int portNumber = devUrl.getPort();
		// If no port provided in the device URL, fall back to the default 8081.
		portNumber = (portNumber != -1) ? portNumber : 8081;
		String host = devUrl.getHost();
		
		try (
	            Socket socket = new Socket(host, portNumber);
	            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	        ) {
	                out.println(strToSend);
	                reply = in.readLine();
	        } catch (UnknownHostException e) {
	            System.err.println(Resp.response(R_CODE.E_HOSTNOTFOUND, host));
	            return "Error - host not found";
	        } catch (IOException e) {
	            System.err.println(Resp.response(R_CODE.E_NOCONNECT, devAcc.getUrl()));
	            return "Error - no connection";
	        } 
		return reply;
	}
	
	/* ****************************************************************************************************************
	 * ***************** P E E K   and   P O K E **********************************************************************
	 * ****************************************************************************************************************
	 */
	
	// Implementation of peek command.  Looks up a property.
	public String peek(DeviceAccount da, String prop, UserAccount callingUser) {
		
		// This line of code is needed to support our testing processes. It can eventually be deleted.
		if (da.getUrl().equals(Const.DevForTestingPurposes)) {
			if (prop.contentEquals(Const.NonExistentPropertyForTestingPurposes)) return Resp.response(R_CODE.E_PROPNOTFOUND, prop);
			else return prop;
		}

		if (callingUser instanceof EndUser || 
				subscrManager.subscriptionExists(da, callingUser)) 
			return sendStringToG37D(da, "PEEK" + Const.DivCh + prop + Const.DivCh + callingUser.getUserType());
		else return Resp.response(R_CODE.E_ACCESSDENIEDNOSUBSCR, da.getUrl() + " user " + callingUser.getUsername());
	}
	
	// Implementation of peek command.  Looks up a property.
	public String poke(DeviceAccount da, String prop, String val, UserAccount callingUser) {
		
		// This if statement is needed to support our testing processes. It can eventually be deleted.
		if (da.getUrl().equals(Const.DevForTestingPurposes)) {
			if (prop.contentEquals(Const.NonExistentPropertyForTestingPurposes)) return Resp.response(R_CODE.E_PROPNOTFOUND, prop);
			else return Resp.response(R_CODE.OK);
		}
		
		if (callingUser instanceof EndUser || subscrManager.subscriptionExists(da, callingUser)) 
			return sendStringToG37D(da, "POKE" + Const.DivCh + prop + Const.DivCh + val + Const.DivCh + callingUser.getUserType());
		else return Resp.response(R_CODE.E_ACCESSDENIEDNOSUBSCR, da.getUrl() + " user " + callingUser.getUsername());
	}
	
	// Implementation of a ping command.  Used only internally.
	public String ping(DeviceAccount da) {
		// This if statement is needed to support our testing processes. It can eventually be deleted.
		if (da.getUrl().equals(Const.DevForTestingPurposes)) return Const.Alive;
		
		return sendStringToG37D(da, "PING");
	}
	
	public String pingAll() {
		String r = "";
		for (DeviceAccount da : getDeviceAccounts()) {
			r = ping(da);
			if (!r.equals(Const.Alive)) return Resp.response(R_CODE.E_PINGFAILED, da.getUrl());
		}
		return Const.Alive;
	}
	
} //DeviceAccountsManagerImpl
