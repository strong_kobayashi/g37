/**
 */
package G37.G37_Controller.impl;

import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.ServiceSubscription;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Subscription</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ServiceSubscriptionImpl extends GenericSubscriptionImpl implements ServiceSubscription {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceSubscriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.SERVICE_SUBSCRIPTION;
	}

} //ServiceSubscriptionImpl
