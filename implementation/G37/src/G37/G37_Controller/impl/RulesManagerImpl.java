package G37.G37_Controller.impl;

import java.io.StringReader;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.junit.jupiter.api.extension.ExtendWith;

import com.google.inject.Inject;
import com.google.inject.Injector;

// Our EMF model
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.Action;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.EndUser;
import G37.G37_Controller.EventCondition;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.Rule;
import G37.G37_Controller.RulesManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.XtextParser;
import G37.G37_Controller.G37_ControllerPackage;
// Our DSL
import g37.DslInjectorProvider;
import g37.DslStandaloneSetup;
import g37.DslStandaloneSetupGenerated;
import g37.dsl.Model;

@ExtendWith(InjectionExtension.class)
@InjectWith(DslInjectorProvider.class)
@SuppressWarnings("all")

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rules Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.RulesManagerImpl#getRules <em>Rules</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.RulesManagerImpl#getDaManager <em>Da Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RulesManagerImpl extends MinimalEObjectImpl.Container implements RulesManager {
	/**
	 * The cached value of the '{@link #getRules() <em>Rules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRules()
	 * @generated
	 * @ordered
	 */
	protected static EList<Rule> rules;
	
	/**
	 * The cached value of the '{@link #getDaManager() <em>Da Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDaManager()
	 * @generated
	 * @ordered
	 */
	protected DeviceAccountsManager daManager;

	G37_ControllerFactory controllerFactory;
	
	protected DslStandaloneSetup dslStandaloneSetup;
	
	protected XtextParser parser;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *  
	 */
	protected RulesManagerImpl() {
		super();
		parser = new XtextParserImpl();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.RULES_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Rule> getRules() {
		if (rules == null) {
			rules = new EObjectContainmentEList<Rule>(Rule.class, this, G37_ControllerPackage.RULES_MANAGER__RULES);
		}
		return rules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeviceAccountsManager getDaManager() {
		if (daManager != null && daManager.eIsProxy()) {
			InternalEObject oldDaManager = (InternalEObject)daManager;
			daManager = (DeviceAccountsManager)eResolveProxy(oldDaManager);
			if (daManager != oldDaManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.RULES_MANAGER__DA_MANAGER, oldDaManager, daManager));
			}
		}
		return daManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceAccountsManager basicGetDaManager() {
		return daManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDaManager(DeviceAccountsManager newDaManager) {
		DeviceAccountsManager oldDaManager = daManager;
		daManager = newDaManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.RULES_MANAGER__DA_MANAGER, oldDaManager, daManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case G37_ControllerPackage.RULES_MANAGER__RULES:
				return ((InternalEList<?>)getRules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.RULES_MANAGER__RULES:
				return getRules();
			case G37_ControllerPackage.RULES_MANAGER__DA_MANAGER:
				if (resolve) return getDaManager();
				return basicGetDaManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.RULES_MANAGER__RULES:
				getRules().clear();
				getRules().addAll((Collection<? extends Rule>)newValue);
				return;
			case G37_ControllerPackage.RULES_MANAGER__DA_MANAGER:
				setDaManager((DeviceAccountsManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.RULES_MANAGER__RULES:
				getRules().clear();
				return;
			case G37_ControllerPackage.RULES_MANAGER__DA_MANAGER:
				setDaManager((DeviceAccountsManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.RULES_MANAGER__RULES:
				return rules != null && !rules.isEmpty();
			case G37_ControllerPackage.RULES_MANAGER__DA_MANAGER:
				return daManager != null;
		}
		return super.eIsSet(featureID);
	}
	
	// This method sets the controller factory member of this object.
	public void setControllerFactory(G37_ControllerFactory cf) {
		controllerFactory = cf;
	}
	
	// Sets the value of the dslStandaloneSetup member
	public void setDslStandaloneSetup(DslStandaloneSetup setup) {
		dslStandaloneSetup = setup;
	}
	
	// Gets the value of the dslStandaloneSetup member
	public DslStandaloneSetup getDslStandaloneSetup() {
		return dslStandaloneSetup;
	}
	
	// This method returns the rule as identified by the rule ID.
	public Rule getRuleByID(String ruleID) {
		Rule rule = null;
		for (Rule rl : getRules()) {
			if (rl.getId().equals(ruleID)) {
				rule = rl;
				break;
			}
		}
		return rule;
	}
	
	// Implementation of ADDRULE command.  Adds a new rule to our home automaton controller.
	public String addRule(String ruleID, String eca, UserAccount callingUser) {
		Model ast;
		if (ruleID == "") return Resp.response(R_CODE.E_RULEIDEMPTY);
		// Check if a rule with such ID already exists
		Rule rule = getRuleByID(ruleID);
		if (rule != null) return Resp.response(R_CODE.E_RULEEXISTS, ruleID);
		// Create the rule
		rule = controllerFactory.createRule(); 
		rule.setId(ruleID);
		rule.setEca(eca);
		rule.setCreator(callingUser);
		try {
			if (parser != null) ast = (Model)parser.parse(new StringReader(eca));
			else {
				return Resp.response(R_CODE.E_CANTPARSEECA, "reference to parser is null");
			}
		} catch (Throwable _e) {
		      return Resp.response(R_CODE.E_CANTPARSEECA, eca + ", parsing error: " + _e.getMessage());
		}
				
		Resp.log("    Parsed the string " + eca + "\n    and obtained model " + ast, true);
		EventCondition eC = controllerFactory.createEventCondition();
		eC.setDaManager(daManager);
		rule.setEventCondition(eC);
		Action act = controllerFactory.createAction();		
		act.setDaManager(daManager);
		rule.setAction(act);
		String r = rule.initFromDSLmodel(ast); 
		if (r.equals(Resp.response(R_CODE.OK))) getRules().add(rule);  // Rule created successfully, so add it to the list of rules.
		return r;
	}
	
	// Another implementation of ADDRULE command.  This is just a legacy signature for compatibility with other team members.
	public String addRule(String ruleID, String eventCond, String action, UserAccount callingUser) {
		return addRule(ruleID, eventCond + action, callingUser);
	}
	
	// Implementation of LSRULES command. Lists the rules accessible to the calling user.
	public String lsRules(UserAccount callingUser) {
		int i = 1;
		String result = "";
		for (Rule rule : getRules()) {
			if (callingUser instanceof EndUser || (rule.getCreator().getUsername().equals(callingUser.getUsername()))) {
				result += "Rule " + i++ + "\t" + rule.getId() + "\t" + rule.getEca() + "\t" + rule.getCreator().getUsername() + "\n";
			}
		}
		return result;
	}
	
	// Implementation of rmRule command.  Deletes a rule with the given ID.
	public String rmRule(String ruleID, UserAccount callingUser) {
		if (getRules().removeIf(rule -> (rule.getId().equals(ruleID) && (callingUser instanceof EndUser || (rule.getCreator().getUsername().equals(callingUser.getUsername())))))) 
			return Resp.response(R_CODE.OK);
		else return Resp.response(R_CODE.E_RULENOTFOUNDORINACC, ruleID); // Rule not found, or inaccessible by this user.
	}
	
	// This method goes through the list of rules and executes them all.
	// Returns OK if all executed without errors, or the first error otherwise.
	public String executeAll() {
		daManager.pingAll();
		for (Rule rule : getRules()) {
			String r = rule.execute();
			if (!r.equals(Resp.response(R_CODE.OK))) return r;
		}
		return Resp.response(R_CODE.OK);
	}
	
} //RulesManagerImpl
