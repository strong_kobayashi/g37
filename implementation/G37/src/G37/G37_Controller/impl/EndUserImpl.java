/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.Const;
import G37.G37_Controller.EndUser;
import G37.G37_Controller.G37_ControllerPackage;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>End User</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EndUserImpl extends UserAccountImpl implements EndUser {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EndUserImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.END_USER;
	}
	
	public String getUserType() {
		return Const.EU;
	}

} //EndUserImpl
