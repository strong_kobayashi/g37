/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccount;
import G37.G37_Controller.DeviceSupplier;
import G37.G37_Controller.EndUser;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.GenericSubscription;
import G37.G37_Controller.Room;
import G37.G37_Controller.RoomsManager;
import G37.G37_Controller.ServiceProvider;
import G37.G37_Controller.SubscriptionsManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subscriptions Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.SubscriptionsManagerImpl#getSubscriptions <em>Subscriptions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubscriptionsManagerImpl extends MinimalEObjectImpl.Container implements SubscriptionsManager {
	/**
	 * The cached value of the '{@link #getSubscriptions() <em>Subscriptions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubscriptions()
	 * @generated
	 * @ordered
	 */
	protected static EList<GenericSubscription> subscriptions;
	
	private G37_ControllerFactory controllerFactory;
	private UserAccountsManager uaManager;
	private RoomsManager roomsManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubscriptionsManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.SUBSCRIPTIONS_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<GenericSubscription> getSubscriptions() {
		if (subscriptions == null) {
			subscriptions = new EObjectContainmentEList<GenericSubscription>(GenericSubscription.class, this, G37_ControllerPackage.SUBSCRIPTIONS_MANAGER__SUBSCRIPTIONS);
		}
		return subscriptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case G37_ControllerPackage.SUBSCRIPTIONS_MANAGER__SUBSCRIPTIONS:
				return ((InternalEList<?>)getSubscriptions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.SUBSCRIPTIONS_MANAGER__SUBSCRIPTIONS:
				return getSubscriptions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.SUBSCRIPTIONS_MANAGER__SUBSCRIPTIONS:
				getSubscriptions().clear();
				getSubscriptions().addAll((Collection<? extends GenericSubscription>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.SUBSCRIPTIONS_MANAGER__SUBSCRIPTIONS:
				getSubscriptions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.SUBSCRIPTIONS_MANAGER__SUBSCRIPTIONS:
				return subscriptions != null && !subscriptions.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	// Sets the controllerFactory member of this object.
	public void setControllerFactory(G37_ControllerFactory cf) {
		controllerFactory = cf;
	}
	
	// Sets the uaManager member of this object.
	public void setUserAccountsManager(UserAccountsManager uaM) {
		uaManager = uaM;
	}
	
	// Sets the RoomsManager member of this object.
	public void setRoomsManager(RoomsManager rM) {
		roomsManager = rM;
	}

	// Implementation of ADDSUBSCR command.  Adds a new subscription to our home automaton controller.
	public String addSubscr(String subscrID, String devClass, String listOfRooms, String username) {
		UserAccount userAccount = null;
		BasicEList<Room> rooms = new BasicEList<Room>();
		GenericSubscription subscr = null;
		
		// Basic checks.
		if (subscrID == "") return Resp.response(R_CODE.E_SUBSCRIDEMPTY);
		if (devClass == "") return Resp.response(R_CODE.E_URLEMPTY);
		if (listOfRooms == "") return Resp.response(R_CODE.E_LISTROOMSEMPTY);
		if (username == "") return Resp.response(R_CODE.E_USERNAMEEMPTY);
		
		// First lets build a list of objects of the type Room. 
		String[] tokens = listOfRooms.split(";");
		for (String tk : tokens) {
			Room rm = roomsManager.getRoomByID(tk);
			if (rm == null) return Resp.response(R_CODE.E_ROOMNOTFOUND, tk);
			rooms.add(rm); 
		}
		if (rooms.isEmpty()) return Resp.response(R_CODE.E_INT_LISTROOMSEMPTY);
		
		// Let's make sure such user exists.
		userAccount = uaManager.getUserAccountByUserName(username);
		if (userAccount == null) return Resp.response(R_CODE.E_USERNAMENOTFOUND, username);
		
		// Basic checks are over, we can create a new subscription.
		if (userAccount instanceof DeviceSupplier) subscr = controllerFactory.createServiceSubscription();
		else if (userAccount instanceof ServiceProvider) subscr = controllerFactory.createSupplierSubscription();
		else if (userAccount instanceof EndUser) return Resp.response(R_CODE.E_SUBSCRDSSP);
		else return Resp.response(R_CODE.E_INT_USERCLASS, userAccount.getClass().toString());
		subscr.setId(subscrID);
		subscr.setDevClass(devClass);
		subscr.setUser(userAccount);
		subscr.getRooms().addAll(rooms);
		getSubscriptions().add(subscr); // Add the subscription to the controller's list
		return Resp.response(R_CODE.OK);
	}
	
	// Implementation of LSSUBSCR command. Lists the subscriptions.
	public String lsSubscr (UserAccount callingUser) {
		String result = "";
		int i = 1;
		String listOfRooms = "";
		for (GenericSubscription subscr : getSubscriptions()) {
			// End users can see all subscriptions, whereas other user only "their" subscriptions
			if (callingUser instanceof EndUser || (subscr.getUser().getUsername().equals(callingUser.getUsername()))) {
				for (Room rm : subscr.getRooms()) {
					listOfRooms += rm.getId()+";";
				}
				listOfRooms = listOfRooms.substring(0, listOfRooms.length()-1); // Remove the trailing semicolon
				result += "Subscription " + i++ + "\t" + subscr.getId() + "\t" + subscr.getDevClass() + "\t" + listOfRooms + "\t" + subscr.getUser().getUsername()+"\n";
				listOfRooms = "";
			}
		}
		
		return result;
	}
	
	// Implementation of RMSUBSCR command.  Deletes a device account with the given URL.
	public String rmSubscr (String subscrID) {
		for (GenericSubscription s : getSubscriptions()) {
			if (s.getId().equals(subscrID)) {
				getSubscriptions().remove(s);
				return Resp.response(R_CODE.OK);
			}
		}
		return Resp.response(R_CODE.E_SUBSCRNOTFOUND, subscrID);
	}
	
	// Check if there exists a subscription that allows a given user to access a given device.
	public boolean subscriptionExists(DeviceAccount da, UserAccount ua) {
		for (GenericSubscription s : getSubscriptions()) {
			if (s.getUser().equals(ua)) {  
				// The given user is a party to this subscription, now let's check if it applies to the given device.
				for (Room rm : s.getRooms()) if (da.getRoom().equals(rm)) return true;  
			}
		}
		return false;
	}
	
	
} //SubscriptionsManagerImpl
