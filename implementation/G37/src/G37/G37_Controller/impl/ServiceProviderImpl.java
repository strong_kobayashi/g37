/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.Const;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.ServiceProvider;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ServiceProviderImpl extends UserAccountImpl implements ServiceProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.SERVICE_PROVIDER;
	}
	
	public String getUserType () {
		return Const.SP;
	}

} //ServiceProviderImpl
