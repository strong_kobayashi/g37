/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.Room;
import G37.G37_Controller.RoomsManager;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rooms Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.RoomsManagerImpl#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomsManagerImpl extends MinimalEObjectImpl.Container implements RoomsManager {
	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected static EList<Room> rooms;
	
	G37_ControllerFactory controllerFactory;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomsManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.ROOMS_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectContainmentEList<Room>(Room.class, this, G37_ControllerPackage.ROOMS_MANAGER__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case G37_ControllerPackage.ROOMS_MANAGER__ROOMS:
				return ((InternalEList<?>)getRooms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.ROOMS_MANAGER__ROOMS:
				return getRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.ROOMS_MANAGER__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.ROOMS_MANAGER__ROOMS:
				getRooms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.ROOMS_MANAGER__ROOMS:
				return rooms != null && !rooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	// This method sets the controller factory member of this class.
	public void setControllerFactory(G37_ControllerFactory cf) {
		controllerFactory = cf;
	}
	
	// This method returns the room as identified by the room ID.
	public Room getRoomByID(String roomID) {
		Room room = null;
		for (Room rm : getRooms()) {
			if (rm.getId().equals(roomID)) {
				room = rm;
				break;
			}
		}
		return room;
	}
	
	// Implementation of ADDROOM command.  Adds a new room to our home automaton controller.
	public String addRoom(String roomID) {
		if (roomID == "") return Resp.response(R_CODE.E_ROOMIDEMPTY);
		if (roomID.contains(";")) return Resp.response(R_CODE.E_ROOMIDSEMICOLON);

		//Check if a room with such ID already exists
		Room room = getRoomByID(roomID);
		if (room != null) return Resp.response(R_CODE.E_ROOMEXISTS, roomID);
		// Create the room
		room = controllerFactory.createRoom(); 
		room.setId(roomID);
		// Add the room to the controller's list
		getRooms().add(room);
		return Resp.response(R_CODE.OK);
	}
	
	// Implementation of LSROOMS command. Lists all existing rooms.
	public String lsRooms() {
		int i = 1;
		String result = "";
		for (Room room : getRooms()) result += "Room " + i++ + "\t" + room.getId() + "\n";
		return result;
	}
	
	// Implementation of RMROOM command.  Deletes a room with the given ID.
	public String rmRoom(String roomID) {
		if (getRooms().removeIf(room -> (room.getId().equals(roomID)))) return Resp.response(R_CODE.OK);
		else return Resp.response(R_CODE.E_ROOMNOTFOUND, roomID);
	}

} //RoomsManagerImpl
