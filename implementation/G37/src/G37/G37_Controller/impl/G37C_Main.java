package G37.G37_Controller.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccount;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.DeviceSupplier;
import G37.G37_Controller.EndUser;
import G37.G37_Controller.G37C;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.RoomsManager;
import G37.G37_Controller.RulesManager;
import G37.G37_Controller.ServiceProvider;
import G37.G37_Controller.SubscriptionsManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;
import g37.DslStandaloneSetup;

public class G37C_Main {
	
	private static G37C controller;
	private static G37_ControllerFactory controllerFactory;
	private static UserAccountsManager uaManager;
	private static DeviceAccountsManager daManager;
	private static RoomsManager roomsManager;
	private static SubscriptionsManager subscrManager;
	private static RulesManager rulesManager;
	
	/* ****************************************************************************************************************
	 * ***************** M I S C **************************************************************************************
	 * ****************************************************************************************************************
	 */
	
	// This is just a utility function for our test-driven approach, to replace repeating code.  Very unimportant.
	private static void validate(String result, String prompt, String expected) {
		Resp.log (prompt);
		assert Pattern.compile(expected).matcher(result).matches();
  	}
	
	// Same as above for a predefined text resource
	private static void validate(String result, String prompt, R_CODE code, String arg) {
		validate(result, prompt, Resp.response(code, arg));
	}
	
	// Same as above without the last argument.
	private static void validate(String result, String prompt, R_CODE code) {
		validate(result, prompt, Resp.response(code));
	}
	
	// This method performs the initial setup.
	public static void setUpController(String[] args) {		
		
	
		
		// Initialize the controller and the controllerFactory
		controllerFactory = G37_ControllerFactory.eINSTANCE;
		controller = controllerFactory.createG37C();
		// Initialize the managers.  The order is important!
		uaManager = controllerFactory.createUserAccountsManager();
		uaManager.setControllerFactory(controllerFactory);
		roomsManager = controllerFactory.createRoomsManager();
		roomsManager.setControllerFactory(controllerFactory);
		daManager = controllerFactory.createDeviceAccountsManager();
		daManager.setControllerFactory(controllerFactory);
		daManager.setRoomsManager(roomsManager);
		// Subscriptions manager
		subscrManager = controllerFactory.createSubscriptionsManager();
		subscrManager.setControllerFactory(controllerFactory);
		subscrManager.setUserAccountsManager(uaManager);
		subscrManager.setRoomsManager(roomsManager);
		subscrManager.setUserAccountsManager(uaManager);
		daManager.setSubscrManager(subscrManager);
		// Rules manager
		rulesManager = controllerFactory.createRulesManager();
		rulesManager.setControllerFactory(controllerFactory);
		rulesManager.setDaManager(daManager);
		// Inject all managers
		controller.setUaManager(uaManager);
		controller.setRoomsManager(roomsManager);
		controller.setDaManager(daManager);
		controller.setRulesManager(rulesManager);
		controller.setSubscrManager(subscrManager);
	}
		
	/* ****************************************************************************************************************
	 * ***************** T E S T   S U I T E S ************************************************************************
	 * ****************************************************************************************************************
	 */
	public static void runTests1() {
		String r;
		UserAccount enduser, spuser, dsuser;
		
		Resp.log("--- Testing users ---\n"); // **************************************************************
		r = uaManager.addUser("admin", "123", "This is the default user account", Const.EU);		validate(r, "  Adding end-user called 'admin':    " + r, R_CODE.OK);
		r = uaManager.addUser("ds", "123", "Device supplier test", Const.DS);						validate(r, "  Adding DS called 'ds':       " + r, R_CODE.OK);
		r = uaManager.addUser("sp", "123", "Service provider test", Const.SP);						validate(r, "  Adding SP called 'sp':       " + r, R_CODE.OK);
		r = uaManager.addUser("inv", "123", "Invalid test", "invalid");								validate(r, "  Adding invalid:  " + r, R_CODE.E_INVUSERTYPE, "invalid");
		r = uaManager.addUser("sp", "345", "Duplicate user!", Const.SP);							validate(r, "  Adding a user called 'sp' once again: " + r, R_CODE.E_USERNAMEEXISTS, "sp");
		r = uaManager.lsUsers();																	validate(r, "\nAll users at this point:\n" + r, "User 1\tEnd user\t\tadmin\tThis is the default user account\nUser 2\tDevice supplier\t\tds\tDevice supplier test\nUser 3\tService provider\tsp\tService provider test\n");
		r = uaManager.rmUser("ds");																	validate(r, "  Removing DS: " + r, R_CODE.OK);
		r = uaManager.lsUsers();																	validate(r, "\nAll users at this point:\n" + r, "User 1\tEnd user\t\tadmin\tThis is the default user account\nUser 2\tService provider\tsp\tService provider test\n");
		r = uaManager.rmUser("sp");																	validate(r, "  Removing SP: " + r, R_CODE.OK);
		r = uaManager.lsUsers();																	validate(r, "\nAll users at this point:\n" + r, "User 1\tEnd user\t\tadmin\tThis is the default user account\n");
		r = uaManager.addUser("sp1", "123", "We can have multiple users of the same type", Const.SP);	validate(r, "  Adding one more SP, this time called 'sp1':       " + r, R_CODE.OK);
		r = uaManager.lsUsers();																	validate(r, "\nAll users at this point:\n" + r, "User 1\tEnd user\t\tadmin\tThis is the default user account\nUser 2\tService provider\tsp1\tWe can have multiple users of the same type\n");
		enduser = uaManager.getUserAccountByUserName("admin");
		assert enduser != null : "Error while looking up the end-user";	
		assert enduser instanceof EndUser : "Unexpected end-user class " + enduser.getClass();
		
		Resp.log("\n--- Testing rooms ---\n"); // **************************************************************
		r = roomsManager.addRoom("Living room");													validate(r, "  Adding Living room:        " + r, R_CODE.OK);
		r = roomsManager.addRoom("Kitchen");														validate(r, "  Adding Kitchen:            " + r, R_CODE.OK);
		r = roomsManager.addRoom("Attic");															validate(r, "  Adding Attic:              " + r, R_CODE.OK);
		r = roomsManager.addRoom("Living room");													validate(r, "  Adding Living room again:  " + r, R_CODE.E_ROOMEXISTS, "Living room");
		r = roomsManager.addRoom("");																validate(r, "  Adding room with empty name:  " + r, R_CODE.E_ROOMIDEMPTY);
		r = roomsManager.addRoom("Living;room");													validate(r, "  Adding room with a semicolon:  " + r, R_CODE.E_ROOMIDSEMICOLON);
		r = roomsManager.lsRooms();																	validate(r, "\nAll rooms at this point:\n" + r, "Room 1\tLiving room\nRoom 2\tKitchen\nRoom 3\tAttic\n");
		r = roomsManager.rmRoom("Attic");															validate(r, "  Removing Attic: " + r, R_CODE.OK);
		r = roomsManager.rmRoom("No such room");													validate(r, "  Removing non-existent room: " + r, R_CODE.E_ROOMNOTFOUND, "No such room");
		r = roomsManager.lsRooms();																	validate(r, "\nAll rooms at this point:\n" + r, "Room 1\tLiving room\nRoom 2\tKitchen\n");
		
		Resp.log("\n--- Testing devices ---\n"); // **************************************************************	
		r = daManager.reg("192.168.0.1", "Living room");											validate(r, "  Adding 192.168.0.1 to Living room:   " + r, R_CODE.OK);
		r = daManager.reg("192.168.0.2", "Kitchen");												validate(r, "  Adding 192.168.0.2 to Kitchen:       " + r, R_CODE.OK);
		r = daManager.reg("192.168.0.2", "Kitchen");												validate(r, "  Adding 192.168.0.2 to Kitchen again: " + r, R_CODE.E_URLEXISTS, "192.168.0.2");
		r = daManager.reg("192.160.0.3", "Attic");													validate(r, "  Adding 192.168.0.3: to Attic:        " + r, R_CODE.E_ROOMNOTFOUND, "Attic");
		r = daManager.lsDev("", enduser);															
		Resp.log("\nAll devices at this point: \n" + r);				
		assert r.equals("Device 1\t192.168.0.1\tLiving room\nDevice 2\t192.168.0.2\tKitchen\n");
		r = daManager.lsDev("Living room", enduser);												
		Resp.log("Devices in Living room: \n" + r);					
		assert r.equals("Device 1\t192.168.0.1\tLiving room\n");
		r = daManager.lsDev("Kitchen", enduser);													
		Resp.log("Devices in Kitchen: \n" + r);						
		assert r.equals("Device 1\t192.168.0.2\tKitchen\n");
		r = daManager.lsDev("Attic", enduser);													
		Resp.log("Devices in Attic: \n" + r);						
		assert r.equals("Error: room \"Attic\" not found.");
		r = daManager.unreg("192.168.0.2");															validate(r, "\n  Removing the device from Kitchen: " + r, R_CODE.OK);
		r = daManager.lsDev("", enduser);															
		Resp.log("\nAll devices at this point: \n" + r);				
		assert r.equals("Device 1\t192.168.0.1\tLiving room\n");
		r = daManager.unreg("1.2.3.4");																validate(r, "  Removing a non-existent device: " + r, R_CODE.E_URLNOTFOUND, "1.2.3.4");	
		r = daManager.mv("192.168.0.1", "Kitchen");													validate(r, "  Moving a device to Kitchen: " + r, R_CODE.OK);
		r = daManager.mv("192.168.0.1", "Attic");													validate(r, "  Moving a device to an invalid room: " + r, R_CODE.E_ROOMNOTFOUND, "Attic");
		r = daManager.mv("1.1.1.1", "Kitchen");														validate(r, "  Moving a non-existent device: " + r, R_CODE.E_URLNOTFOUND, "1.1.1.1");
		r = daManager.lsDev("", enduser);															
		Resp.log("\nAll devices at this point: \n" + r);				
		assert r.equals("Device 1\t192.168.0.1\tKitchen\n");
		
		Resp.log("\n--- Testing subscriptions ---\n"); // **************************************************************
		// Prepare our users.
		r = uaManager.addUser("sp", "123", "Service provider test", Const.SP);						validate(r, "  Adding SP back:       " + r, R_CODE.OK);
		r = uaManager.addUser("ds", "123", "Device supplier test", Const.DS);						validate(r, "  Adding DS back:       " + r, R_CODE.OK);
		assert uaManager.getUserAccounts().size() >= 3 : "Unexpected number of users: "+uaManager.getUserAccounts().size();
		spuser = uaManager.getUserAccountByUserName("sp");
		assert spuser != null : "Error while looking up the service provider";			
		assert spuser instanceof ServiceProvider : "Unexpected service provider class " + spuser.getClass();
		dsuser = uaManager.getUserAccountByUserName("ds");
		assert dsuser != null : "Error while looking up the device supplier";	 
		assert dsuser instanceof DeviceSupplier : "Unexpected device supplier class " + dsuser.getClass();
		// Play with subscriptions.
		r = roomsManager.addRoom("Cellar");															validate(r, "  First let's add a cellar: " + r, R_CODE.OK);	
		r = subscrManager.addSubscr("Lights subscr.", "Lights", "Living room;Kitchen", "sp");		validate(r, "  Adding a Lights subscription to Living room and Kitchen: " + r, R_CODE.OK);
		r = subscrManager.addSubscr("Heater subscr.", "Heaters", "Kitchen;Cellar", "sp");			validate(r, "  Adding a Heater subscription to Kitchen and Cellar: " + r, R_CODE.OK);
		r = subscrManager.addSubscr("Invalid", "Some class", "Wrong room", "sp");					validate(r, "  Adding a subscription with a non-existent room: " + r, R_CODE.E_ROOMNOTFOUND, "Wrong room");
		r = subscrManager.addSubscr("Invalid", "Some class", "Kitchen", "wrong user");				validate(r, "  Adding a subscription with a non-existent user: " + r, R_CODE.E_USERNAMENOTFOUND, "wrong user");
		r = subscrManager.addSubscr("Invalid", "Some class", "Kitchen", "admin");					validate(r, "  Adding a subscription with an end-user at the other end: " + r, R_CODE.E_SUBSCRDSSP);
		r = subscrManager.lsSubscr(enduser);														validate(r, "\nAll subscriptions at this point: \n" + r, "Subscription 1	Lights subscr.	Lights	Living room;Kitchen	sp\nSubscription 2	Heater subscr.	Heaters	Kitchen;Cellar	sp\n");		
		r = subscrManager.rmSubscr("Lights subscr.");												validate(r, "  Removing the Lights subscription: " + r, R_CODE.OK);
		r = subscrManager.rmSubscr("Invalid subscr.");												validate(r, "  Removing a non-existent subscription: " + r, R_CODE.E_SUBSCRNOTFOUND, "Invalid subscr.");
		r = subscrManager.lsSubscr(enduser);														validate(r, "\nAll subscriptions at this point: \n" + r, "Subscription 1\tHeater subscr.\tHeaters\tKitchen;Cellar\tsp\n");
		r = subscrManager.addSubscr("Fridge subscr.", "Refrigerators", "Cellar", "ds");				validate(r, "  Adding a Fridge subscription to Cellar, with DS: " + r, R_CODE.OK);
		r = subscrManager.addSubscr ("Fire subscr.","Fire alarm", "Living room;Kitchen;Cellar","ds");	validate(r, "  Adding a Fire subscription to all rooms, with DS: " + r, R_CODE.OK);
		r = subscrManager.lsSubscr(dsuser);															validate(r, "\nDS should see both of his subscriptions, but no SP's: \n" + r, "Subscription 1\tFridge subscr.\tRefrigerators\tCellar\tds\nSubscription 2\tFire subscr.\tFire alarm\tLiving room;Kitchen;Cellar\tds\n");	
		r = subscrManager.lsSubscr(spuser);															validate(r, "SP should not see DS's subscriptions: \n" + r, "Subscription 1\tHeater subscr.\tHeaters\tKitchen;Cellar\tsp\n");	
		r = subscrManager.lsSubscr(enduser);														validate(r, "\nAll subscriptions at this point: \n" + r, "Subscription 1\tHeater subscr.\tHeaters\tKitchen;Cellar\tsp\nSubscription 2\tFridge subscr.\tRefrigerators\tCellar\tds\nSubscription 3\tFire subscr.\tFire alarm\tLiving room;Kitchen;Cellar\tds\n");
		
		Resp.log("\n--- Testing rules (for the moment, on nonexistent devices) ---\n"); // **************************************************************
		r = daManager.reg("192.168.0.2:8011", "Kitchen");											validate(r, "  Adding 192.168.0.2 to Kitchen:       " + r, R_CODE.OK);
		r = daManager.reg("192.168.0.4", "Cellar");													validate(r, "  Adding 192.168.0.2 to Cellar:       " + r, R_CODE.OK);
		r = daManager.reg("192.168.0.3", "Cellar");													validate(r, "  Adding 192.168.0.3 to Cellar:       " + r, R_CODE.OK);
		r = rulesManager.addRule("Lights rule", "IF (192.168.0.1 Movement == true) ", "POKE [192.168.0.1 'Is On' true]", spuser);	validate(r, "  Adding a Lights rule: " + r, R_CODE.OK);
		r = rulesManager.addRule("Sound rule", "IF (192.168.0.2:8011 Movement == true) ", "POKE [192.168.0.4 alarm true]", spuser);	validate(r, "  Adding a Sound rule: " + r, R_CODE.OK);	
		r = rulesManager.addRule("Heating rule", "IF (192.168.0.3 Temperature > 30) ", "POKE [192.168.0.3 'Is On' false]", enduser);	validate(r, "  Adding a Heating rule: " + r, R_CODE.OK);
		r = rulesManager.lsRules(enduser);															validate(r, "\nAll rules at this point: \n" + r, "Rule 1	Lights rule	G37.G37_Controller.impl.EventConditionImpl@.+	G37.G37_Controller.impl.ActionImpl@.+ \\(property: Is On, value: true\\)	sp\nRule 2	Sound rule	G37.G37_Controller.impl.EventConditionImpl@.+	G37.G37_Controller.impl.ActionImpl@.+ \\(property: alarm, value: true\\)	sp\nRule 3	Heating rule	G37.G37_Controller.impl.EventConditionImpl@.+	G37.G37_Controller.impl.ActionImpl@.+ \\(property: Is On, value: false\\)	admin\n");				
		r = rulesManager.lsRules(spuser);															validate(r, "SP should only see his rules: \n" + r, "Rule 1	Lights rule	G37.G37_Controller.impl.EventConditionImpl@.+	G37.G37_Controller.impl.ActionImpl@.+ \\(property: Is On, value: true\\)	sp\nRule 2	Sound rule	G37.G37_Controller.impl.EventConditionImpl@.+	G37.G37_Controller.impl.ActionImpl@.+ \\(property: alarm, value: true\\)	sp\n");				
		r = rulesManager.lsRules(dsuser);															validate(r, "DS should only see his rules, which is none: \n" + r, "");		
		r = rulesManager.addRule("", "IF (192.168.0.3 Temperature > 30)" , "POKE [192.168.0.3 'Is On' false]", enduser);	validate(r, "  Trying to add a rule with empty name: " + r, R_CODE.E_RULEIDEMPTY);
		r = rulesManager.addRule("Heating rule", "IF (192.168.0.3 Temperature > 30)", "POKE [192.168.0.3 'Is On' false]", enduser);	validate(r, "  Trying to add a Heating rule again: " + r, R_CODE.E_RULEEXISTS, "Heating rule");
		r = rulesManager.rmRule("Lights rule", spuser);												validate(r, "  SP should be able to delete his rule: " + r, R_CODE.OK);
		r = rulesManager.rmRule("Heating rule", spuser);											validate(r, "  ...but not somebody else's rules: " + r, R_CODE.E_RULENOTFOUNDORINACC, "Heating rule");
		r = rulesManager.lsRules(enduser);															validate(r, "\nAll rules at this point: \n" + r, "Rule 1	Sound rule	G37.G37_Controller.impl.EventConditionImpl@.+	G37.G37_Controller.impl.ActionImpl@.+ \\(property: alarm, value: true\\)	sp\nRule 2	Heating rule	G37.G37_Controller.impl.EventConditionImpl@.+	G37.G37_Controller.impl.ActionImpl@.+ \\(property: Is On, value: false\\)	admin\n");
	
		Resp.log("\n--- Testing peek and poke ---\n"); // **************************************************************
		String localDevURL = "127.0.0.1:8111";
		r = daManager.reg(localDevURL, "Living room");												validate(r, "  Adding 127.0.0.1:8111 to Living room:   " + r + "\n", R_CODE.OK);
		DeviceAccount localDev = daManager.getDeviceAccountByURL(localDevURL);
		assert localDev.getUrl().equals(localDevURL);
		enduser = uaManager.getUserAccountByUserName("admin");
		assert enduser.getUsername().equals("admin");
		r = daManager.peek(localDev, Const.DeviceClass, enduser);									validate(r, "  Peeked at 'Device class' of " + localDevURL + "\n" + "  Device responded: " + r, "Light bulb");
		r = daManager.peek(localDev, Const.IsOn, enduser);											validate(r, "  Peeked at 'Is On' of " + localDevURL + "\n  Device responded: " + r, "false|true");
		r = daManager.peek(localDev, Const.Uptime, enduser);										validate(r, "  Peeked at 'Uptime' of " + localDevURL + " - remember the value!\n  Device responded: " + r, "0");
		r = daManager.poke(localDev, Const.IsOn, "true", enduser);									validate(r, "  Poked 'true' to 'Is On' of " + localDevURL +"\n  Device responded: " + r, R_CODE.OK);
		r = daManager.peek(localDev, Const.IsOn, enduser);											validate(r, "  Peeked at 'Is On' of " + localDevURL + "\n  Device responded: " + r, "false|true");
		r = daManager.peek(localDev, Const.IsOn, dsuser);											validate(r, "  DS tried to peek at 'Is On' of " + localDevURL + "\n  Device responded: " + r, R_CODE.E_DSACCESS);
		r = daManager.poke(localDev, Const.DeviceDescr, "New description", dsuser);					validate(r, "  DS tried to poke at 'Device description' of " + localDevURL + "\n  Device responded: " + r, R_CODE.E_DSACCESS);
		r = daManager.peek(localDev, "Firmware Version", dsuser);									validate(r, "  It's OK, however, for DS to peek at 'Firmware version'\n  Device responded: " + r, "1.0.0");
		r = daManager.poke(localDev, Const.FirmwareBuffer, "0110111001010110111001", dsuser);		validate(r, "  ... or to poke stuff into 'Firmware buffer'\n  Device responded: " + r, R_CODE.OK);
		r = daManager.poke(localDev, "Firmware Version", "1.0.1", dsuser);							validate(r, "  After which we should remember to update the Firmware version to 1.0.1\n  Device responded: " + r, R_CODE.OK);
		r = daManager.peek(localDev, Const.DeviceDescr, spuser);									validate(r, "  Tried to peek at a property of a device that SP is not subscribed to\n  Device responded: " + r, R_CODE.E_ACCESSDENIEDNOSUBSCR, localDevURL + " user " + spuser.getUsername());
		r = subscrManager.addSubscr("Lights subscr.", "Lights", "Living room;Kitchen", "sp");		validate(r, "  ... well, let's add the needed subscription: " + r, R_CODE.OK);	
		r = daManager.peek(localDev, Const.DeviceDescr, spuser);									validate(r, "  Now the same \"peek\" should work\n  Device responded: " + r, "This is my favorite light bulb");
		r = daManager.peek(localDev, "", spuser);													validate(r, "  Tried to peek at a property with an empty name\n  Device responded: " + r, R_CODE.E_EMPTYPROP);
		r = daManager.peek(localDev, "No such property", spuser);									validate(r, "  Tried to peek at a non-existent property called \"No such property\"\n  Device responded: " + r, R_CODE.E_PROPNOTFOUND, "No such property");
		r = daManager.poke(localDev, Const.DeviceDescr, "Descr. updated by SP user", spuser);		validate(r, "  Let SP modify device description\n  Device responded: " + r, R_CODE.OK);
		r = daManager.peek(localDev, Const.DeviceDescr, spuser);									validate(r, "  Did he succeed? Check the new value of the property\n  Device responded: " + r, "Descr. updated by SP user");		
		r = daManager.peek(localDev, "Firmware Version", spuser);									validate(r, "  Let SP see the firmware version (it was updated by DS, remember?) \n  Device responded: " + r, "1.0.1"); 
		r = daManager.poke(localDev, Const.DeviceClass, "This ain't gonna work", spuser);			validate(r, "  SP/DS shouldn't be able to poke stuff into 'Device class', it's read-only\n" + "  Device responded: " + r, R_CODE.E_ISREADONLY, Const.DeviceClass);
		r = daManager.poke(localDev, Const.DeviceClass, "This ain't gonna work either", enduser);	validate(r, "  And even end-users can't do that\n" + "  Device responded: " + r, R_CODE.E_ISREADONLY, Const.DeviceClass);		
		//  Test uptime
		r = daManager.peek(localDev, Const.Uptime, enduser);										validate(r, "  Peeked at 'Uptime' of " + localDevURL + " - compare the value to what we had last time!\n  Device responded: " + r, "[0-9]+");
		r = daManager.poke(localDev, Const.IsOn, "false", enduser);									validate(r, "  Now turn it off and wait a couple of seconds...\n  Device responded: " + r, R_CODE.OK);
		Resp.delay(2);
		r = daManager.peek(localDev, Const.Uptime, enduser);										validate(r, "  Peeked at 'Uptime' of " + localDevURL + " - see, uptime didn't change because the device was off\n  Device responded: " + r, "[0-9]+");
		r = daManager.poke(localDev, Const.IsOn, "true", enduser);									validate(r, "  Turn on and wait 2 seconds..." + localDevURL +"\n  Device responded: " + r, R_CODE.OK);
		Resp.delay(2);
		r = daManager.peek(localDev, Const.Uptime, enduser);										validate(r, "  Uptime started ticking again!\n  Device responded: " + r, "[0-9]+");
		
		Resp.log("\n--- Testing rules again, this time on our existing device ---\n"); // **************************************************************
		Resp.log("  First we need to remove the old rules as they'll be getting in the way of testing");
		r = rulesManager.rmRule("Heating rule", enduser);											validate(r, "  Removing the Heating rule: " + r, R_CODE.OK);
		r = rulesManager.rmRule("Sound rule", enduser);												validate(r, "  Removing the Sound rule: " + r, R_CODE.OK);
		r = rulesManager.lsRules(enduser);															validate(r, "  Now our rules list must be empty: <" + r + ">", "");
		r = daManager.peek(localDev, Const.IsOn, spuser);											validate(r, "  Peeked at 'Is On' of " + localDevURL + " - our light bulb should be on right now\n  Device responded: " + r, "true");
		r = rulesManager.addRule("Lights on-off rule", "IF (" + localDevURL + " 'Is On' == true) POKE[", localDevURL + " 'Is On' false]", spuser);	validate(r, "  This rule turns our lamp OFF if and only if it was ON: " + r, R_CODE.OK);
		r = rulesManager.lsRules(enduser);															validate(r, "\nLet's check our rules list: \n" + r, "Rule 1	Lights on-off rule	G37.G37_Controller.impl.EventConditionImpl@.+	G37.G37_Controller.impl.ActionImpl@.+ \\(property: Is On, value: false\\)	sp\n");
		r = rulesManager.executeAll(); 																validate(r, "  Executing all rules: " + r, R_CODE.OK);
		r = daManager.peek(localDev, Const.IsOn, spuser);											validate(r, "  Peeked at 'Is On' of " + localDevURL + " - our light bulb should be off right now\n  Device responded: " + r, "false");
		r = rulesManager.addRule("Lights off-on rule",  "IF (" + localDevURL + " 'Is On' NOT == true) POKE[", localDevURL + " 'Is On' true]", spuser);	validate(r, "  This rule turns our lamp ON if and only if it was OFF: " + r, R_CODE.OK);
		r = rulesManager.executeAll(); 																validate(r, "  Executing all rules: " + r, R_CODE.OK);
		r = daManager.peek(localDev, Const.IsOn, spuser);											validate(r, "  Peeked at 'Is On' of " + localDevURL + " - our light bulb should be on right now\n  Device responded: " + r, "true");
		r = rulesManager.executeAll(); 																validate(r, "  Now with each execution, our bulb should go on / off every time. Executing: " + r, R_CODE.OK);	
		r = daManager.peek(localDev, Const.IsOn, spuser);											validate(r, "  Peeked at 'Is On' of " + localDevURL + " - our light bulb would be off right now IF OUR EVENT CONDITIONS WORKED, BUT THEY DON'T SO IT'S ON \n  Device responded: " + r, "true");
		r = rulesManager.executeAll(); 																validate(r, "  Executing once again: " + r, R_CODE.OK);			
		r = daManager.peek(localDev, Const.IsOn, spuser);											validate(r, "  Peeked at 'Is On' of " + localDevURL + " - our light bulb should be on right now\n  Device responded: " + r, "true");
		
		Resp.log("  Shutting down the device " + localDevURL + "...");
		Resp.log("  Device responded: " + daManager.sendStringToG37D(localDev, "SHUTDOWN")); // Shut the device down
	} // runTests1
	
public static void runTests2() {
		String r;
		UserAccount enduser, spuser, dsuser;
		
		Resp.log("--- Testing users ---\n"); // **************************************************************
		r = uaManager.addUser("admin", "123", "This is the default user account", Const.EU);		validate(r, "  Adding end-user called 'admin':    " + r, R_CODE.OK);
		r = uaManager.addUser("ds", "3456", "Device supplier", Const.DS);							validate(r, "  Adding end-user called 'ds':    " + r, R_CODE.OK);
		enduser = uaManager.getUserAccountByUserName("admin");
		dsuser = uaManager.getUserAccountByUserName("ds");		
		
		Resp.log("\n--- Testing rooms ---\n"); // **************************************************************
		r = roomsManager.addRoom("Living room");													validate(r, "  Adding Living room:        " + r, R_CODE.OK);
		
		Resp.log("\n--- Testing peek and poke ---\n"); // **************************************************************
		String localDevURL = "127.0.0.1:8112";
		String localDevURL2 = "127.0.0.1:8113";
		r = daManager.reg(localDevURL, "Living room");												validate(r, "  Adding 127.0.0.1:8111 to Living room:   " + r + "\n", R_CODE.OK);
		DeviceAccount localDev = daManager.getDeviceAccountByURL(localDevURL);
		assert localDev.getUrl().equals(localDevURL);
		r = daManager.peek(localDev, "Temperature", enduser);										validate(r, "  Peeked at 'Temperature' of " + localDevURL + "\n  Device responded: " + r, "[0-9]+");
		Resp.delay(2);
		r = daManager.peek(localDev, "Temperature", enduser);										validate(r, "  Peeked at 'Temperature' of " + localDevURL + "\n  Device responded: " + r, "[0-9]+");
		Resp.delay(2);
		r = daManager.peek(localDev, "Temperature", enduser);										validate(r, "  Peeked at 'Temperature' of " + localDevURL + "\n  Device responded: " + r, "[0-9]+");
		r = daManager.peek(localDev, "Temperature", dsuser);										validate(r, "  Peeked at 'Temperature' of " + localDevURL + "\n  Device responded: " + r, R_CODE.E_ACCESSDENIEDNOSUBSCR, "127.0.0.1:8112 user ds");
		// Play around with the Smart window
		r = daManager.reg(localDevURL2, "Living room");												validate(r, "  Adding 127.0.0.1:8113 to Living room:   " + r + "\n", R_CODE.OK);
		DeviceAccount localDev2 = daManager.getDeviceAccountByURL(localDevURL2);
		assert localDev2.getUrl().equals(localDevURL2);
		r = daManager.peek(localDev2, "Device description", enduser);								validate(r, "  Peeked at 'Device description' of " + localDevURL2 + "\n  Device responded: " + r, "This is a smart automatic window from Fakro");
		r = daManager.peek(localDev2, "Device description", dsuser);								validate(r, "  Peeked at 'Device description' of " + localDevURL2 + "\n  Device responded: " + r, R_CODE.E_ACCESSDENIEDNOSUBSCR, "127.0.0.1:8113 user ds");
		Resp.delay(2);
		Resp.log("  Shutting down the device " + localDevURL + "...");
		Resp.log("  Device responded: " + daManager.sendStringToG37D(localDev, "SHUTDOWN")); // Shut the device down
		Resp.log("  Shutting down the device " + localDevURL2 + "...");
		Resp.log("  Device responded: " + daManager.sendStringToG37D(localDev2, "SHUTDOWN")); // Shut the device down
		
	} // runTests2

public static void runTests3() { // This is a separate space for testing our event conditions 
	String r;
	UserAccount enduser;
	r = uaManager.addUser("admin", "123", "This is the default user account", Const.EU);		validate(r, "  Adding end-user called 'admin':    " + r, R_CODE.OK);
	enduser = uaManager.getUserAccountByUserName("admin");
	r = roomsManager.addRoom("Living room");													validate(r, "  Adding Living room:        " + r, R_CODE.OK);
	// Create an imitated device. When you "peek" at any property of it, you get back the name of that property. E.g. peek(testDa, "bonanza", user) returns "bonanza" 
	r = daManager.reg(Const.DevForTestingPurposes, "Living room");								validate(r, "  Adding an imitated testing device to Living room:   " + r, R_CODE.OK);
	DeviceAccount simulatedDev = daManager.getDeviceAccountByURL(Const.DevForTestingPurposes);
	assert simulatedDev.getUrl().equals(Const.DevForTestingPurposes);
	r = daManager.peek(simulatedDev, "banana", enduser);										validate(r, "  The \"banana\" property of the simulated testing device must return \"banana\". Device responded: " + r, "banana");	
	r = daManager.peek(simulatedDev, Const.NonExistentPropertyForTestingPurposes, enduser);		validate(r, "  Peeking at the \"" + Const.NonExistentPropertyForTestingPurposes + "\" property of the simulated testing device, device responded: " + r, Resp.response(R_CODE.E_PROPNOTFOUND, Const.NonExistentPropertyForTestingPurposes));
	r = daManager.poke(simulatedDev, "banana", "toothpick", enduser);							validate(r, "  Poking anything into any property of the simulated testing device must return OK. Device responded: " + r, R_CODE.OK);
	r = daManager.poke(simulatedDev, Const.NonExistentPropertyForTestingPurposes, "0", enduser);validate(r, "  ...except for the one and only \"" + Const.NonExistentPropertyForTestingPurposes + "\" property. Device responded: " + r, Resp.response(R_CODE.E_PROPNOTFOUND, Const.NonExistentPropertyForTestingPurposes));
	// Add a few rules
	r = rulesManager.addRule("Rule that shouldn't fire up", "IF (" + Const.DevForTestingPurposes + " bonanza == bonanza) "
	      + "AND (" + Const.DevForTestingPurposes + " kikomi == bukomi) POKE [" + Const.DevForTestingPurposes + " 'Is On' true]", enduser);	
																								validate(r, "  Adding a rule that shouldn't fire up: " + r, R_CODE.OK);
	r = rulesManager.addRule("Rule that should fire up", "IF (" + Const.DevForTestingPurposes + " bonanza == bonanza) "
		      + "OR (" + Const.DevForTestingPurposes + " kikomi == bukomi) POKE [" + Const.DevForTestingPurposes + " 'Is On' true]", enduser);	
																								validate(r, "  Adding a rule that should fire up: " + r, R_CODE.OK);
	r = rulesManager.addRule("Rule with 3 elementary EC; should fire up", "IF (" + Const.DevForTestingPurposes + " bonanza == bonanza) "
		      + "OR (" + Const.DevForTestingPurposes + " kikomi == bukomi) "
		      + "AND (" + Const.DevForTestingPurposes + " mubba == mubba) "
		      + "EMAIL [Greetings from the greatest home automation system this side of the equator.]", enduser);	
																								validate(r, "  Adding a rule with 3 elementary EC; should fire up: " + r, R_CODE.OK);
    // Execute all rules
	r = rulesManager.executeAll(); 																validate(r, "\nExecuting all rules: " + r, R_CODE.OK);
	

}

public static void runTests4() { // This is a separate space for testing our latest "doCommand" method.

	// Testing the doCommand that accepts "short" strings (command and arguments in separate strings)
	Resp.log(controller.doCommand("addUser", "admin", "pwdabc", "Admin user for testing", "EU"));
	Resp.log(controller.doCommand("addRoom", "The hall"));
	Resp.log(controller.doCommand("reg", Const.DevForTestingPurposes, "The hall"));
	Resp.log(controller.doCommand("addRule", "Rule4", "IF (" + Const.DevForTestingPurposes + " bonanza == bonanza) "
	    + "AND (" + Const.DevForTestingPurposes + " kikomi == bukomi) POKE [" + Const.DevForTestingPurposes + " 'Is On' true]", "admin"));
	// Testing the doCommand that accepts "long" strings (everything as one string). Arguments with spaces must be in quotes. INSIDE arguments, SINGLE quotes are to be used if needed.
	String longCommand = "addRule   \"Funny rule\"    \" IF  (" + Const.DevForTestingPurposes + "  'ripe pineapple' NOT == 'green pineapple') "
			+ "AND (" + Const.DevForTestingPurposes + "  'left hand' NOT  == 'right foot') POKE [" + Const.DevForTestingPurposes + " 'Some property' 'some value']  \"   admin";
	Resp.log(controller.doCommand(longCommand));
	longCommand = "peek    "  + Const.DevForTestingPurposes + "\"  'Big Green Monkey' \"   admin";
	Resp.log(controller.doCommand(longCommand));
	Resp.log(controller.executeAllRules());
}

public static void runTests5() {
	String r;
	UserAccount enduser, spuser, dsuser;
	r = uaManager.addUser("admin", "123", "This is the default user account", Const.EU);		validate(r, "  Adding end-user called 'admin':    " + r, R_CODE.OK);
	r = uaManager.addUser("panasonic service", "567", "This is the user account for the Panasonic service company", Const.SP);		validate(r, "  Adding SP-user called 'panasonic service':    " + r, R_CODE.OK);
	r = roomsManager.addRoom("Bathroom 1");														validate(r, "  Adding room 'Bathroom 1':        " + r, R_CODE.OK);
	String localDevURL = "127.0.0.1:8114";
	r = daManager.reg(localDevURL, "Bathroom 1");												validate(r, "  Adding a vent fan to Bathroom 1:   " + r + "\n", R_CODE.OK);
	DeviceAccount localDev = daManager.getDeviceAccountByURL(localDevURL);
	assert localDev.getUrl().equals(localDevURL);
	enduser = uaManager.getUserAccountByUserName("admin");
	assert enduser.getUsername().equals("admin");
	spuser = uaManager.getUserAccountByUserName("panasonic service");
	assert spuser.getUsername().equals("panasonic service");
	r = daManager.peek(localDev, Const.DeviceClass, spuser);									validate(r, "  Peeked at 'Device class' of " + localDevURL + "\n  Device responded: " + r, R_CODE.E_ACCESSDENIEDNOSUBSCR, localDevURL + " user " + spuser.getUsername());
	Resp.log("  Oops, we forgot to subscribe this user to this device...");
	r = subscrManager.addSubscr("Vent fan subscr.", "Vent fan", "Bathroom 1", "panasonic service");		validate(r, "  Adding a 'Vent fan' subscription to 'Bathroom 1': " + r, R_CODE.OK);
	r = daManager.peek(localDev, Const.DeviceClass, spuser);									validate(r, "  Again peeked at 'Device class' of " + localDevURL + "\n  Device responded: " + r, "Vent fan");
	r = daManager.peek(localDev, Const.Speed, spuser);										    validate(r, "  Peeked at 'Speed' of " + localDevURL + "\n  Device responded: " + r, "[0-9]+");
	r = daManager.poke(localDev, Const.Speed, "3", spuser);										validate(r, "  Setting the speed of " + localDevURL + " to 3.\n  Device responded: " + r, R_CODE.OK);
	r = daManager.peek(localDev, Const.Speed, spuser);										    validate(r, "  is the speed 3 now?" + "\n  Device responded: " + r, "3");
	r = rulesManager.addRule("Filter alert", "IF (" + localDevURL + " 'Filter needs cleaning' == true) EMAIL [Alert! Please come and clean filter!]", spuser);	validate(r, "  Adding a 'Filter alert' rule: " + r, R_CODE.OK);									
	Resp.log("  Now from time to time it should be sending us alert emails:");
	for (int i = 0; i < 10000000; i++) {
		controller.executeAllRules();
		String r1 = daManager.pingAll(); 
		if (!r1.equals(Const.Alive)) {
			Resp.log(r);
			break;
		}
		Resp.delay(1); 
	}
	Resp.log("  Shutting down the device " + localDevURL + "...");
	Resp.log("  Device responded: " + daManager.sendStringToG37D(localDev, "SHUTDOWN")); // Shut the device down
}
	
/* ****************************************************************************************************************
 * ***************** M A I N **************************************************************************************
 * ****************************************************************************************************************
 */
public static void main(String[] args) {
		Resp.log("=== G37 Controller launched ===\n");
		controllerFactory = G37_ControllerFactory.eINSTANCE; 
		controller = controllerFactory.createG37C();
		controller.setup(args);
		uaManager = controller.getUaManager();
		daManager = controller.getDaManager();
		roomsManager = controller.getRoomsManager();
		subscrManager = controller.getSubscrManager();
		rulesManager = controller.getRulesManager();
		//controller.RunOnSockets(Const.ControllerListenerPort);
		// Run our test suites.  Remember to start the corresponding devices first!
		//runTests1();  // Call this only if you first started Smart_light_bulb_Philips.json !!!
		runTests2();  // Call this only if you first started Smart_indoor_thermometer_AcuRite.json and Smart_window_Fakro.json!!!
		//runTests3(); // This test suite only works with a simulated device so you don't need to start any "real" devices.
		//runTests4();    // Same as runTests3()
		//runTests5();  // Call this only if you first started Smart_vent_fan_Panasonic.json !!!
	}

}

