/**
 */
package G37.G37_Controller.impl;

import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.GenericSubscription;
import G37.G37_Controller.Room;
import G37.G37_Controller.UserAccount;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Subscription</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.GenericSubscriptionImpl#getId <em>Id</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.GenericSubscriptionImpl#getDevClass <em>Dev Class</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.GenericSubscriptionImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link G37.G37_Controller.impl.GenericSubscriptionImpl#getUser <em>User</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GenericSubscriptionImpl extends MinimalEObjectImpl.Container implements GenericSubscription {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDevClass() <em>Dev Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevClass()
	 * @generated
	 * @ordered
	 */
	protected static final String DEV_CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDevClass() <em>Dev Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevClass()
	 * @generated
	 * @ordered
	 */
	protected String devClass = DEV_CLASS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getUser() <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUser()
	 * @generated
	 * @ordered
	 */
	protected UserAccount user;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GenericSubscriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.GENERIC_SUBSCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.GENERIC_SUBSCRIPTION__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDevClass() {
		return devClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDevClass(String newDevClass) {
		String oldDevClass = devClass;
		devClass = newDevClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.GENERIC_SUBSCRIPTION__DEV_CLASS, oldDevClass, devClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectResolvingEList<Room>(Room.class, this, G37_ControllerPackage.GENERIC_SUBSCRIPTION__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UserAccount getUser() {
		if (user != null && user.eIsProxy()) {
			InternalEObject oldUser = (InternalEObject)user;
			user = (UserAccount)eResolveProxy(oldUser);
			if (user != oldUser) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, G37_ControllerPackage.GENERIC_SUBSCRIPTION__USER, oldUser, user));
			}
		}
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserAccount basicGetUser() {
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUser(UserAccount newUser) {
		UserAccount oldUser = user;
		user = newUser;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, G37_ControllerPackage.GENERIC_SUBSCRIPTION__USER, oldUser, user));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__ID:
				return getId();
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__DEV_CLASS:
				return getDevClass();
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__ROOMS:
				return getRooms();
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__USER:
				if (resolve) return getUser();
				return basicGetUser();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__ID:
				setId((String)newValue);
				return;
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__DEV_CLASS:
				setDevClass((String)newValue);
				return;
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__USER:
				setUser((UserAccount)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__ID:
				setId(ID_EDEFAULT);
				return;
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__DEV_CLASS:
				setDevClass(DEV_CLASS_EDEFAULT);
				return;
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__ROOMS:
				getRooms().clear();
				return;
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__USER:
				setUser((UserAccount)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__DEV_CLASS:
				return DEV_CLASS_EDEFAULT == null ? devClass != null : !DEV_CLASS_EDEFAULT.equals(devClass);
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case G37_ControllerPackage.GENERIC_SUBSCRIPTION__USER:
				return user != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", devClass: ");
		result.append(devClass);
		result.append(')');
		return result.toString();
	}

} //GenericSubscriptionImpl
