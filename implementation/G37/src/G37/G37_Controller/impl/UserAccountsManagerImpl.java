/**
 */
package G37.G37_Controller.impl;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceSupplier;
import G37.G37_Controller.EndUser;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.G37_ControllerPackage;
import G37.G37_Controller.ServiceProvider;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;
import G37.G37_RestAPI.dao.UserDAO;
import G37.G37_RestAPI.models.User;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Accounts Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.impl.UserAccountsManagerImpl#getUserAccounts <em>User Accounts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UserAccountsManagerImpl extends MinimalEObjectImpl.Container implements UserAccountsManager {
	/**
	 * The cached value of the '{@link #getUserAccounts() <em>User Accounts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserAccounts()
	 * @generated
	 * @ordered
	 */
	
	protected static EList<UserAccount> userAccounts;

	private G37_ControllerFactory controllerFactory;
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserAccountsManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return G37_ControllerPackage.Literals.USER_ACCOUNTS_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<UserAccount> getUserAccounts() {
		if (userAccounts == null) {
			userAccounts = new EObjectContainmentEList<UserAccount>(UserAccount.class, this, G37_ControllerPackage.USER_ACCOUNTS_MANAGER__USER_ACCOUNTS);
		}
		return userAccounts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case G37_ControllerPackage.USER_ACCOUNTS_MANAGER__USER_ACCOUNTS:
				return ((InternalEList<?>)getUserAccounts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case G37_ControllerPackage.USER_ACCOUNTS_MANAGER__USER_ACCOUNTS:
				return getUserAccounts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case G37_ControllerPackage.USER_ACCOUNTS_MANAGER__USER_ACCOUNTS:
				getUserAccounts().clear();
				getUserAccounts().addAll((Collection<? extends UserAccount>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.USER_ACCOUNTS_MANAGER__USER_ACCOUNTS:
				getUserAccounts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case G37_ControllerPackage.USER_ACCOUNTS_MANAGER__USER_ACCOUNTS:
				return userAccounts != null && !userAccounts.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	// This method sets the controller factory member of this object.
	public void setControllerFactory(G37_ControllerFactory cf) {
		controllerFactory = cf;
	}
	
	// This method adds a new user to our list of users.
	public String addUser(UserAccount ua) {
		if (ua != null) {
			userAccounts.add(ua);
			Resp.response(R_CODE.OK);
		}
		return Resp.response(R_CODE.E_INVUSERTYPE, "null reference to user account received by addUser");
	}
	
	// This is another implementation of the ADDUSER command: create a new user account 
	// with the attributes provided, and add it to our list of user.
	public String addUser(String username, String pwd, String descr, String type) {
		if (username == "") return Resp.response(R_CODE.E_USERNAMEEMPTY);
		if (pwd == "") return Resp.response(R_CODE.E_PWDEMPTY);
		
		// Check if a user with such username already exists
		UserAccount ua = getUserAccountByUserName(username);
		if (ua != null) return Resp.response(R_CODE.E_USERNAMEEXISTS, username);
		
		// Try to create a user with the right type
		switch (type.toUpperCase()) {
			case Const.EU: ua = controllerFactory.createEndUser(); break; 
			case Const.DS: ua = controllerFactory.createDeviceSupplier(); break; 
			case Const.SP: ua = controllerFactory.createServiceProvider(); break; 
			default: return Resp.response(R_CODE.E_INVUSERTYPE, type);
		}
		
		// Set the user's fields
		ua.setUsername(username);
		ua.setPassword(pwd);
		ua.setDescription(descr);
		
		// Add the user to the controller's list
		userAccounts.add(ua);
		
		// Store user in database
		User user = new User();
		user.setUserName(username);
		user.setPassword(pwd);
		user.setUserDescription(descr);
		user.setUserType(type);
		UserDAO userDao = new UserDAO();
		boolean userSaved = userDao.saveUser(user);
		if (!userSaved)
			return Resp.response(R_CODE.E_USERNAMEEXISTS, ua.getUsername());
		
		return Resp.response(R_CODE.OK); 
	}
		
	// This method lists all existing users.
	public String lsUsers() {
		String userType;
		String result = "";
		int i = 1;
		for (UserAccount ua : getUserAccounts()) {
			if (ua instanceof EndUser) userType = "End user\t";
			else if (ua instanceof ServiceProvider) userType = "Service provider";
			else if (ua instanceof DeviceSupplier) userType = "Device supplier\t";
			else throw new IllegalArgumentException("Invalid user class: " + ua.getClass());
			result += "User " + i++ + "\t" + userType + "\t" + ua.getUsername() + "\t" + ua.getDescription() + "\n";
		}
		return result;
	}
	
	// This method deletes a user with the given username.
	public String rmUser(String username) {
		UserAccount ua = getUserAccountByUserName(username);
		if (ua != null) {
			userAccounts.remove(ua);
			return Resp.response(R_CODE.OK);
		}
		return Resp.response(R_CODE.E_USERNAMENOTFOUND, username);
	}
	
	// This method deletes a user identified by user account
	public String rmUser(UserAccount ua) {
		if (ua != null) {
			userAccounts.remove(ua);
			return Resp.response(R_CODE.OK);
		}
		return Resp.response(R_CODE.E_INVUSERTYPE, "null reference to user account received by rmUser");
	}	
	
	// This method returns the user account as identified by the username.
	public UserAccount getUserAccountByUserName(String username) {
		UserAccount userAccount = null;
		for (UserAccount ua : getUserAccounts()) {
			if (ua.getUsername().equals(username)) {
				userAccount = ua;
				break;
			}
		}
		return userAccount;
	}

} //UserAccountsManagerImpl
