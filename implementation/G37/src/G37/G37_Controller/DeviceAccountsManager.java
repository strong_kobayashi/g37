/**
 */
package G37.G37_Controller;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Device Accounts Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.DeviceAccountsManager#getDeviceAccounts <em>Device Accounts</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getDeviceAccountsManager()
 * @model
 * @generated
 */
public interface DeviceAccountsManager extends EObject {
	/**
	 * Returns the value of the '<em><b>Device Accounts</b></em>' containment reference list.
	 * The list contents are of type {@link G37.G37_Controller.DeviceAccount}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Accounts</em>' containment reference list.
	 * @see G37.G37_Controller.G37_ControllerPackage#getDeviceAccountsManager_DeviceAccounts()
	 * @model containment="true"
	 * @generated
	 */
	EList<DeviceAccount> getDeviceAccounts();
	
	// Sets the controllerFactory member of this object.
	void setControllerFactory(G37_ControllerFactory cf);
	
	// Sets the roomsManager member of this object.
	void setRoomsManager(RoomsManager rMan);
	
	// Set the subscrManager member of this object.
	void setSubscrManager(SubscriptionsManager sM);
	
	// Returns the DeviceAccount object as identified by the device account's URL.
	DeviceAccount getDeviceAccountByURL(String devUrl);
	
	// Implementation of reg command.  Adds a new device account to our home automaton controller.
	String reg(String devUrl, String roomID);
	
	// Implementation of lsDev command. Lists the devices (all, or in a given room).
	String lsDev (String roomID, UserAccount callingUser);

	// Implementation of unreg command.  Deletes a device account with the given URL.
	String unreg(String devUrl);
	
	// Implementation of mv command. Reassigns the device to another room.
	String mv(String devUrl, String roomID);
	
	// This method sends a string to the device as identified by the device account, and returns the reply.
	String sendStringToG37D(DeviceAccount devAcc, String strToSend);
	
	// Implementation of peek command.  Looks up a property.
	String peek(DeviceAccount da, String prop, UserAccount callingUser);
	
	// Implementation of peek command.  Looks up a property.
	String poke(DeviceAccount da, String prop, String val, UserAccount callingUser);
	
	// Ping commands are for internal use.  Just ping the device(s).
	String ping(DeviceAccount da);
	String pingAll();
	
} // DeviceAccountsManager
