/**
 */
package G37.G37_Controller;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>End User</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getEndUser()
 * @model
 * @generated
 */
public interface EndUser extends UserAccount {
} // EndUser
