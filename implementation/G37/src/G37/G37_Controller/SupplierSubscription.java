/**
 */
package G37.G37_Controller;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Supplier Subscription</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getSupplierSubscription()
 * @model
 * @generated
 */
public interface SupplierSubscription extends GenericSubscription {
} // SupplierSubscription
