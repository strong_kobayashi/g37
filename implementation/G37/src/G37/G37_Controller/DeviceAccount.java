/**
 */
package G37.G37_Controller;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Device Account</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.DeviceAccount#getUrl <em>Url</em>}</li>
 *   <li>{@link G37.G37_Controller.DeviceAccount#getRoom <em>Room</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getDeviceAccount()
 * @model
 * @generated
 */
public interface DeviceAccount extends EObject {
	/**
	 * Returns the value of the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url</em>' attribute.
	 * @see #setUrl(String)
	 * @see G37.G37_Controller.G37_ControllerPackage#getDeviceAccount_Url()
	 * @model required="true"
	 * @generated
	 */
	String getUrl();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.DeviceAccount#getUrl <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url</em>' attribute.
	 * @see #getUrl()
	 * @generated
	 */
	void setUrl(String value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link G37.G37_Controller.Room#getDeviceAccounts <em>Device Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' reference.
	 * @see #setRoom(Room)
	 * @see G37.G37_Controller.G37_ControllerPackage#getDeviceAccount_Room()
	 * @see G37.G37_Controller.Room#getDeviceAccounts
	 * @model opposite="deviceAccounts"
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.DeviceAccount#getRoom <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' reference.
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

} // DeviceAccount
