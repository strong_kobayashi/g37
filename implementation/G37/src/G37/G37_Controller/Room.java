/**
 */
package G37.G37_Controller;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.Room#getId <em>Id</em>}</li>
 *   <li>{@link G37.G37_Controller.Room#getDeviceAccounts <em>Device Accounts</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see G37.G37_Controller.G37_ControllerPackage#getRoom_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.Room#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Device Accounts</b></em>' reference list.
	 * The list contents are of type {@link G37.G37_Controller.DeviceAccount}.
	 * It is bidirectional and its opposite is '{@link G37.G37_Controller.DeviceAccount#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Accounts</em>' reference list.
	 * @see G37.G37_Controller.G37_ControllerPackage#getRoom_DeviceAccounts()
	 * @see G37.G37_Controller.DeviceAccount#getRoom
	 * @model opposite="room"
	 * @generated
	 */
	EList<DeviceAccount> getDeviceAccounts();
	
	/*
	 * This method returns true if the room contains the given device,
	 * false otherwise.
	 * Author: sergeyb
	 */
	boolean contains(DeviceAccount da);

} // Room
