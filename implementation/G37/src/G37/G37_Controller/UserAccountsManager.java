/**
 */
package G37.G37_Controller;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Accounts Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.UserAccountsManager#getUserAccounts <em>User Accounts</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getUserAccountsManager()
 * @model
 * @generated
 */
public interface UserAccountsManager extends EObject {
	/**
	 * Returns the value of the '<em><b>User Accounts</b></em>' containment reference list.
	 * The list contents are of type {@link G37.G37_Controller.UserAccount}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Accounts</em>' containment reference list.
	 * @see G37.G37_Controller.G37_ControllerPackage#getUserAccountsManager_UserAccounts()
	 * @model containment="true"
	 * @generated
	 */
	EList<UserAccount> getUserAccounts();
	
	// This method sets the controller factory member of this object.
	void setControllerFactory(G37_ControllerFactory cf);
	
	// This method implements the ADDUSER command: adds a new user to our list of users.
	String addUser(UserAccount user);
	
	// This is another implementation of the ADDUSER command: create a new user account 
	// with the attributes provided, and add it to our list of user.
	public String addUser(String username, String pwd, String descr, String type);
	
	// This method implements the LSUSERS command: lists all existing users.
	String lsUsers();
	
	// This method implements the RMUSER command: deletes a user identified by the reference
	// to its user account.
	String rmUser(UserAccount ua);
	
	// This is another implementation of the RMUSER command: deletes a user with the given username.
	String rmUser(String username);
	
	// Return the user account as identified by its name
	UserAccount getUserAccountByUserName(String username);

} // UserAccountsManager
