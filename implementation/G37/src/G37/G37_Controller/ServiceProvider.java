/**
 */
package G37.G37_Controller;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getServiceProvider()
 * @model
 * @generated
 */
public interface ServiceProvider extends UserAccount {
} // ServiceProvider
