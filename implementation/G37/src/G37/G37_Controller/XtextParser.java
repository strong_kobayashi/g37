package G37.G37_Controller;

import java.io.IOException;
import java.io.Reader;

import org.eclipse.emf.ecore.EObject;

public interface XtextParser {

	    /**
	     * Parses data provided by an input reader using Xtext and returns the root node of the resulting object tree.
	     * @param reader Input reader
	     * @return root object node
	     * @throws IOException when errors occur during the parsing process
	     */
	    public EObject parse(Reader reader) throws IOException;
	
}
