/**
 */
package G37.G37_Controller;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subscriptions Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.SubscriptionsManager#getSubscriptions <em>Subscriptions</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getSubscriptionsManager()
 * @model
 * @generated
 */
public interface SubscriptionsManager extends EObject {
	/**
	 * Returns the value of the '<em><b>Subscriptions</b></em>' containment reference list.
	 * The list contents are of type {@link G37.G37_Controller.GenericSubscription}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subscriptions</em>' containment reference list.
	 * @see G37.G37_Controller.G37_ControllerPackage#getSubscriptionsManager_Subscriptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<GenericSubscription> getSubscriptions();
	
	// Sets the controllerFactory member of this object.
	void setControllerFactory(G37_ControllerFactory cf);
	
	// Sets the uaManager member of this object.
	void setUserAccountsManager(UserAccountsManager uaM);
	
	// Sets the RoomsManager member of this object.
	void setRoomsManager(RoomsManager rM);
	
	// Implementation of ADDSUBSCR command.  Adds a new subscription to our home automaton controller.
	String addSubscr(String subscrID, String devClass, String listOfRooms, String username);
	
	// Implementation of LSSUBSCR command. Lists the subscriptions.
	String lsSubscr(UserAccount callingUser);
	
	// Implementation of RMSUBSCR command.  Deletes a device account with the given URL.
	String rmSubscr(String subscrID);
	
	// Check if there exists a subscription that allows a given user to access a given device.
	boolean subscriptionExists(DeviceAccount da, UserAccount ua);
	
} // SubscriptionsManager
