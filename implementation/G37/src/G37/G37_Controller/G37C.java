/**
 */
package G37.G37_Controller;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>G37C</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link G37.G37_Controller.G37C#getUaManager <em>Ua Manager</em>}</li>
 *   <li>{@link G37.G37_Controller.G37C#getDaManager <em>Da Manager</em>}</li>
 *   <li>{@link G37.G37_Controller.G37C#getRoomsManager <em>Rooms Manager</em>}</li>
 *   <li>{@link G37.G37_Controller.G37C#getSubscrManager <em>Subscr Manager</em>}</li>
 *   <li>{@link G37.G37_Controller.G37C#getRulesManager <em>Rules Manager</em>}</li>
 * </ul>
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getG37C()
 * @model
 * @generated
 */
public interface G37C extends EObject {
	/**
	 * Returns the value of the '<em><b>Ua Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ua Manager</em>' reference.
	 * @see #setUaManager(UserAccountsManager)
	 * @see G37.G37_Controller.G37_ControllerPackage#getG37C_UaManager()
	 * @model
	 * @generated
	 */
	UserAccountsManager getUaManager();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.G37C#getUaManager <em>Ua Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ua Manager</em>' reference.
	 * @see #getUaManager()
	 * @generated
	 */
	void setUaManager(UserAccountsManager value);

	/**
	 * Returns the value of the '<em><b>Da Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Da Manager</em>' reference.
	 * @see #setDaManager(DeviceAccountsManager)
	 * @see G37.G37_Controller.G37_ControllerPackage#getG37C_DaManager()
	 * @model
	 * @generated
	 */
	DeviceAccountsManager getDaManager();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.G37C#getDaManager <em>Da Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Da Manager</em>' reference.
	 * @see #getDaManager()
	 * @generated
	 */
	void setDaManager(DeviceAccountsManager value);

	/**
	 * Returns the value of the '<em><b>Rooms Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms Manager</em>' reference.
	 * @see #setRoomsManager(RoomsManager)
	 * @see G37.G37_Controller.G37_ControllerPackage#getG37C_RoomsManager()
	 * @model
	 * @generated
	 */
	RoomsManager getRoomsManager();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.G37C#getRoomsManager <em>Rooms Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rooms Manager</em>' reference.
	 * @see #getRoomsManager()
	 * @generated
	 */
	void setRoomsManager(RoomsManager value);

	/**
	 * Returns the value of the '<em><b>Subscr Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subscr Manager</em>' reference.
	 * @see #setSubscrManager(SubscriptionsManager)
	 * @see G37.G37_Controller.G37_ControllerPackage#getG37C_SubscrManager()
	 * @model
	 * @generated
	 */
	SubscriptionsManager getSubscrManager();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.G37C#getSubscrManager <em>Subscr Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subscr Manager</em>' reference.
	 * @see #getSubscrManager()
	 * @generated
	 */
	void setSubscrManager(SubscriptionsManager value);

	/**
	 * Returns the value of the '<em><b>Rules Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules Manager</em>' reference.
	 * @see #setRulesManager(RulesManager)
	 * @see G37.G37_Controller.G37_ControllerPackage#getG37C_RulesManager()
	 * @model
	 * @generated
	 */
	RulesManager getRulesManager();

	/**
	 * Sets the value of the '{@link G37.G37_Controller.G37C#getRulesManager <em>Rules Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rules Manager</em>' reference.
	 * @see #getRulesManager()
	 * @generated
	 */
	void setRulesManager(RulesManager value);
	
	// This method does the initialization of all managers etc.
	void setup(String args[]);
	
	// These 2 methods execute a command (provided in 2 possible forms) by calling the appropriate command of the appropriate manager.
	String doCommand(String command, String... args);
	String doCommand(String commandWithArgs);
	
	// This method executes all rules. Returns OK if all executed without errors, or the first error otherwise.
	String executeAllRules();

	// This method provides controller's main loop for the _fallback_scenario_ of using sockets for user<->controller communication.
	void RunOnSockets(int port);
	
} // G37C
