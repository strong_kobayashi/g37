/**
 */
package G37.G37_Controller;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Subscription</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getServiceSubscription()
 * @model
 * @generated
 */
public interface ServiceSubscription extends GenericSubscription {
} // ServiceSubscription
