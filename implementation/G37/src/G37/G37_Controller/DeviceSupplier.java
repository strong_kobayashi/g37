/**
 */
package G37.G37_Controller;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Device Supplier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see G37.G37_Controller.G37_ControllerPackage#getDeviceSupplier()
 * @model
 * @generated
 */
public interface DeviceSupplier extends UserAccount {
} // DeviceSupplier
