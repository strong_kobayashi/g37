package G37.G37_RestAPI.dao;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

import G37.G37_Controller.UserAccount;
import G37.G37_RestAPI.models.User;
import G37.G37_RestAPI.utils.EncryptionUtil;
import G37.G37_RestAPI.utils.HibernateUtil;

public class UserDAO {
	
	// Stores a new user in the database.
	public boolean saveUser(User user) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	user.setPassword(EncryptionUtil.encryptPassword(user.getPassword()));
            session.save(user);
            return true;
        } catch (final ConstraintViolationException e) {
            e.printStackTrace();
            return false;
        }
    }

	// Looks for the username of a user in the database and returns the corresponding User object,
	// if successful.
	public User findUserByName(String username) {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			Query<User> q = session.createQuery("from User where username = :username", User.class);
			q.setParameter("username", username);
			return q.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		return null;
	}
}
