package G37.G37_RestAPI;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.G37C;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.RulesManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;
import G37.G37_RestAPI.models.Rule;
import G37.G37_RestAPI.models.Subscription;
import G37.G37_RestAPI.models.User;
import G37.G37_RestAPI.utils.EncryptionUtil;

@Path("/rule")
public class RuleResource {
	private G37C controller;
	private G37_ControllerFactory controllerFactory;
	private RulesManager rulesManager;
	private UserAccountsManager userAccountsManager;
	private DeviceAccountsManager deviceAccountsManager;

	public RuleResource() {
		controllerFactory = G37_ControllerFactory.eINSTANCE;
		rulesManager = controllerFactory.createRulesManager();
		rulesManager.setControllerFactory(controllerFactory);
		deviceAccountsManager = controllerFactory.createDeviceAccountsManager();
		deviceAccountsManager.setControllerFactory(controllerFactory);
		rulesManager.setDaManager(deviceAccountsManager);
		
		controller = controllerFactory.createG37C();
		controller.setRulesManager(rulesManager);
		controller.setUaManager(userAccountsManager);
		controller.setDaManager(deviceAccountsManager);
	}
	
	@POST
	@Path("/addrule")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addRule(Rule rule) {
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(rule.getUserName());
		
		User existingUserInDB = EncryptionUtil.validateToken(rule.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		String ruleAddResult = rulesManager.addRule(rule.getRuleName(), rule.getEventConditionAction(), callingUser);
		rulesManager.executeAll();
		
		return ruleAddResult;
	}
	
	@POST
	@Path("/rmrule")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String removeRule(Rule rule) {
		User existingUserInDB = EncryptionUtil.validateToken(rule.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(rule.getUserName());
		
		String ruleRemoveResult = rulesManager.rmRule(rule.getRuleName(), callingUser);
		rulesManager.executeAll();
		
		return ruleRemoveResult;
	}
	
	@GET
	@Path("/list/{userName}/{token}")
	public String listRules(@PathParam("userName") String userName, @PathParam("token") String token) {
		User existingUserInDB = EncryptionUtil.validateToken(token);
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(userName);

		String rules = rulesManager.lsRules(callingUser);
		rulesManager.executeAll();
		
		return rules;
	}
}
