package G37.G37_RestAPI;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.G37C;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.RoomsManager;
import G37.G37_Controller.RulesManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;
import G37.G37_RestAPI.models.Room;
import G37.G37_RestAPI.models.User;
import G37.G37_RestAPI.utils.EncryptionUtil;

@Path("/room")
public class RoomResource {
	private G37C controller;
	private G37_ControllerFactory controllerFactory;
	private RoomsManager roomsManager;
	private UserAccountsManager userAccountsManager;
	private DeviceAccountsManager deviceAccountsManager;
	private RulesManager rulesManager;
	
	public RoomResource() {
		controllerFactory = G37_ControllerFactory.eINSTANCE;
		roomsManager = controllerFactory.createRoomsManager();
		roomsManager.setControllerFactory(controllerFactory);
		deviceAccountsManager = controllerFactory.createDeviceAccountsManager();
		deviceAccountsManager.setControllerFactory(controllerFactory);
		rulesManager = controllerFactory.createRulesManager();
		rulesManager.setControllerFactory(controllerFactory);
		rulesManager.setDaManager(deviceAccountsManager);
		
		controller = controllerFactory.createG37C();
		controller.setRulesManager(rulesManager);
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addRoom(Room room) {
		User existingUserInDB = EncryptionUtil.validateToken(room.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(room.getUserName());
		if (callingUser == null)
			return "Invalid input.";
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		String roomCreateResult = roomsManager.addRoom(room.getRoomId());
		rulesManager.executeAll();
		
		return roomCreateResult;
	}

	@POST
	@Path("/remove")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String removeRoom(Room room) {
		User existingUserInDB = EncryptionUtil.validateToken(room.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(room.getUserName());
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		String roomRemoveResult = roomsManager.rmRoom(room.getRoomId());
		rulesManager.executeAll();
		
		return roomRemoveResult;
	}
	
	@GET
	@Path("/list/{userName}/{token}")
	public String listRooms(@PathParam("userName") String userName, @PathParam("token") String token) {
		User existingUserInDB = EncryptionUtil.validateToken(token);
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(userName);
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		String listRoomsResult = roomsManager.lsRooms();
		rulesManager.executeAll();
		
		return listRoomsResult;
	}
}
