package G37.G37_RestAPI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.G37C;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.RulesManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;
import G37.G37_Controller.impl.UserAccountsManagerImpl;
import G37.G37_RestAPI.dao.UserDAO;
import G37.G37_RestAPI.models.JWTResponse;
import G37.G37_RestAPI.models.User;
import G37.G37_RestAPI.utils.EncryptionUtil;

@Path("/user")
public class UserResource {
	private G37C controller;
	private G37_ControllerFactory controllerFactory;
	private UserAccountsManager userAccountsManager;
	private DeviceAccountsManager deviceAccountsManager;
	private RulesManager rulesManager;
	
	public UserResource() {
		controllerFactory = G37_ControllerFactory.eINSTANCE;
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		deviceAccountsManager = controllerFactory.createDeviceAccountsManager();
		deviceAccountsManager.setControllerFactory(controllerFactory);
		rulesManager = controllerFactory.createRulesManager();
		rulesManager.setControllerFactory(controllerFactory);
		rulesManager.setDaManager(deviceAccountsManager);
		
		controller = controllerFactory.createG37C();
		controller.setRulesManager(rulesManager);
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(User user) {
		JWTResponse jwtResponse = new JWTResponse();
        UserDAO userDao = new UserDAO();
        
        // Look for an existing user in the database.
        User existingUserInDB = userDao.findUserByName(user.getUserName());
        if (existingUserInDB == null) {
            jwtResponse.setErrorMessage("User does not exist!");
            return Response.ok().entity(jwtResponse).build();
        }
        
        // If the user was found, generate a JWT token.
        if (existingUserInDB.getPassword().equals(EncryptionUtil.encryptPassword(user.getPassword()))) {
        	jwtResponse.setToken(EncryptionUtil.generateToken(user));
        	jwtResponse.setUserName(existingUserInDB.getUserName());
        } else {
        	jwtResponse.setErrorMessage("Wrong password!");
        }
        		
		return Response.status(200).entity(jwtResponse).build();
	}
	
	@POST
	@Path("/addAdmin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void addAdmin(User user) {
		userAccountsManager.addUser(user.getUserName(), user.getPassword(), user.getUserDescription(), user.getUserType());
	}
	
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addUser(User user) {
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(user.getCallingUser());
		if (callingUser == null)
			return "Invalid input.";
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		String userCreateResult = userAccountsManager.addUser(user.getUserName(), user.getPassword(), user.getUserDescription(), user.getUserType());
		rulesManager.executeAll();
		
		return userCreateResult;
	}
	
	@POST
	@Path("/unregister")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String removeUser(User user) {
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(user.getCallingUser());
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		String userRemoveResult = userAccountsManager.rmUser(user.getUserName());
		rulesManager.executeAll();
		
		return userRemoveResult;
	}
	
	@GET
	@Path("/list/{userName}/{token}")
	public String listUsers(@PathParam("userName") String userName, @PathParam("token") String token) {
		User existingUserInDB = EncryptionUtil.validateToken(token);
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(userName);
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		String listUsersResult = userAccountsManager.lsUsers();
		rulesManager.executeAll();
		
		return listUsersResult;
	}	
}
