package G37.G37_RestAPI.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@Entity
@XmlRootElement
public class Rule {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@XmlElement
	private String ruleName;
    
    @XmlElement
    private String eventConditionAction;
    
    @XmlElement
    private String deviceUrl;
    
    @XmlElement
    private String userName;
    
    @XmlElement
    private String token;
}
