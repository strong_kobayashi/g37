package G37.G37_RestAPI.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@Entity
@XmlRootElement
public class User {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(unique = true)
	@XmlElement
	private String userName;

    @XmlElement
    private String password;
    
    @XmlElement
    private String userDescription;
    
    @XmlElement
    private String userType;
    
    @XmlElement
    private String callingUser;
}
