package G37.G37_RestAPI.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement
public class JWTResponse {
	
    @XmlElement
    private String token;
    
	@XmlElement
    private String errorMessage;
	
	@XmlElement
	private String userName;
}
