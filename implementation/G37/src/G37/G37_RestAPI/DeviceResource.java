package G37.G37_RestAPI;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccount;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.G37C;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.RoomsManager;
import G37.G37_Controller.RulesManager;
import G37.G37_Controller.SubscriptionsManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;
import G37.G37_RestAPI.models.Device;
import G37.G37_RestAPI.models.User;
import G37.G37_RestAPI.utils.EncryptionUtil;

@Path("/device")
public class DeviceResource {
	private G37C controller;
	private G37_ControllerFactory controllerFactory;
	private DeviceAccountsManager deviceAccountsManager;
	private UserAccountsManager userAccountsManager;
	private RoomsManager roomsManager;
	private SubscriptionsManager subscriptionsManager;
	private RulesManager rulesManager;
	
	public DeviceResource()	{
		controllerFactory = G37_ControllerFactory.eINSTANCE;
		deviceAccountsManager = controllerFactory.createDeviceAccountsManager();
		deviceAccountsManager.setControllerFactory(controllerFactory);
		deviceAccountsManager = controllerFactory.createDeviceAccountsManager();
		deviceAccountsManager.setControllerFactory(controllerFactory);
		rulesManager = controllerFactory.createRulesManager();
		rulesManager.setControllerFactory(controllerFactory);
		rulesManager.setDaManager(deviceAccountsManager);
		subscriptionsManager = controllerFactory.createSubscriptionsManager();
		subscriptionsManager.setControllerFactory(controllerFactory);
		deviceAccountsManager.setSubscrManager(subscriptionsManager);
		
		controller = controllerFactory.createG37C();
		controller.setRulesManager(rulesManager);
		controller.setSubscrManager(subscriptionsManager);
	}
	
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registerDevice(Device device) {
		User existingUserInDB = EncryptionUtil.validateToken(device.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(device.getUserName());
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		roomsManager = controllerFactory.createRoomsManager();
		roomsManager.setControllerFactory(controllerFactory);
		deviceAccountsManager.setRoomsManager(roomsManager);
		
		String regDeviceResult = deviceAccountsManager.reg(device.getDeviceUrl(), device.getRoomId());
		rulesManager.executeAll();
		
		return regDeviceResult;
	}
	
	@POST
	@Path("/unregister")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String unregisterDevice(Device device) {
		User existingUserInDB = EncryptionUtil.validateToken(device.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(device.getUserName());
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		String unregDeviceResult = deviceAccountsManager.unreg(device.getDeviceUrl());
		rulesManager.executeAll();
		
		return unregDeviceResult;
	}
	
	@PUT
	@Path("/move")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String moveDevice(Device device) {
		User existingUserInDB = EncryptionUtil.validateToken(device.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(device.getUserName());
		if (callingUser == null)
			return "Invalid input.";
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		roomsManager = controllerFactory.createRoomsManager();
		roomsManager.setControllerFactory(controllerFactory);
		deviceAccountsManager.setRoomsManager(roomsManager);
		
		String moveDeviceResult = deviceAccountsManager.mv(device.getDeviceUrl(), device.getRoomId());
		rulesManager.executeAll();
		
		return moveDeviceResult;
	}
	
	@GET
	@Path("/list/{roomId}/{userName}/{token}")
	public String listDevices(@PathParam("roomId") String roomId, @PathParam("userName") String userName, @PathParam("token") String token) {
		User existingUserInDB = EncryptionUtil.validateToken(token);
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		if (roomId.equals("none"))
			roomId = "";
		
		roomsManager = controllerFactory.createRoomsManager();
		roomsManager.setControllerFactory(controllerFactory);
		deviceAccountsManager.setRoomsManager(roomsManager);
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(userName);

		String listDevResult = deviceAccountsManager.lsDev(roomId, callingUser);
		rulesManager.executeAll();
		
		return listDevResult;
	}

	@GET
	@Path("/peek/{deviceUrl}/{propertyName}/{userName}/{token}")
	public String peek(@PathParam("deviceUrl") String deviceUrl, @PathParam("propertyName") String propertyName, @PathParam("userName") String userName, @PathParam("token") String token) {
		User existingUserInDB = EncryptionUtil.validateToken(token);
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(userName);
		DeviceAccount deviceAccount = deviceAccountsManager.getDeviceAccountByURL(deviceUrl);
		
		String peekDeviceResult = deviceAccountsManager.peek(deviceAccount, propertyName, callingUser);
		rulesManager.executeAll();
		
		return peekDeviceResult;
	}

    @PUT
    @Path("/poke")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public String poke(Device device) {
    	User existingUserInDB = EncryptionUtil.validateToken(device.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(device.getUserName());
		DeviceAccount deviceAccount = deviceAccountsManager.getDeviceAccountByURL(device.getDeviceUrl());
    	
		String pokeDeviceResult = deviceAccountsManager.poke(deviceAccount, device.getPropertyName(), device.getPropertyValue(), callingUser);
		rulesManager.executeAll();
		
		return pokeDeviceResult;
    }
}
