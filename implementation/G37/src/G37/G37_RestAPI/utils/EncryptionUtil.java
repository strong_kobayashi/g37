package G37.G37_RestAPI.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import G37.G37_RestAPI.dao.UserDAO;
import G37.G37_RestAPI.models.User;

public class EncryptionUtil {

	// This method generates a JWT token that is sent back to the user after
	// logging in.
	public static String generateToken(User user) {
        try {
        	Algorithm algorithm = Algorithm.HMAC256("secret");
            Date expirationDate = Date.from(Instant.now().plus(24, ChronoUnit.HOURS));
            Date issuedAt = Date.from(Instant.now());
            return JWT.create()
                    .withIssuedAt(issuedAt)
                    .withExpiresAt(expirationDate)
                    .withIssuer("auth0")
                    .withClaim("userName", user.getUserName())
                    .sign(algorithm);
        } catch (JWTCreationException e) {
        	e.printStackTrace();
        }
        
        return null;
	}
	
	// This method validates a JWT token: it looks for the user for whom the token was
	// generated in the database and returns the corresponding User object if successful.
	public static User validateToken(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256("secret");
			JWTVerifier verifier = JWT.require(algorithm).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            Claim userNameClaim = jwt.getClaim("userName");
            
            UserDAO userDao = new UserDAO();
            return userDao.findUserByName(userNameClaim.asString());
		} catch (JWTVerificationException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	// This method takes in an unencrypted password (that is entered by the client)
	// and encrypts it.
	public static String encryptPassword(String unencryptedPassword) {
		String encryptedPassword = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
			byte[] bytes = messageDigest.digest(unencryptedPassword.getBytes("UTF-8"));
			StringBuilder stringBuilder = new StringBuilder();
			for (byte b : bytes) {
				stringBuilder.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
			}
			encryptedPassword = stringBuilder.toString();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return encryptedPassword;
	}

}
