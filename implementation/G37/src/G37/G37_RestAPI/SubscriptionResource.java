package G37.G37_RestAPI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import G37.Auxiliary.Const;
import G37.Auxiliary.R_CODE;
import G37.Auxiliary.Resp;
import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.G37C;
import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.RoomsManager;
import G37.G37_Controller.RulesManager;
import G37.G37_Controller.SubscriptionsManager;
import G37.G37_Controller.UserAccount;
import G37.G37_Controller.UserAccountsManager;
import G37.G37_RestAPI.models.Subscription;
import G37.G37_RestAPI.models.User;
import G37.G37_RestAPI.utils.EncryptionUtil;

@Path("/subscr")
public class SubscriptionResource {
	private G37C controller;
	private G37_ControllerFactory controllerFactory;
	private SubscriptionsManager subscriptionManager;
	private UserAccountsManager userAccountsManager;
	private DeviceAccountsManager deviceAccountsManager;
	private RoomsManager roomsManager;
	private RulesManager rulesManager;

	public SubscriptionResource() {
		controllerFactory = G37_ControllerFactory.eINSTANCE;
		subscriptionManager = controllerFactory.createSubscriptionsManager();
		subscriptionManager.setControllerFactory(controllerFactory);
		deviceAccountsManager = controllerFactory.createDeviceAccountsManager();
		deviceAccountsManager.setControllerFactory(controllerFactory);
		rulesManager = controllerFactory.createRulesManager();
		rulesManager.setControllerFactory(controllerFactory);
		rulesManager.setDaManager(deviceAccountsManager);
		
		controller = controllerFactory.createG37C();
		controller.setRulesManager(rulesManager);
	}
	
	@POST
	@Path("/addsubscr")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addSubscription(Subscription subscr) {
		User existingUserInDB = EncryptionUtil.validateToken(subscr.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(subscr.getCallingUser());
		if (callingUser == null)
			return "Invalid input.";
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		roomsManager = controllerFactory.createRoomsManager();
		roomsManager.setControllerFactory(controllerFactory);
		subscriptionManager.setRoomsManager(roomsManager);
		subscriptionManager.setUserAccountsManager(userAccountsManager);
		
		String subscrAddResult = subscriptionManager.addSubscr(subscr.getSubscriptionId(), subscr.getDevClass(), subscr.getListOfRooms(), subscr.getUserName());
		rulesManager.executeAll();
		
		return subscrAddResult.toString();
	}
	
	@POST
	@Path("/rmsubscr")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String removeSubscription(Subscription subscr) {
		User existingUserInDB = EncryptionUtil.validateToken(subscr.getToken());
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(subscr.getCallingUser());
		if (!callingUser.getUserType().equals(Const.EU))
			return Resp.response(R_CODE.E_USERMAYNOTDOCOMMAND);
		
		String subscrRemoveResult = subscriptionManager.rmSubscr(subscr.getSubscriptionId());
		rulesManager.executeAll();
		
		return subscrRemoveResult;
	}
	
	@GET
	@Path("/list/{userName}/{token}")
	public String listSubscriptions(@PathParam("userName") String userName, @PathParam("token") String token) {
		User existingUserInDB = EncryptionUtil.validateToken(token);
		if (existingUserInDB == null)
			return "Invalid JWT token.";
		
		userAccountsManager = controllerFactory.createUserAccountsManager();
		userAccountsManager.setControllerFactory(controllerFactory);
		UserAccount callingUser = userAccountsManager.getUserAccountByUserName(userName);

		String listSubscrResult = subscriptionManager.lsSubscr(callingUser);
		rulesManager.executeAll();
		
		return listSubscrResult;
	}
}
