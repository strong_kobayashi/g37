package G37.Auxiliary;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class StartupContextListener implements ServletContextListener {
	
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		InputStream inputStream = null;
		
		try {
			URL resource = new URL(this.getClass().getResource("."), "Responses.json");
			inputStream = resource.openConnection().getInputStream();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Load the Responses.json file
		Resp.responses = Resp.loadResponsesFromJSON(inputStream);
	}
}
