package G37.Auxiliary;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Resp {
	
	public static List<String> responses;
	protected static boolean debug;
	
	public static void setDebug() {
		debug = true;
	}
	
	// Open a JSON *array* file with the path and name provided by the argument, and convert it
	// into a list of strings.
	public static List<String> loadResponsesFromJSON(InputStream inputStream) {
		List<String> resp = new ArrayList<String>();
		JsonParser parser = new JsonParser();
		try (Reader reader = new InputStreamReader(inputStream)) {
	        Iterator<JsonElement> iterator = parser.parse(reader).getAsJsonArray().iterator();
	        while (iterator.hasNext()) {
	        	String element = ((JsonElement)iterator.next()).toString();
	        	resp.add(element.substring(1, element.length()-1)); // Add while trimming away the quotation marks.
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		return resp;
	}
		
	// Return a response string from the static array field named responses, which corresponds to the code provided by the 
	// first argument.  "%1" token in the response will be replaced by the second argument.
	// Example: response (E_ROOMNOTFOUND, "Bedroom") will return "Error: room \"Bedroom\" not found." (or whatever is defined
	// in the responses array).
	public static String response(R_CODE code, String arg) {
		String r = "";
		Iterator<String> iterator = responses.iterator();
		while (iterator.hasNext()) {
	    	if (R_CODE.valueOf(iterator.next()) == code) {  // If we found the code match...
	    		r = iterator.next(); // ...then the next string is what we need.
	    		break;
	    	}
	    	iterator.next();  // Skip one string because code and messages go after one another.

	    }
		if (r == "") r = "Error: unknown error, code " + code;
		r = r.replace("%1", "\""+arg+"\"");
		return r;
	}
	
	// This method does the opposite of "response": takes in the response string and returns the error code.
	public static R_CODE lookUpCode(String r) {
		R_CODE r_code = R_CODE.E_OTHER;
		String current, previous = Resp.response(R_CODE.OK); 
		// Replace "blahblahblah" with "%1";
		String r1 = Pattern.compile("\".*\"").matcher(r).replaceFirst("%1");
		Iterator<String> iterator = responses.iterator();
		while (iterator.hasNext()) {
			current = iterator.next();
			if (r1.equals(current)) {
				r_code = R_CODE.valueOf(previous);
				break;
			}
			else previous = current;
		}
		return r_code;
	}

	// Same as above for the case with no second argument.
	public static String response(R_CODE code) {
		return response(code, "");
	}
	
	// An auxiliary method for trimming spaces and quotation marks off the beginning and the end of a string.
	public static String trimString(String str, String seqToTrim) {
		if (str == null) return null;
		String r = str.trim();
		r = r.startsWith(seqToTrim) ? r.substring(seqToTrim.length(), r.length()) : r;  // Remove leading seq
		r = r.endsWith(seqToTrim) ? r.substring(0, r.length()-seqToTrim.length()) : r;  // Remove trailing seq
		return r;
	}
	
	public static void delay(int sec) {
		try {  
    		TimeUnit.SECONDS.sleep(sec);
    	} catch (InterruptedException e) {
    		e.printStackTrace();
    	}
	}
	
	public static void log (String msg, boolean onlyInDebugMode) {
		if (onlyInDebugMode && !debug) return;
		System.out.println(msg);
	}

	public static void log (String msg) {
		log (msg, false);
	}
	
}
