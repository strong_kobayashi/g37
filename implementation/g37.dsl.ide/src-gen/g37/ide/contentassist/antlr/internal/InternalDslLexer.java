package g37.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDslLexer extends Lexer {
    public static final int RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES=4;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__10=10;
    public static final int T__9=9;
    public static final int RULE_WS=6;
    public static final int RULE_THE_REST=7;
    public static final int RULE_SNGLQUOTED_NON_EMPTY_SEQ=5;
    public static final int T__22=22;
    public static final int RULE_FLOAT=8;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalDslLexer() {;} 
    public InternalDslLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalDslLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalDsl.g"; }

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:11:6: ( 'NOT' )
            // InternalDsl.g:11:8: 'NOT'
            {
            match("NOT"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "T__10"
    public final void mT__10() throws RecognitionException {
        try {
            int _type = T__10;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:12:7: ( 'POKE' )
            // InternalDsl.g:12:9: 'POKE'
            {
            match("POKE"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__10"

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:13:7: ( 'EMAIL' )
            // InternalDsl.g:13:9: 'EMAIL'
            {
            match("EMAIL"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:14:7: ( 'AND' )
            // InternalDsl.g:14:9: 'AND'
            {
            match("AND"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:15:7: ( 'OR' )
            // InternalDsl.g:15:9: 'OR'
            {
            match("OR"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:16:7: ( '==' )
            // InternalDsl.g:16:9: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:17:7: ( 'contains' )
            // InternalDsl.g:17:9: 'contains'
            {
            match("contains"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:18:7: ( '>' )
            // InternalDsl.g:18:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:19:7: ( '<' )
            // InternalDsl.g:19:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:20:7: ( '>=' )
            // InternalDsl.g:20:9: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:21:7: ( '<=' )
            // InternalDsl.g:21:9: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:22:7: ( 'IF' )
            // InternalDsl.g:22:9: 'IF'
            {
            match("IF"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:23:7: ( '(' )
            // InternalDsl.g:23:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:24:7: ( ')' )
            // InternalDsl.g:24:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "RULE_FLOAT"
    public final void mRULE_FLOAT() throws RecognitionException {
        try {
            int _type = RULE_FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:1435:12: ( ( ( '0' .. '9' )+ | ( '0' .. '9' )+ '.' ( '0' .. '9' )+ ) )
            // InternalDsl.g:1435:14: ( ( '0' .. '9' )+ | ( '0' .. '9' )+ '.' ( '0' .. '9' )+ )
            {
            // InternalDsl.g:1435:14: ( ( '0' .. '9' )+ | ( '0' .. '9' )+ '.' ( '0' .. '9' )+ )
            int alt4=2;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // InternalDsl.g:1435:15: ( '0' .. '9' )+
                    {
                    // InternalDsl.g:1435:15: ( '0' .. '9' )+
                    int cnt1=0;
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( ((LA1_0>='0' && LA1_0<='9')) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalDsl.g:1435:16: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt1 >= 1 ) break loop1;
                                EarlyExitException eee =
                                    new EarlyExitException(1, input);
                                throw eee;
                        }
                        cnt1++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // InternalDsl.g:1435:27: ( '0' .. '9' )+ '.' ( '0' .. '9' )+
                    {
                    // InternalDsl.g:1435:27: ( '0' .. '9' )+
                    int cnt2=0;
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( ((LA2_0>='0' && LA2_0<='9')) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalDsl.g:1435:28: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt2 >= 1 ) break loop2;
                                EarlyExitException eee =
                                    new EarlyExitException(2, input);
                                throw eee;
                        }
                        cnt2++;
                    } while (true);

                    match('.'); 
                    // InternalDsl.g:1435:43: ( '0' .. '9' )+
                    int cnt3=0;
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalDsl.g:1435:44: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt3 >= 1 ) break loop3;
                                EarlyExitException eee =
                                    new EarlyExitException(3, input);
                                throw eee;
                        }
                        cnt3++;
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FLOAT"

    // $ANTLR start "RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES"
    public final void mRULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES() throws RecognitionException {
        try {
            int _type = RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:1437:41: ( (~ ( ( ' ' | '\\t' | '\\r' | '\\n' | '\\'' | '(' | ')' | '\"' ) ) )+ )
            // InternalDsl.g:1437:43: (~ ( ( ' ' | '\\t' | '\\r' | '\\n' | '\\'' | '(' | ')' | '\"' ) ) )+
            {
            // InternalDsl.g:1437:43: (~ ( ( ' ' | '\\t' | '\\r' | '\\n' | '\\'' | '(' | ')' | '\"' ) ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='\u0000' && LA5_0<='\b')||(LA5_0>='\u000B' && LA5_0<='\f')||(LA5_0>='\u000E' && LA5_0<='\u001F')||LA5_0=='!'||(LA5_0>='#' && LA5_0<='&')||(LA5_0>='*' && LA5_0<='\uFFFF')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalDsl.g:1437:43: ~ ( ( ' ' | '\\t' | '\\r' | '\\n' | '\\'' | '(' | ')' | '\"' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\b')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\u001F')||input.LA(1)=='!'||(input.LA(1)>='#' && input.LA(1)<='&')||(input.LA(1)>='*' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES"

    // $ANTLR start "RULE_SNGLQUOTED_NON_EMPTY_SEQ"
    public final void mRULE_SNGLQUOTED_NON_EMPTY_SEQ() throws RecognitionException {
        try {
            int _type = RULE_SNGLQUOTED_NON_EMPTY_SEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:1439:31: ( '\\'' (~ ( ( '\\'' | '\\r' | '\\n' | '(' | ')' | '\"' ) ) )+ '\\'' )
            // InternalDsl.g:1439:33: '\\'' (~ ( ( '\\'' | '\\r' | '\\n' | '(' | ')' | '\"' ) ) )+ '\\''
            {
            match('\''); 
            // InternalDsl.g:1439:38: (~ ( ( '\\'' | '\\r' | '\\n' | '(' | ')' | '\"' ) ) )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='\u0000' && LA6_0<='\t')||(LA6_0>='\u000B' && LA6_0<='\f')||(LA6_0>='\u000E' && LA6_0<='!')||(LA6_0>='#' && LA6_0<='&')||(LA6_0>='*' && LA6_0<='\uFFFF')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalDsl.g:1439:38: ~ ( ( '\\'' | '\\r' | '\\n' | '(' | ')' | '\"' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='&')||(input.LA(1)>='*' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);

            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SNGLQUOTED_NON_EMPTY_SEQ"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:1441:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalDsl.g:1441:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalDsl.g:1441:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='\t' && LA7_0<='\n')||LA7_0=='\r'||LA7_0==' ') ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalDsl.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_THE_REST"
    public final void mRULE_THE_REST() throws RecognitionException {
        try {
            int _type = RULE_THE_REST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDsl.g:1443:15: ( '[' (~ ( ( '[' | ']' ) ) )* ']' ( ' ' | '\\t' )* ( '\\r' )? ( '\\n' )? EOF )
            // InternalDsl.g:1443:17: '[' (~ ( ( '[' | ']' ) ) )* ']' ( ' ' | '\\t' )* ( '\\r' )? ( '\\n' )? EOF
            {
            match('['); 
            // InternalDsl.g:1443:21: (~ ( ( '[' | ']' ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='\u0000' && LA8_0<='Z')||LA8_0=='\\'||(LA8_0>='^' && LA8_0<='\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalDsl.g:1443:21: ~ ( ( '[' | ']' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='Z')||input.LA(1)=='\\'||(input.LA(1)>='^' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            match(']'); 
            // InternalDsl.g:1443:39: ( ' ' | '\\t' )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='\t'||LA9_0==' ') ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalDsl.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            // InternalDsl.g:1443:51: ( '\\r' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\r') ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDsl.g:1443:51: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            // InternalDsl.g:1443:57: ( '\\n' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='\n') ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDsl.g:1443:57: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }

            match(EOF); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_THE_REST"

    public void mTokens() throws RecognitionException {
        // InternalDsl.g:1:8: ( T__9 | T__10 | T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | RULE_FLOAT | RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES | RULE_SNGLQUOTED_NON_EMPTY_SEQ | RULE_WS | RULE_THE_REST )
        int alt12=19;
        alt12 = dfa12.predict(input);
        switch (alt12) {
            case 1 :
                // InternalDsl.g:1:10: T__9
                {
                mT__9(); 

                }
                break;
            case 2 :
                // InternalDsl.g:1:15: T__10
                {
                mT__10(); 

                }
                break;
            case 3 :
                // InternalDsl.g:1:21: T__11
                {
                mT__11(); 

                }
                break;
            case 4 :
                // InternalDsl.g:1:27: T__12
                {
                mT__12(); 

                }
                break;
            case 5 :
                // InternalDsl.g:1:33: T__13
                {
                mT__13(); 

                }
                break;
            case 6 :
                // InternalDsl.g:1:39: T__14
                {
                mT__14(); 

                }
                break;
            case 7 :
                // InternalDsl.g:1:45: T__15
                {
                mT__15(); 

                }
                break;
            case 8 :
                // InternalDsl.g:1:51: T__16
                {
                mT__16(); 

                }
                break;
            case 9 :
                // InternalDsl.g:1:57: T__17
                {
                mT__17(); 

                }
                break;
            case 10 :
                // InternalDsl.g:1:63: T__18
                {
                mT__18(); 

                }
                break;
            case 11 :
                // InternalDsl.g:1:69: T__19
                {
                mT__19(); 

                }
                break;
            case 12 :
                // InternalDsl.g:1:75: T__20
                {
                mT__20(); 

                }
                break;
            case 13 :
                // InternalDsl.g:1:81: T__21
                {
                mT__21(); 

                }
                break;
            case 14 :
                // InternalDsl.g:1:87: T__22
                {
                mT__22(); 

                }
                break;
            case 15 :
                // InternalDsl.g:1:93: RULE_FLOAT
                {
                mRULE_FLOAT(); 

                }
                break;
            case 16 :
                // InternalDsl.g:1:104: RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES
                {
                mRULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES(); 

                }
                break;
            case 17 :
                // InternalDsl.g:1:144: RULE_SNGLQUOTED_NON_EMPTY_SEQ
                {
                mRULE_SNGLQUOTED_NON_EMPTY_SEQ(); 

                }
                break;
            case 18 :
                // InternalDsl.g:1:174: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 19 :
                // InternalDsl.g:1:182: RULE_THE_REST
                {
                mRULE_THE_REST(); 

                }
                break;

        }

    }


    protected DFA4 dfa4 = new DFA4(this);
    protected DFA12 dfa12 = new DFA12(this);
    static final String DFA4_eotS =
        "\1\uffff\1\3\2\uffff";
    static final String DFA4_eofS =
        "\4\uffff";
    static final String DFA4_minS =
        "\1\60\1\56\2\uffff";
    static final String DFA4_maxS =
        "\2\71\2\uffff";
    static final String DFA4_acceptS =
        "\2\uffff\1\2\1\1";
    static final String DFA4_specialS =
        "\4\uffff}>";
    static final String[] DFA4_transitionS = {
            "\12\1",
            "\1\2\1\uffff\12\1",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        public String getDescription() {
            return "1435:14: ( ( '0' .. '9' )+ | ( '0' .. '9' )+ '.' ( '0' .. '9' )+ )";
        }
    }
    static final String DFA12_eotS =
        "\1\uffff\7\21\1\32\1\34\1\21\2\uffff\1\36\1\21\3\uffff\4\21\1\47\1\50\1\21\1\52\1\uffff\1\53\1\uffff\1\54\1\uffff\3\21\1\uffff\1\56\2\21\1\61\2\uffff\1\21\3\uffff\1\36\1\uffff\1\63\1\21\1\uffff\1\21\1\uffff\1\66\1\21\1\uffff\2\21\1\72\1\uffff";
    static final String DFA12_eofS =
        "\73\uffff";
    static final String DFA12_minS =
        "\1\0\2\117\1\115\1\116\1\122\1\75\1\157\2\0\1\106\2\uffff\2\0\3\uffff\1\124\1\113\1\101\1\104\2\0\1\156\1\0\1\uffff\1\0\1\uffff\1\0\1\uffff\1\60\1\0\1\11\1\uffff\1\0\1\105\1\111\1\0\2\uffff\1\164\3\uffff\1\0\1\uffff\1\0\1\114\1\uffff\1\141\1\uffff\1\0\1\151\1\uffff\1\156\1\163\1\0\1\uffff";
    static final String DFA12_maxS =
        "\1\uffff\2\117\1\115\1\116\1\122\1\75\1\157\2\uffff\1\106\2\uffff\2\uffff\3\uffff\1\124\1\113\1\101\1\104\2\uffff\1\156\1\uffff\1\uffff\1\uffff\1\uffff\1\uffff\1\uffff\1\71\1\uffff\1\40\1\uffff\1\uffff\1\105\1\111\1\uffff\2\uffff\1\164\3\uffff\1\uffff\1\uffff\1\uffff\1\114\1\uffff\1\141\1\uffff\1\uffff\1\151\1\uffff\1\156\1\163\1\uffff\1\uffff";
    static final String DFA12_acceptS =
        "\13\uffff\1\15\1\16\2\uffff\1\21\1\22\1\20\10\uffff\1\10\1\uffff\1\11\1\uffff\1\17\3\uffff\1\23\4\uffff\1\5\1\6\1\uffff\1\12\1\13\1\14\1\uffff\1\1\2\uffff\1\4\1\uffff\1\2\2\uffff\1\3\3\uffff\1\7";
    static final String DFA12_specialS =
        "\1\16\7\uffff\1\12\1\14\3\uffff\1\7\1\13\7\uffff\1\10\1\11\1\uffff\1\0\1\uffff\1\2\1\uffff\1\4\2\uffff\1\20\2\uffff\1\1\2\uffff\1\6\6\uffff\1\17\1\uffff\1\3\4\uffff\1\5\4\uffff\1\15\1\uffff}>";
    static final String[] DFA12_transitionS = {
            "\11\21\2\20\2\21\1\20\22\21\1\20\1\21\1\uffff\4\21\1\17\1\13\1\14\6\21\12\15\2\21\1\11\1\6\1\10\2\21\1\4\3\21\1\3\3\21\1\12\4\21\1\1\1\5\1\2\12\21\1\16\7\21\1\7\uff9c\21",
            "\1\22",
            "\1\23",
            "\1\24",
            "\1\25",
            "\1\26",
            "\1\27",
            "\1\30",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\23\21\1\31\uffc2\21",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\23\21\1\33\uffc2\21",
            "\1\35",
            "",
            "",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\4\21\1\37\1\21\12\15\uffc6\21",
            "\11\40\2\42\2\40\1\42\22\40\1\42\1\40\1\42\4\40\3\42\61\40\1\uffff\1\40\1\41\uffa2\40",
            "",
            "",
            "",
            "\1\43",
            "\1\44",
            "\1\45",
            "\1\46",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "\1\51",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "",
            "\12\55",
            "\11\40\2\42\2\40\1\42\22\40\1\42\1\40\1\42\4\40\3\42\61\40\1\uffff\1\40\1\41\uffa2\40",
            "\2\42\2\uffff\1\42\22\uffff\1\42",
            "",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "\1\57",
            "\1\60",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "",
            "",
            "\1\62",
            "",
            "",
            "",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\6\21\12\55\uffc6\21",
            "",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "\1\64",
            "",
            "\1\65",
            "",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            "\1\67",
            "",
            "\1\70",
            "\1\71",
            "\11\21\2\uffff\2\21\1\uffff\22\21\1\uffff\1\21\1\uffff\4\21\3\uffff\uffd6\21",
            ""
    };

    static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
    static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
    static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
    static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
    static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
    static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
    static final short[][] DFA12_transition;

    static {
        int numStates = DFA12_transitionS.length;
        DFA12_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
        }
    }

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = DFA12_eot;
            this.eof = DFA12_eof;
            this.min = DFA12_min;
            this.max = DFA12_max;
            this.accept = DFA12_accept;
            this.special = DFA12_special;
            this.transition = DFA12_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__9 | T__10 | T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | RULE_FLOAT | RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES | RULE_SNGLQUOTED_NON_EMPTY_SEQ | RULE_WS | RULE_THE_REST );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA12_25 = input.LA(1);

                        s = -1;
                        if ( ((LA12_25>='\u0000' && LA12_25<='\b')||(LA12_25>='\u000B' && LA12_25<='\f')||(LA12_25>='\u000E' && LA12_25<='\u001F')||LA12_25=='!'||(LA12_25>='#' && LA12_25<='&')||(LA12_25>='*' && LA12_25<='\uFFFF')) ) {s = 17;}

                        else s = 42;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA12_35 = input.LA(1);

                        s = -1;
                        if ( ((LA12_35>='\u0000' && LA12_35<='\b')||(LA12_35>='\u000B' && LA12_35<='\f')||(LA12_35>='\u000E' && LA12_35<='\u001F')||LA12_35=='!'||(LA12_35>='#' && LA12_35<='&')||(LA12_35>='*' && LA12_35<='\uFFFF')) ) {s = 17;}

                        else s = 46;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA12_27 = input.LA(1);

                        s = -1;
                        if ( ((LA12_27>='\u0000' && LA12_27<='\b')||(LA12_27>='\u000B' && LA12_27<='\f')||(LA12_27>='\u000E' && LA12_27<='\u001F')||LA12_27=='!'||(LA12_27>='#' && LA12_27<='&')||(LA12_27>='*' && LA12_27<='\uFFFF')) ) {s = 17;}

                        else s = 43;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA12_47 = input.LA(1);

                        s = -1;
                        if ( ((LA12_47>='\u0000' && LA12_47<='\b')||(LA12_47>='\u000B' && LA12_47<='\f')||(LA12_47>='\u000E' && LA12_47<='\u001F')||LA12_47=='!'||(LA12_47>='#' && LA12_47<='&')||(LA12_47>='*' && LA12_47<='\uFFFF')) ) {s = 17;}

                        else s = 51;

                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA12_29 = input.LA(1);

                        s = -1;
                        if ( ((LA12_29>='\u0000' && LA12_29<='\b')||(LA12_29>='\u000B' && LA12_29<='\f')||(LA12_29>='\u000E' && LA12_29<='\u001F')||LA12_29=='!'||(LA12_29>='#' && LA12_29<='&')||(LA12_29>='*' && LA12_29<='\uFFFF')) ) {s = 17;}

                        else s = 44;

                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA12_52 = input.LA(1);

                        s = -1;
                        if ( ((LA12_52>='\u0000' && LA12_52<='\b')||(LA12_52>='\u000B' && LA12_52<='\f')||(LA12_52>='\u000E' && LA12_52<='\u001F')||LA12_52=='!'||(LA12_52>='#' && LA12_52<='&')||(LA12_52>='*' && LA12_52<='\uFFFF')) ) {s = 17;}

                        else s = 54;

                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA12_38 = input.LA(1);

                        s = -1;
                        if ( ((LA12_38>='\u0000' && LA12_38<='\b')||(LA12_38>='\u000B' && LA12_38<='\f')||(LA12_38>='\u000E' && LA12_38<='\u001F')||LA12_38=='!'||(LA12_38>='#' && LA12_38<='&')||(LA12_38>='*' && LA12_38<='\uFFFF')) ) {s = 17;}

                        else s = 49;

                        if ( s>=0 ) return s;
                        break;
                    case 7 : 
                        int LA12_13 = input.LA(1);

                        s = -1;
                        if ( ((LA12_13>='0' && LA12_13<='9')) ) {s = 13;}

                        else if ( (LA12_13=='.') ) {s = 31;}

                        else if ( ((LA12_13>='\u0000' && LA12_13<='\b')||(LA12_13>='\u000B' && LA12_13<='\f')||(LA12_13>='\u000E' && LA12_13<='\u001F')||LA12_13=='!'||(LA12_13>='#' && LA12_13<='&')||(LA12_13>='*' && LA12_13<='-')||LA12_13=='/'||(LA12_13>=':' && LA12_13<='\uFFFF')) ) {s = 17;}

                        else s = 30;

                        if ( s>=0 ) return s;
                        break;
                    case 8 : 
                        int LA12_22 = input.LA(1);

                        s = -1;
                        if ( ((LA12_22>='\u0000' && LA12_22<='\b')||(LA12_22>='\u000B' && LA12_22<='\f')||(LA12_22>='\u000E' && LA12_22<='\u001F')||LA12_22=='!'||(LA12_22>='#' && LA12_22<='&')||(LA12_22>='*' && LA12_22<='\uFFFF')) ) {s = 17;}

                        else s = 39;

                        if ( s>=0 ) return s;
                        break;
                    case 9 : 
                        int LA12_23 = input.LA(1);

                        s = -1;
                        if ( ((LA12_23>='\u0000' && LA12_23<='\b')||(LA12_23>='\u000B' && LA12_23<='\f')||(LA12_23>='\u000E' && LA12_23<='\u001F')||LA12_23=='!'||(LA12_23>='#' && LA12_23<='&')||(LA12_23>='*' && LA12_23<='\uFFFF')) ) {s = 17;}

                        else s = 40;

                        if ( s>=0 ) return s;
                        break;
                    case 10 : 
                        int LA12_8 = input.LA(1);

                        s = -1;
                        if ( (LA12_8=='=') ) {s = 25;}

                        else if ( ((LA12_8>='\u0000' && LA12_8<='\b')||(LA12_8>='\u000B' && LA12_8<='\f')||(LA12_8>='\u000E' && LA12_8<='\u001F')||LA12_8=='!'||(LA12_8>='#' && LA12_8<='&')||(LA12_8>='*' && LA12_8<='<')||(LA12_8>='>' && LA12_8<='\uFFFF')) ) {s = 17;}

                        else s = 26;

                        if ( s>=0 ) return s;
                        break;
                    case 11 : 
                        int LA12_14 = input.LA(1);

                        s = -1;
                        if ( ((LA12_14>='\u0000' && LA12_14<='\b')||(LA12_14>='\u000B' && LA12_14<='\f')||(LA12_14>='\u000E' && LA12_14<='\u001F')||LA12_14=='!'||(LA12_14>='#' && LA12_14<='&')||(LA12_14>='*' && LA12_14<='Z')||LA12_14=='\\'||(LA12_14>='^' && LA12_14<='\uFFFF')) ) {s = 32;}

                        else if ( (LA12_14==']') ) {s = 33;}

                        else if ( ((LA12_14>='\t' && LA12_14<='\n')||LA12_14=='\r'||LA12_14==' '||LA12_14=='\"'||(LA12_14>='\'' && LA12_14<=')')) ) {s = 34;}

                        else s = 17;

                        if ( s>=0 ) return s;
                        break;
                    case 12 : 
                        int LA12_9 = input.LA(1);

                        s = -1;
                        if ( (LA12_9=='=') ) {s = 27;}

                        else if ( ((LA12_9>='\u0000' && LA12_9<='\b')||(LA12_9>='\u000B' && LA12_9<='\f')||(LA12_9>='\u000E' && LA12_9<='\u001F')||LA12_9=='!'||(LA12_9>='#' && LA12_9<='&')||(LA12_9>='*' && LA12_9<='<')||(LA12_9>='>' && LA12_9<='\uFFFF')) ) {s = 17;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
                    case 13 : 
                        int LA12_57 = input.LA(1);

                        s = -1;
                        if ( ((LA12_57>='\u0000' && LA12_57<='\b')||(LA12_57>='\u000B' && LA12_57<='\f')||(LA12_57>='\u000E' && LA12_57<='\u001F')||LA12_57=='!'||(LA12_57>='#' && LA12_57<='&')||(LA12_57>='*' && LA12_57<='\uFFFF')) ) {s = 17;}

                        else s = 58;

                        if ( s>=0 ) return s;
                        break;
                    case 14 : 
                        int LA12_0 = input.LA(1);

                        s = -1;
                        if ( (LA12_0=='N') ) {s = 1;}

                        else if ( (LA12_0=='P') ) {s = 2;}

                        else if ( (LA12_0=='E') ) {s = 3;}

                        else if ( (LA12_0=='A') ) {s = 4;}

                        else if ( (LA12_0=='O') ) {s = 5;}

                        else if ( (LA12_0=='=') ) {s = 6;}

                        else if ( (LA12_0=='c') ) {s = 7;}

                        else if ( (LA12_0=='>') ) {s = 8;}

                        else if ( (LA12_0=='<') ) {s = 9;}

                        else if ( (LA12_0=='I') ) {s = 10;}

                        else if ( (LA12_0=='(') ) {s = 11;}

                        else if ( (LA12_0==')') ) {s = 12;}

                        else if ( ((LA12_0>='0' && LA12_0<='9')) ) {s = 13;}

                        else if ( (LA12_0=='[') ) {s = 14;}

                        else if ( (LA12_0=='\'') ) {s = 15;}

                        else if ( ((LA12_0>='\t' && LA12_0<='\n')||LA12_0=='\r'||LA12_0==' ') ) {s = 16;}

                        else if ( ((LA12_0>='\u0000' && LA12_0<='\b')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='\u001F')||LA12_0=='!'||(LA12_0>='#' && LA12_0<='&')||(LA12_0>='*' && LA12_0<='/')||(LA12_0>=':' && LA12_0<=';')||(LA12_0>='?' && LA12_0<='@')||(LA12_0>='B' && LA12_0<='D')||(LA12_0>='F' && LA12_0<='H')||(LA12_0>='J' && LA12_0<='M')||(LA12_0>='Q' && LA12_0<='Z')||(LA12_0>='\\' && LA12_0<='b')||(LA12_0>='d' && LA12_0<='\uFFFF')) ) {s = 17;}

                        if ( s>=0 ) return s;
                        break;
                    case 15 : 
                        int LA12_45 = input.LA(1);

                        s = -1;
                        if ( ((LA12_45>='0' && LA12_45<='9')) ) {s = 45;}

                        else if ( ((LA12_45>='\u0000' && LA12_45<='\b')||(LA12_45>='\u000B' && LA12_45<='\f')||(LA12_45>='\u000E' && LA12_45<='\u001F')||LA12_45=='!'||(LA12_45>='#' && LA12_45<='&')||(LA12_45>='*' && LA12_45<='/')||(LA12_45>=':' && LA12_45<='\uFFFF')) ) {s = 17;}

                        else s = 30;

                        if ( s>=0 ) return s;
                        break;
                    case 16 : 
                        int LA12_32 = input.LA(1);

                        s = -1;
                        if ( (LA12_32==']') ) {s = 33;}

                        else if ( ((LA12_32>='\u0000' && LA12_32<='\b')||(LA12_32>='\u000B' && LA12_32<='\f')||(LA12_32>='\u000E' && LA12_32<='\u001F')||LA12_32=='!'||(LA12_32>='#' && LA12_32<='&')||(LA12_32>='*' && LA12_32<='Z')||LA12_32=='\\'||(LA12_32>='^' && LA12_32<='\uFFFF')) ) {s = 32;}

                        else if ( ((LA12_32>='\t' && LA12_32<='\n')||LA12_32=='\r'||LA12_32==' '||LA12_32=='\"'||(LA12_32>='\'' && LA12_32<=')')) ) {s = 34;}

                        else s = 17;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 12, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}