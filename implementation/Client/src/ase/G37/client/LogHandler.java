package ase.G37.client;

import org.apache.log4j.Logger;

public class LogHandler {
    private static LogHandler logHandler;
    private static final Logger log = Logger.getLogger(LogHandler.class.getName());

    public static LogHandler getLog() {
        if (logHandler == null)
            logHandler = new LogHandler();
        return logHandler;
    }

    public void logEntry(String output) {
        log.debug(output);
    }
}
