package ase.G37.client.serializers;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ase.G37.client.models.Rule;

public class RuleSerializer implements JsonSerializer<Rule> {

	@Override
	public JsonElement serialize(Rule src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject result = new JsonObject();
        result.add("ruleName", new JsonPrimitive(src.getRuleName()));
        result.add("eventConditionAction", new JsonPrimitive(src.getEventConditionAction()));
        result.add("userName", new JsonPrimitive(src.getUserName()));
        result.add("token", new JsonPrimitive(src.getToken()));
        return result;
	}
	
}
