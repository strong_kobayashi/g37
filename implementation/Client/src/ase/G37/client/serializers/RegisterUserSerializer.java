package ase.G37.client.serializers;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ase.G37.client.models.RegisterUser;

public class RegisterUserSerializer implements JsonSerializer<RegisterUser> {
    
	@Override
	public JsonElement serialize(final RegisterUser user, final Type type, final JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.add("userName", new JsonPrimitive(user.getUserName()));
        result.add("password", new JsonPrimitive(user.getPassword()));
        result.add("userDescription", new JsonPrimitive(user.getUserDescription()));
        result.add("userType", new JsonPrimitive(user.getUserType()));
        result.add("callingUser", new JsonPrimitive(user.getCallingUser()));
        return result;
    }
}
