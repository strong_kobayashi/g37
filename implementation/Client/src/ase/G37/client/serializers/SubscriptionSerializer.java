package ase.G37.client.serializers;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ase.G37.client.models.Subscription;

public class SubscriptionSerializer implements JsonSerializer<Subscription> {
	
	@Override
	public JsonElement serialize(Subscription src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject result = new JsonObject();
        result.add("subscriptionId", new JsonPrimitive(src.getSubscriptionId()));
        result.add("devClass", new JsonPrimitive(src.getDevClass()));
        result.add("listOfRooms", new JsonPrimitive(src.getListOfRooms()));
        result.add("userName", new JsonPrimitive(src.getUserName()));
        result.add("token", new JsonPrimitive(src.getToken()));
        result.add("callingUser",  new JsonPrimitive(src.getCallingUser()));
        return result;
	}
}
