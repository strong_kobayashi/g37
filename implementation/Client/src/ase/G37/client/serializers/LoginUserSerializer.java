package ase.G37.client.serializers;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ase.G37.client.models.LoginUser;

public class LoginUserSerializer implements JsonSerializer<LoginUser> {
    
	@Override
	public JsonElement serialize(final LoginUser user, final Type type, final JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.add("userName", new JsonPrimitive(user.getName()));
        result.add("password", new JsonPrimitive(user.getPassword()));
        return result;
    }
}
