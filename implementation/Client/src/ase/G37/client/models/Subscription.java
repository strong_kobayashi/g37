package ase.G37.client.models;

import lombok.Data;

@Data
public class Subscription {
	private String subscriptionId;
	private String devClass;
	private String listOfRooms;
	private String userName;
	private String token;
	private String callingUser;
	
	public Subscription(String subscriptionId, String devClass, String listOfRooms, String userName, String token, String callingUser) {
		this.subscriptionId = subscriptionId;
		this.devClass = devClass;
		this.listOfRooms = listOfRooms;
		this.userName = userName;
		this.token = token;
		this.callingUser = callingUser;
	}
}
