package ase.G37.client.models;

import lombok.Data;

@Data
public class ServerResponse {
	private String token;
	private String errorMessage;
	private String userName;
	
	public ServerResponse(String token, String errorMessage, String userName) {
		this.token = token;
		this.errorMessage = errorMessage;
		this.userName = userName;
	}
}
