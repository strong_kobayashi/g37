package ase.G37.client.models;

import lombok.Data;

@Data
public class RegisterUser {
	private String userName;
	private String password;
	private String userDescription;
	private String userType;
	private String callingUser;
	
	public RegisterUser(String userName, String password, String userDescription, String userType, String callingUser) {
		this.userName = userName;
		this.password = password;
		this.userDescription = userDescription;
		this.userType = userType;
		this.callingUser = callingUser;
	}
}
