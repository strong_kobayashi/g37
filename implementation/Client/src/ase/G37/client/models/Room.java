package ase.G37.client.models;

import lombok.Data;

@Data
public class Room {
	private String roomId;
	private String userName;
	private String token;
	
	public Room(String roomId, String userName, String token) {
		this.roomId = roomId;
		this.userName = userName;
		this.token = token;
	}
}
