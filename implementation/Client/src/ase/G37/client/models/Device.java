package ase.G37.client.models;

import lombok.Data;

@Data
public class Device {
	private String deviceUrl;
	private String roomId;
	private String token;
	private String userName; // Required for command "lsdev"
	private String propertyName;
	private String propertyValue;
	
	public Device(String deviceUrl, String roomId, String token, String userName, String propertyName, String propertyValue) {
		this.deviceUrl = deviceUrl;
		this.roomId = roomId;
		this.token = token;
		this.userName = userName;
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
	}
}
