package ase.G37.client.models;

import lombok.Data;

@Data
public class LoginUser {
	private String name;
	private String password;
	
	public LoginUser(String name, String password) {
		this.name = name;
		this.password = password;
	}
	
}
