package ase.G37.client.models;

import lombok.Data;

@Data
public class Rule {
	private String ruleName;
	private String eventConditionAction;
	private String userName;
	private String token;
	
	public Rule(String ruleName, String eventConditionAction, String userName, String token) {
		this.ruleName = ruleName;
		this.eventConditionAction = eventConditionAction;
		this.userName = userName;
		this.token = token;
	}
}
