package ase.G37.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ase.G37.client.models.Device;
import ase.G37.client.models.ServerResponse;
import ase.G37.client.models.LoginUser;
import ase.G37.client.models.RegisterUser;
import ase.G37.client.models.Room;
import ase.G37.client.models.Rule;
import ase.G37.client.models.Subscription;
import ase.G37.client.serializers.LoginUserSerializer;
import ase.G37.client.serializers.RegisterUserSerializer;
import ase.G37.client.serializers.RuleSerializer;
import ase.G37.client.serializers.SubscriptionSerializer;

@SuppressWarnings("restriction")
public class HomeAutomationClient extends Application {
	private Stage window;
	private Scene scene1, scene2, scene3;
    private Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12, button13, button14;
    
    private final Clipboard clipboard = Clipboard.getSystemClipboard();
    private final ClipboardContent content = new ClipboardContent();

    private String demoDefaultText = //Enter Default Setup Commands Here
            "Line1\n" +
            "Line2\n" +
            "Line3\n" +
            "Line4\n" +
            "Line5\n" +
            "Line6\n" +
            "Line7\n" +
            "Line8\n" +
            "Line9\n" +
            "Line10";

    private String setupDefaultText = //Enter Default Setup Commands Here
            "LLLine1\n" +
                    "LLLine2\n" +
                    "LLLine3\n" +
                    "LLLine4\n" +
                    "LLLine5\n" +
                    "LLLine6\n" +
                    "LLLine7\n" +
                    "LLLine8\n" +
                    "LLLine9\n" +
                    "LLLine10";

    private List<String> splitter = new ArrayList<String>();
	private int counter = -1;

	private static String localhost; // Reference to G37C server
	private static Client client;
	private static WebTarget target;
	private static LogHandler log;
	
	private static String jwtToken;
	private static String userName;
    
	public static void initializeClient() {
		client = ClientBuilder.newClient();
		target = client.target(UriBuilder.fromUri(localhost).build());
		log = LogHandler.getLog();
	}
	
	public static ServerResponse login(String loginUserJson) {
		String responseJson = target.path("user").path("login").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(loginUserJson), String.class);
		ServerResponse serverResponse = new Gson().fromJson(responseJson, ServerResponse.class);

		return new ServerResponse(serverResponse.getToken(), serverResponse.getErrorMessage(), serverResponse.getUserName());
	}
	
	public static void addAdmin(String addAdminJson) {
		target.path("user").path("addAdmin").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(addAdminJson));
	}
	
	public static String addUser(String registerUserJson) {
		return target.path("user").path("register").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(registerUserJson), String.class);
	}
	
	public static String removeUser(String removeUserJson) {
		return target.path("user").path("unregister").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(removeUserJson), String.class);
	}
	
	public static String listUsers(String tuple) {
		return target.path("user").path("list/" + tuple).request().get(String.class);
	}
	
	public static String addRoom(String roomJson) {
		return target.path("room").path("add").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(roomJson), String.class);
	}
	
	public static String removeRoom(String roomJson) {
		return target.path("room").path("remove").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(roomJson), String.class);
	}
	
	public static String listRooms(String tuple) {
		return target.path("room").path("list/" + tuple).request().get(String.class);
	}
	
	public static String registerDevice(String registerDeviceJson) {
		return target.path("device").path("register").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(registerDeviceJson), String.class);
	}
	
	public static String unregisterDevice(String unregisterDeviceJson) {
		return target.path("device").path("unregister").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(unregisterDeviceJson), String.class);
	}
	
	public static String moveDevice(String moveDeviceJson) {
		return target.path("device").path("move").request(MediaType.APPLICATION_JSON_TYPE).put(Entity.json(moveDeviceJson), String.class);
	}
	
	public static String addSubscription(String addSubscriptionJson) {
		return target.path("subscr").path("addsubscr").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(addSubscriptionJson), String.class);
	}
	
	public static String removeSubscription(String removeSubscriptionJson) {
		return target.path("subscr").path("rmsubscr").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(removeSubscriptionJson), String.class);
	}
	
	public static String addRule(String addRuleJson) {
		return target.path("rule").path("addrule").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(addRuleJson), String.class);
	}
	
	public static String removeRule(String removeRuleJson) {
		return target.path("rule").path("rmrule").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(removeRuleJson), String.class);
	}
	
	public static String listDevices(String tuple) {
		return target.path("device/list").path(tuple).request().get(String.class);
	}
	
	public static String peekDevice(String tuple) {
		return target.path("device/peek").path(tuple).request().get(String.class);
	}
	
	public static String pokeDevice(String pokeDeviceJson) {
		return target.path("device").path("poke").request(MediaType.APPLICATION_JSON_TYPE).put(Entity.json(pokeDeviceJson), String.class);
	}
	
	public static String listRules(String pair) {
		return target.path("rule/list").path(pair).request().get(String.class);
	}
	
	public static String listSubscriptions(String pair) {
		return target.path("subscr/list").path(pair).request().get(String.class);
	}
	
	public static String getManual() {
		return "\n***\n\nType one of the following commands to execute the operations: \n\n" +
                "login [username] [password] - log in to G37C.\n" +
                "logout - log out of G37C.\n" +
                "adduser [username] [password] [user-description] [user-type] - add a new user to G37.\n" +
                "rmuser [username] - remove a user from G37.\n" +
                "addroom [room-id] - add a new room to G37C.\n" +
                "rmroom [room-id] - remove a room from G37C.\n" +  
                "reg [device-url] [room-id] - register the new device with G37C.\n" +
                "unreg [device-url] - uninstall a device.\n" +
                "mv [device-url] [room-id] - move a device to another room.\n" +
                "lsdev [-A] [room-id] - list all accessible devices.\n" +
                "addsubscr [subscription-id] [dev-class] [list-of-rooms] [username] - create a new subscription.\n" +
                "rmsubscr [subscription-id] - delete a subscription.\n" +
                "lssubscr - list all subscriptions.\n" +
                "lsroom - list all rooms in the house.\n" +
                "peek [device-url] [prop-name] - return the value of a device property.\n" +
                "poke [device-url] [prop-name] [prop-value] - assign a new value to a device property.\n" +
                "addrule [rule-name] [event-condition-action] - create a new rule.\n" +
                "rmrule [rule-name] - delete a existing rule.\n" +
                "lsrule - list all created rules.\n" +
                "manual - display the manual.\n"+
                "setup - run all commands in setup.txt\n\n***";
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
    public void start(Stage primaryStage) throws Exception {
		
		if (Files.exists(Paths.get("demo.txt"))) {
            // do something
			System.out.println("demo.txt found.");
            demoDefaultText = new String(Files.readAllBytes(Paths.get("demo.txt")));
        } else {
            PrintWriter writer = new PrintWriter("demo.txt", "UTF-8");
            writer.println(demoDefaultText);
            writer.close();
            System.out.println("demo.txt created.");
        }

		if (Files.exists(Paths.get("setup.txt"))) {
            // do something
			System.out.println("setup.txt found.");
            setupDefaultText = new String(Files.readAllBytes(Paths.get("setup.txt")));
        } else {
            PrintWriter writer = new PrintWriter("setup.txt", "UTF-8");
            writer.println(setupDefaultText);
            writer.close();
            System.out.println("setup.txt created.");
        }

		window = primaryStage;
        window.setTitle("Home Automation Client");

        // Form
        TextField controllerURL = new TextField("http://localhost:8080/G37/api");
        
        TextField commandInput = new TextField();
        
        Label controllerConnec = new Label("Controller URL:");
        controllerConnec.setTextFill(Color.BLUE);

        TextArea demoText = new TextArea(demoDefaultText);
        demoText.setPrefHeight(360);

        TextArea setupText = new TextArea(setupDefaultText);
        setupText.setPrefHeight(360);

        Label output = new Label();
        output.setTextFill(Color.DARKBLUE);
        output.setFont(Font.font("Verdana", FontWeight.BOLD, 16));

        Label setupHeader = new Label("Setup.txt");
        setupHeader.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        Label demoHeader = new Label("Demo.txt");
        demoHeader.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        Label history = new Label();
        history.setAlignment(Pos.TOP_LEFT);
        history.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        history.setMinHeight(250);

        Label setupShortcut = new Label("Press UP and DOWN arrow keys for Demo command shortcuts...");
        setupShortcut.setTextFill(Color.DARKOLIVEGREEN);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(history);
        scrollPane.setPrefHeight(200);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        ScrollPane scrollPane2 = new ScrollPane();
        scrollPane2.setContent(output);
        scrollPane2.setPrefHeight(90);
        scrollPane2.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane2.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        
        // Button 1
        button1 = new Button("Run");
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            		
            		if (controllerConnec.getText()=="Connection Succesful!!!") {
	            		try {
	            		String c = "";
	            		String[] input = commandInput.getText().split("\"?( |$)(?=(([^\"]*\"){2})*[^\"]*$)\"?");
	            		
	            		switch (input[0].toLowerCase()) {
		    				case "login":
		    					if (!checkIfTokenIsSet()) {
		    						LoginUser loginUser = new LoginUser(input[1], input[2]);
		    						Gson loginUserGson = new GsonBuilder().registerTypeAdapter(LoginUser.class, new LoginUserSerializer()).create();
		    						ServerResponse serverResponse = login(loginUserGson.toJson(loginUser));
		    						jwtToken = serverResponse.getToken();
		    						userName = serverResponse.getUserName();
		    						if (jwtToken != null)
		    							c = "Login successful.";
		    						else
		    							c = serverResponse.getErrorMessage();
		    					}
		    					else
		    						c = "You are already logged in!";
		    					break;
		    				case "logout":
		    					if (checkIfTokenIsSet()) {
		    						jwtToken = null;
		    						c = "Logout successful.";
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "adduser":
		    					if (checkIfTokenIsSet() && userName != null) {
			    					RegisterUser regUser = new RegisterUser(input[1], input[2], input[3], input[4], userName);
			    					Gson regUserGson = new GsonBuilder().registerTypeAdapter(RegisterUser.class, new RegisterUserSerializer()).create();
			    					c = addUser(regUserGson.toJson(regUser));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "rmuser":
		    					if (checkIfTokenIsSet() && userName != null) {
			    					RegisterUser remUser = new RegisterUser(input[1], null, null, null, userName);
			    					Gson remUserGson = new GsonBuilder().serializeNulls().create();
			    					c = removeUser(remUserGson.toJson(remUser));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "lsusers":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						c = listUsers(userName + "/" + jwtToken);
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "addroom":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Room addRoom = new Room(input[1], userName, jwtToken);
		    						Gson addRoomGson = new GsonBuilder().serializeNulls().create();
		    						c = addRoom(addRoomGson.toJson(addRoom));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "rmroom":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Room remRoom = new Room(input[1], userName, jwtToken);
		    						Gson remRoomGson = new GsonBuilder().serializeNulls().create();
		    						c = removeRoom(remRoomGson.toJson(remRoom));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "reg":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Device regDevice = new Device(input[1], input[2], jwtToken, userName, null, null);
		    						Gson regDeviceGson = new GsonBuilder().serializeNulls().create();
		    						c = registerDevice(regDeviceGson.toJson(regDevice));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "unreg":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Device unregDevice = new Device(input[1], null, jwtToken, userName, null, null);
		    						Gson unregDeviceGson = new GsonBuilder().serializeNulls().create();
		    						c = unregisterDevice(unregDeviceGson.toJson(unregDevice));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "mv":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Device moveDevice = new Device(input[1], input[2], jwtToken, userName, null, null);
		    						Gson moveDeviceGson = new GsonBuilder().serializeNulls().create();
		    						c = moveDevice(moveDeviceGson.toJson(moveDevice));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "lsdev":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						if (input.length == 1) {
		    							input = Arrays.copyOf(input, 2);
		    							input[1] = "none";
		    						}
		    						c = listDevices(input[1] + "/" + userName + "/" + jwtToken);
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "addsubscr":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Subscription addSubscription = new Subscription(input[1], input[2], input[3], input[4], jwtToken, userName);
		    						Gson addSubscriptionGson = new GsonBuilder().registerTypeAdapter(Subscription.class, new SubscriptionSerializer()).create();
		    						c = addSubscription(addSubscriptionGson.toJson(addSubscription));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "rmsubscr":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Subscription remSubscription = new Subscription(input[1], null, null, null, jwtToken, userName);
		    						Gson remSubscriptionGson = new GsonBuilder().serializeNulls().create();
		    						c = removeSubscription(remSubscriptionGson.toJson(remSubscription));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "lssubscr":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						c = listSubscriptions(userName + "/" + jwtToken);
		    					}
		    					break;
		    				case "lsroom":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						c = listRooms(userName + "/" + jwtToken);
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "peek":
		    					if (checkIfTokenIsSet() && userName != null) {					
		    						c = peekDevice(input[1] + "/" + input[2] + "/" + userName + "/" + jwtToken);
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "poke":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Device pokeDevice = new Device(input[1], null, jwtToken, userName, input[2], input[3]);
		    						Gson pokeDeviceGson = new GsonBuilder().serializeNulls().create();
		    						c = pokeDevice(pokeDeviceGson.toJson(pokeDevice));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "addrule":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Rule addRule = new Rule(input[1], input[2], userName, jwtToken);
		    						Gson addRuleGson = new GsonBuilder().registerTypeAdapter(Rule.class, new RuleSerializer()).create();
		    						c = addRule(addRuleGson.toJson(addRule));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "rmrule":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						Rule removeRule = new Rule(input[1], null, userName, jwtToken);
		    						Gson removeRuleGson = new GsonBuilder().serializeNulls().create();
		    						c = removeRule(removeRuleGson.toJson(removeRule));
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "lsrule":
		    					if (checkIfTokenIsSet() && userName != null) {
		    						c = listRules(userName + "/" + jwtToken);
		    					}
		    					else
		    						c = "You are not logged in.";
		    					break;
		    				case "manual":
		    					c = getManual();
		    					break;
		    				case "setup":
		    					button5.fire();
		    					break;
		    				default:
		    					c = "No such command.";
		    					break;
	            		}
	            		
	            		log.logEntry(c);
	                    String h = "> " + commandInput.getText() + "\n" + c + "\n";
	                    if (commandInput.getText().equals("manual")) {
	                        output.setText("> manual" + "\n" + "Output too long, please view in the history");
	                    }
	                    else
	                        output.setText(h);
	                    history.setText(history.getText() + h + "\n");
	                    history.heightProperty().addListener(observable -> scrollPane.setVvalue(1.0));
	                    commandInput.setText("");
	                    }catch(Exception e) {
	                    	output.setText(":( Are you connected to the Controller?");
	                    	controllerConnec.setText("Connection Lost!!! Connect Again.");
	                    	controllerConnec.setTextFill(Color.RED);}
                    }
            		else
            			output.setText(":( Are you connected to the Controller?");
            	
            }
        });

      // Button 2
        button2 = new Button("Demo.txt/Setup.txt");
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                window.setScene(scene3);
                if (demoHeader.getText() == "Demo.txt (UPDATED)") {
                    demoHeader.setText("Demo.txt (MODIFIED)");
                    demoHeader.setTextFill(Color.BLACK);
                }
                else if (demoHeader.getText() == "Demo.txt (DEFAULT MODIFIED)"){
                    demoHeader.setText("Demo.txt");
                    demoHeader.setTextFill(Color.BLACK);
                }
            }
        });

        // Button 3
        button3 = new Button("Client");
        button3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                window.setScene(scene1);
                output.setText("");
            }
        });

        // Button 4
        button4 = new Button("Clear History");
        button4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                history.setText("");
                history.heightProperty().addListener(observable -> scrollPane.setVvalue(0.0));
                output.setText("");
                commandInput.setText("");
            }
        });

      // Button 5
        button5 = new Button("Run Setup");
        button5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (controllerConnec.getText()=="Connection Succesful!!!") {
            	splitter.clear();
                splitSetupText(splitter, setupText.getText());
                for (String setupCommand : splitter){
                    commandInput.setText(setupCommand);
                    button1.fire();
                }
                output.setText("All Setup commands succesfully executed. Check history tab for details.");}
                else 
                	output.setText(":( Are you connected to the Controller?");
            }
        });

        // Button 6
        button6 = new Button("Restore Default");
        button6.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            demoText.setText(demoDefaultText);
            demoHeader.setText("Demo.txt");
            demoHeader.setTextFill(Color.BLACK);
            }
        });

        // Button 7
        button7 = new Button("Switch to Demo.txt");
        button7.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                window.setScene(scene3);
                if (demoHeader.getText() == "Demo.txt (UPDATED)") {
                    demoHeader.setText("Demo.txt (MODIFIED)");
                    demoHeader.setTextFill(Color.BLACK);
                }
            }
        });

        //Button 8
        button8 = new Button("Switch to Setup.txt");
        button8.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                window.setScene(scene2);
            }
        });

        // Button 9
        button9 = new Button("Client");
        button9.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                window.setScene(scene1);
                output.setText("");
            }
        });
        
        // Button 10
        button10 = new Button("Set as new Default");
        button10.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    demoDefaultText = demoText.getText();
                    PrintWriter writer = new PrintWriter("demo.txt", "UTF-8");
                    writer.println(demoDefaultText);
                    writer.close();
                    System.out.println("New Demo.txt default is set.");
                    demoHeader.setText("Demo.txt (DEFAULT MODIFIED)");
                    demoHeader.setTextFill(Color.BLACK);
                } catch(Exception e) {
                    System.out.println("Demo.txt not found");
                    }
            }
        });
        
        // Button 11
        button11 = new Button("Restore Default");
        button11.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            setupText.setText(setupDefaultText);
            setupHeader.setText("Setup.txt");
            setupHeader.setTextFill(Color.BLACK);
            }
        });
        
        // Button 12
        button12 = new Button("Set as new Default");
        button12.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    setupDefaultText = setupText.getText();
                    PrintWriter writer = new PrintWriter("setup.txt", "UTF-8");
                    writer.println(setupDefaultText);
                    writer.close();
                    System.out.println("New Setup.txt default is set.");
                    setupHeader.setText("Setup.txt (DEFAULT MODIFIED)");
                    setupHeader.setTextFill(Color.BLACK);
                } catch (Exception e) {
                    System.out.println("Setup.txt not found");
                }
            }
        });

        // Button 13
        button13 = new Button("Copy to Clipboard");
        button13.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                content.putString(history.getText());
                clipboard.setContent(content);
            }
        });

     // Button 14
        button14 = new Button("Connect to Controller");
        button14.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	localhost = controllerURL.getText();
            	initializeClient();
            	controllerConnec.setText("Connecting...");
            	try{
            	RegisterUser regAdmin = new RegisterUser("admin", "admin", "Administrator account", "EU", "admin");
        		Gson regAdminGson = new GsonBuilder().serializeNulls().create();
        		addAdmin(regAdminGson.toJson(regAdmin));
        		controllerConnec.setText("Connection Succesful!!!");
        		controllerConnec.setTextFill(Color.GREEN);}
            	catch(Exception e) {
        		controllerConnec.setText("Connection Refused. Is the Controller URL correct? The server seems to be down. Please try Again!!!");
        		controllerConnec.setTextFill(Color.RED);
        		}
            }
        });

        
        commandInput.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                output.setText("");
            }
        });

        commandInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    button1.fire();
                }
                else if (event.getCode().equals(KeyCode.DOWN)) {
                    commandInput.setText("");
                    splitter.clear();
                    splitSetupText(splitter, demoText.getText());
                    output.setText("");
                    if (counter > 0) {
                        commandInput.setText(splitter.get(--counter));
                    }
                    else {
                        counter = splitter.size();
                    }

                }
                else if (event.getCode().equals(KeyCode.UP)) {
                    commandInput.setText("");
                    splitter.clear();
                    splitSetupText(splitter, demoText.getText());
                    output.setText("");
                    if (counter< splitter.size()-1) {
                        commandInput.setText(splitter.get(++counter));
                    }
                    else {
                        counter = -1;
                    }
                }
                else {
                    output.setText("");
                }
            }
        });

        demoText.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                demoHeader.setText("Demo.txt (UPDATED)");
                demoHeader.setTextFill(Color.RED);
            }
        });

        setupText.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                setupHeader.setText("Setup.txt (UPDATED)");
                setupHeader.setTextFill(Color.RED);
            }
        });

        
        // Layout1
        VBox layout1 = new VBox(15);
        layout1.setPadding(new Insets(20, 20, 20, 20));
        layout1.getChildren().addAll(button14, controllerConnec, controllerURL, button4, history, scrollPane, button13, setupShortcut, commandInput, button1, output, scrollPane2, button5, button2);
        scene1 = new Scene(layout1, 1200, 650);

        // Layout2
        VBox layout2 = new VBox(15);
        layout2.setPadding(new Insets(20, 20, 20, 20));
        scene2 = new Scene(layout2, 1200, 650);
        layout2.getChildren().addAll(button3, setupHeader, button11, button12, setupText, button7);

        // Layout3
        VBox layout3 = new VBox(15);
        layout3.setPadding(new Insets(20, 20, 20, 20));
        scene3 = new Scene(layout3, 1200, 650);
        layout3.getChildren().addAll(button9, demoHeader, button6, button10, demoText, button8);

        window.setScene(scene1);
        window.show();
    }

    private void splitSetupText(List<String> stringList, String message) {
        for (String s : message.split("\n")) {
            stringList.add(s.trim());
        }
    }

	private static boolean checkIfTokenIsSet() {
		if (jwtToken == null)
			return false;
		
		return true;
	}
}
