/*
 * generated by Xtext 2.17.1
 */
grammar InternalDsl;

options {
	superClass=AbstractInternalAntlrParser;
}

@lexer::header {
package g37.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package g37.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import g37.services.DslGrammarAccess;

}

@parser::members {

 	private DslGrammarAccess grammarAccess;

    public InternalDslParser(TokenStream input, DslGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }

    @Override
    protected String getFirstRuleName() {
    	return "Model";
   	}

   	@Override
   	protected DslGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}

}

@rulecatch {
    catch (RecognitionException re) {
        recover(input,re);
        appendSkippedTokens();
    }
}

// Entry rule entryRuleModel
entryRuleModel returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getModelRule()); }
	iv_ruleModel=ruleModel
	{ $current=$iv_ruleModel.current; }
	EOF;

// Rule Model
ruleModel returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			this_WS_0=RULE_WS
			{
				newLeafNode(this_WS_0, grammarAccess.getModelAccess().getWSTerminalRuleCall_0());
			}
		)*
		otherlv_1='IF'
		{
			newLeafNode(otherlv_1, grammarAccess.getModelAccess().getIFKeyword_1());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getModelAccess().getFirstElementalEventConditionParserRuleCall_2_0());
				}
				lv_first_2_0=ruleElementalEventCondition
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getModelRule());
					}
					set(
						$current,
						"first",
						lv_first_2_0,
						"g37.Dsl.ElementalEventCondition");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			(
				(
					{
						newCompositeNode(grammarAccess.getModelAccess().getOpBinaryOpBinaryParserRuleCall_3_0_0());
					}
					lv_opBinary_3_0=ruleOpBinary
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getModelRule());
						}
						add(
							$current,
							"opBinary",
							lv_opBinary_3_0,
							"g37.Dsl.OpBinary");
						afterParserOrEnumRuleCall();
					}
				)
			)
			(
				this_WS_4=RULE_WS
				{
					newLeafNode(this_WS_4, grammarAccess.getModelAccess().getWSTerminalRuleCall_3_1());
				}
			)+
			(
				(
					{
						newCompositeNode(grammarAccess.getModelAccess().getAdditionalElementalEventConditionParserRuleCall_3_2_0());
					}
					lv_additional_5_0=ruleElementalEventCondition
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getModelRule());
						}
						add(
							$current,
							"additional",
							lv_additional_5_0,
							"g37.Dsl.ElementalEventCondition");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
		(
			(
				{
					newCompositeNode(grammarAccess.getModelAccess().getActionActionParserRuleCall_4_0());
				}
				lv_action_6_0=ruleAction
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getModelRule());
					}
					set(
						$current,
						"action",
						lv_action_6_0,
						"g37.Dsl.Action");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			this_WS_7=RULE_WS
			{
				newLeafNode(this_WS_7, grammarAccess.getModelAccess().getWSTerminalRuleCall_5());
			}
		)+
		(
			(
				lv_arg_8_0=RULE_THE_REST
				{
					newLeafNode(lv_arg_8_0, grammarAccess.getModelAccess().getArgTHE_RESTTerminalRuleCall_6_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getModelRule());
					}
					setWithLastConsumed(
						$current,
						"arg",
						lv_arg_8_0,
						"g37.Dsl.THE_REST");
				}
			)
		)
	)
;

// Entry rule entryRuleElementalEventCondition
entryRuleElementalEventCondition returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getElementalEventConditionRule()); }
	iv_ruleElementalEventCondition=ruleElementalEventCondition
	{ $current=$iv_ruleElementalEventCondition.current; }
	EOF;

// Rule ElementalEventCondition
ruleElementalEventCondition returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			this_WS_0=RULE_WS
			{
				newLeafNode(this_WS_0, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_0());
			}
		)*
		otherlv_1='('
		{
			newLeafNode(otherlv_1, grammarAccess.getElementalEventConditionAccess().getLeftParenthesisKeyword_1());
		}
		(
			this_WS_2=RULE_WS
			{
				newLeafNode(this_WS_2, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_2());
			}
		)*
		(
			(
				lv_devUrl_3_0=RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES
				{
					newLeafNode(lv_devUrl_3_0, grammarAccess.getElementalEventConditionAccess().getDevUrlSEQ_NO_WS_NO_QUOTES_NO_PARENTHESESTerminalRuleCall_3_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getElementalEventConditionRule());
					}
					setWithLastConsumed(
						$current,
						"devUrl",
						lv_devUrl_3_0,
						"g37.Dsl.SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES");
				}
			)
		)
		(
			this_WS_4=RULE_WS
			{
				newLeafNode(this_WS_4, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_4());
			}
		)+
		(
			(
				{
					newCompositeNode(grammarAccess.getElementalEventConditionAccess().getPropNamePropertyNameOrValParserRuleCall_5_0());
				}
				lv_propName_5_0=rulePropertyNameOrVal
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
					}
					set(
						$current,
						"propName",
						lv_propName_5_0,
						"g37.Dsl.PropertyNameOrVal");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			this_WS_6=RULE_WS
			{
				newLeafNode(this_WS_6, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_6());
			}
		)+
		(
			(
				(
					{
						newCompositeNode(grammarAccess.getElementalEventConditionAccess().getNegationOpUnaryParserRuleCall_7_0_0());
					}
					lv_negation_7_0=ruleOpUnary
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
						}
						set(
							$current,
							"negation",
							true,
							"g37.Dsl.OpUnary");
						afterParserOrEnumRuleCall();
					}
				)
			)
			(
				this_WS_8=RULE_WS
				{
					newLeafNode(this_WS_8, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_7_1());
				}
			)+
		)?
		(
			(
				(
					(
						{
							newCompositeNode(grammarAccess.getElementalEventConditionAccess().getOpStrCompOpStrCompareParserRuleCall_8_0_0_0());
						}
						lv_opStrComp_9_0=ruleOpStrCompare
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
							}
							set(
								$current,
								"opStrComp",
								lv_opStrComp_9_0,
								"g37.Dsl.OpStrCompare");
							afterParserOrEnumRuleCall();
						}
					)
				)
				(
					this_WS_10=RULE_WS
					{
						newLeafNode(this_WS_10, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_8_0_1());
					}
				)+
				(
					(
						{
							newCompositeNode(grammarAccess.getElementalEventConditionAccess().getPropValuePropertyNameOrValParserRuleCall_8_0_2_0());
						}
						lv_propValue_11_0=rulePropertyNameOrVal
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
							}
							set(
								$current,
								"propValue",
								lv_propValue_11_0,
								"g37.Dsl.PropertyNameOrVal");
							afterParserOrEnumRuleCall();
						}
					)
				)
			)
			    |
			(
				(
					(
						{
							newCompositeNode(grammarAccess.getElementalEventConditionAccess().getOpFloatCompOpFloatCompareParserRuleCall_8_1_0_0());
						}
						lv_opFloatComp_12_0=ruleOpFloatCompare
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
							}
							set(
								$current,
								"opFloatComp",
								lv_opFloatComp_12_0,
								"g37.Dsl.OpFloatCompare");
							afterParserOrEnumRuleCall();
						}
					)
				)
				(
					this_WS_13=RULE_WS
					{
						newLeafNode(this_WS_13, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_8_1_1());
					}
				)+
				(
					(
						lv_propValue_14_0=RULE_FLOAT
						{
							newLeafNode(lv_propValue_14_0, grammarAccess.getElementalEventConditionAccess().getPropValueFLOATTerminalRuleCall_8_1_2_0());
						}
						{
							if ($current==null) {
								$current = createModelElement(grammarAccess.getElementalEventConditionRule());
							}
							setWithLastConsumed(
								$current,
								"propValue",
								lv_propValue_14_0,
								"g37.Dsl.FLOAT");
						}
					)
				)
			)
		)
		(
			this_WS_15=RULE_WS
			{
				newLeafNode(this_WS_15, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_9());
			}
		)*
		otherlv_16=')'
		{
			newLeafNode(otherlv_16, grammarAccess.getElementalEventConditionAccess().getRightParenthesisKeyword_10());
		}
		(
			this_WS_17=RULE_WS
			{
				newLeafNode(this_WS_17, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_11());
			}
		)*
	)
;

// Entry rule entryRuleAction
entryRuleAction returns [String current=null]:
	{ newCompositeNode(grammarAccess.getActionRule()); }
	iv_ruleAction=ruleAction
	{ $current=$iv_ruleAction.current.getText(); }
	EOF;

// Rule Action
ruleAction returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		kw='POKE'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getActionAccess().getPOKEKeyword_0());
		}
		    |
		kw='EMAIL'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getActionAccess().getEMAILKeyword_1());
		}
	)
;

// Entry rule entryRuleOpBinary
entryRuleOpBinary returns [String current=null]:
	{ newCompositeNode(grammarAccess.getOpBinaryRule()); }
	iv_ruleOpBinary=ruleOpBinary
	{ $current=$iv_ruleOpBinary.current.getText(); }
	EOF;

// Rule OpBinary
ruleOpBinary returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		kw='AND'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpBinaryAccess().getANDKeyword_0());
		}
		    |
		kw='OR'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpBinaryAccess().getORKeyword_1());
		}
	)
;

// Entry rule entryRuleOpUnary
entryRuleOpUnary returns [String current=null]:
	{ newCompositeNode(grammarAccess.getOpUnaryRule()); }
	iv_ruleOpUnary=ruleOpUnary
	{ $current=$iv_ruleOpUnary.current.getText(); }
	EOF;

// Rule OpUnary
ruleOpUnary returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	kw='NOT'
	{
		$current.merge(kw);
		newLeafNode(kw, grammarAccess.getOpUnaryAccess().getNOTKeyword());
	}
;

// Entry rule entryRuleOpStrCompare
entryRuleOpStrCompare returns [String current=null]:
	{ newCompositeNode(grammarAccess.getOpStrCompareRule()); }
	iv_ruleOpStrCompare=ruleOpStrCompare
	{ $current=$iv_ruleOpStrCompare.current.getText(); }
	EOF;

// Rule OpStrCompare
ruleOpStrCompare returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		kw='=='
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpStrCompareAccess().getEqualsSignEqualsSignKeyword_0());
		}
		    |
		kw='contains'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpStrCompareAccess().getContainsKeyword_1());
		}
	)
;

// Entry rule entryRuleOpFloatCompare
entryRuleOpFloatCompare returns [String current=null]:
	{ newCompositeNode(grammarAccess.getOpFloatCompareRule()); }
	iv_ruleOpFloatCompare=ruleOpFloatCompare
	{ $current=$iv_ruleOpFloatCompare.current.getText(); }
	EOF;

// Rule OpFloatCompare
ruleOpFloatCompare returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		kw='>'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getGreaterThanSignKeyword_0());
		}
		    |
		kw='<'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getLessThanSignKeyword_1());
		}
		    |
		kw='>='
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getGreaterThanSignEqualsSignKeyword_2());
		}
		    |
		kw='<='
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getLessThanSignEqualsSignKeyword_3());
		}
		    |
		kw='=='
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getEqualsSignEqualsSignKeyword_4());
		}
	)
;

// Entry rule entryRulePropertyNameOrVal
entryRulePropertyNameOrVal returns [String current=null]:
	{ newCompositeNode(grammarAccess.getPropertyNameOrValRule()); }
	iv_rulePropertyNameOrVal=rulePropertyNameOrVal
	{ $current=$iv_rulePropertyNameOrVal.current.getText(); }
	EOF;

// Rule PropertyNameOrVal
rulePropertyNameOrVal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0=RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES
		{
			$current.merge(this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0);
		}
		{
			newLeafNode(this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0, grammarAccess.getPropertyNameOrValAccess().getSEQ_NO_WS_NO_QUOTES_NO_PARENTHESESTerminalRuleCall_0());
		}
		    |
		this_SNGLQUOTED_NON_EMPTY_SEQ_1=RULE_SNGLQUOTED_NON_EMPTY_SEQ
		{
			$current.merge(this_SNGLQUOTED_NON_EMPTY_SEQ_1);
		}
		{
			newLeafNode(this_SNGLQUOTED_NON_EMPTY_SEQ_1, grammarAccess.getPropertyNameOrValAccess().getSNGLQUOTED_NON_EMPTY_SEQTerminalRuleCall_1());
		}
	)
;

RULE_FLOAT : (('0'..'9')+|('0'..'9')+ '.' ('0'..'9')+);

RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES : ~((' '|'\t'|'\r'|'\n'|'\''|'('|')'|'"'))+;

RULE_SNGLQUOTED_NON_EMPTY_SEQ : '\'' ~(('\''|'\r'|'\n'|'('|')'|'"'))+ '\'';

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_THE_REST : '[' ~(('['|']'))* ']' (' '|'\t')* '\r'? '\n'? EOF;
