package g37.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import g37.services.DslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_WS", "RULE_THE_REST", "RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES", "RULE_FLOAT", "RULE_SNGLQUOTED_NON_EMPTY_SEQ", "'IF'", "'('", "')'", "'POKE'", "'EMAIL'", "'AND'", "'OR'", "'NOT'", "'=='", "'contains'", "'>'", "'<'", "'>='", "'<='"
    };
    public static final int RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES=6;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__10=10;
    public static final int T__9=9;
    public static final int RULE_WS=4;
    public static final int RULE_THE_REST=5;
    public static final int RULE_SNGLQUOTED_NON_EMPTY_SEQ=8;
    public static final int T__22=22;
    public static final int RULE_FLOAT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDsl.g"; }



     	private DslGrammarAccess grammarAccess;

        public InternalDslParser(TokenStream input, DslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected DslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalDsl.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalDsl.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalDsl.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalDsl.g:71:1: ruleModel returns [EObject current=null] : ( (this_WS_0= RULE_WS )* otherlv_1= 'IF' ( (lv_first_2_0= ruleElementalEventCondition ) ) ( ( (lv_opBinary_3_0= ruleOpBinary ) ) (this_WS_4= RULE_WS )+ ( (lv_additional_5_0= ruleElementalEventCondition ) ) )* ( (lv_action_6_0= ruleAction ) ) (this_WS_7= RULE_WS )+ ( (lv_arg_8_0= RULE_THE_REST ) ) ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token this_WS_0=null;
        Token otherlv_1=null;
        Token this_WS_4=null;
        Token this_WS_7=null;
        Token lv_arg_8_0=null;
        EObject lv_first_2_0 = null;

        AntlrDatatypeRuleToken lv_opBinary_3_0 = null;

        EObject lv_additional_5_0 = null;

        AntlrDatatypeRuleToken lv_action_6_0 = null;



        	enterRule();

        try {
            // InternalDsl.g:77:2: ( ( (this_WS_0= RULE_WS )* otherlv_1= 'IF' ( (lv_first_2_0= ruleElementalEventCondition ) ) ( ( (lv_opBinary_3_0= ruleOpBinary ) ) (this_WS_4= RULE_WS )+ ( (lv_additional_5_0= ruleElementalEventCondition ) ) )* ( (lv_action_6_0= ruleAction ) ) (this_WS_7= RULE_WS )+ ( (lv_arg_8_0= RULE_THE_REST ) ) ) )
            // InternalDsl.g:78:2: ( (this_WS_0= RULE_WS )* otherlv_1= 'IF' ( (lv_first_2_0= ruleElementalEventCondition ) ) ( ( (lv_opBinary_3_0= ruleOpBinary ) ) (this_WS_4= RULE_WS )+ ( (lv_additional_5_0= ruleElementalEventCondition ) ) )* ( (lv_action_6_0= ruleAction ) ) (this_WS_7= RULE_WS )+ ( (lv_arg_8_0= RULE_THE_REST ) ) )
            {
            // InternalDsl.g:78:2: ( (this_WS_0= RULE_WS )* otherlv_1= 'IF' ( (lv_first_2_0= ruleElementalEventCondition ) ) ( ( (lv_opBinary_3_0= ruleOpBinary ) ) (this_WS_4= RULE_WS )+ ( (lv_additional_5_0= ruleElementalEventCondition ) ) )* ( (lv_action_6_0= ruleAction ) ) (this_WS_7= RULE_WS )+ ( (lv_arg_8_0= RULE_THE_REST ) ) )
            // InternalDsl.g:79:3: (this_WS_0= RULE_WS )* otherlv_1= 'IF' ( (lv_first_2_0= ruleElementalEventCondition ) ) ( ( (lv_opBinary_3_0= ruleOpBinary ) ) (this_WS_4= RULE_WS )+ ( (lv_additional_5_0= ruleElementalEventCondition ) ) )* ( (lv_action_6_0= ruleAction ) ) (this_WS_7= RULE_WS )+ ( (lv_arg_8_0= RULE_THE_REST ) )
            {
            // InternalDsl.g:79:3: (this_WS_0= RULE_WS )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_WS) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDsl.g:80:4: this_WS_0= RULE_WS
            	    {
            	    this_WS_0=(Token)match(input,RULE_WS,FOLLOW_3); 

            	    				newLeafNode(this_WS_0, grammarAccess.getModelAccess().getWSTerminalRuleCall_0());
            	    			

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_1=(Token)match(input,9,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getModelAccess().getIFKeyword_1());
            		
            // InternalDsl.g:89:3: ( (lv_first_2_0= ruleElementalEventCondition ) )
            // InternalDsl.g:90:4: (lv_first_2_0= ruleElementalEventCondition )
            {
            // InternalDsl.g:90:4: (lv_first_2_0= ruleElementalEventCondition )
            // InternalDsl.g:91:5: lv_first_2_0= ruleElementalEventCondition
            {

            					newCompositeNode(grammarAccess.getModelAccess().getFirstElementalEventConditionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_first_2_0=ruleElementalEventCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModelRule());
            					}
            					set(
            						current,
            						"first",
            						lv_first_2_0,
            						"g37.Dsl.ElementalEventCondition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDsl.g:108:3: ( ( (lv_opBinary_3_0= ruleOpBinary ) ) (this_WS_4= RULE_WS )+ ( (lv_additional_5_0= ruleElementalEventCondition ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>=14 && LA3_0<=15)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalDsl.g:109:4: ( (lv_opBinary_3_0= ruleOpBinary ) ) (this_WS_4= RULE_WS )+ ( (lv_additional_5_0= ruleElementalEventCondition ) )
            	    {
            	    // InternalDsl.g:109:4: ( (lv_opBinary_3_0= ruleOpBinary ) )
            	    // InternalDsl.g:110:5: (lv_opBinary_3_0= ruleOpBinary )
            	    {
            	    // InternalDsl.g:110:5: (lv_opBinary_3_0= ruleOpBinary )
            	    // InternalDsl.g:111:6: lv_opBinary_3_0= ruleOpBinary
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getOpBinaryOpBinaryParserRuleCall_3_0_0());
            	    					
            	    pushFollow(FOLLOW_6);
            	    lv_opBinary_3_0=ruleOpBinary();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"opBinary",
            	    							lv_opBinary_3_0,
            	    							"g37.Dsl.OpBinary");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalDsl.g:128:4: (this_WS_4= RULE_WS )+
            	    int cnt2=0;
            	    loop2:
            	    do {
            	        int alt2=2;
            	        int LA2_0 = input.LA(1);

            	        if ( (LA2_0==RULE_WS) ) {
            	            alt2=1;
            	        }


            	        switch (alt2) {
            	    	case 1 :
            	    	    // InternalDsl.g:129:5: this_WS_4= RULE_WS
            	    	    {
            	    	    this_WS_4=(Token)match(input,RULE_WS,FOLLOW_4); 

            	    	    					newLeafNode(this_WS_4, grammarAccess.getModelAccess().getWSTerminalRuleCall_3_1());
            	    	    				

            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt2 >= 1 ) break loop2;
            	                EarlyExitException eee =
            	                    new EarlyExitException(2, input);
            	                throw eee;
            	        }
            	        cnt2++;
            	    } while (true);

            	    // InternalDsl.g:134:4: ( (lv_additional_5_0= ruleElementalEventCondition ) )
            	    // InternalDsl.g:135:5: (lv_additional_5_0= ruleElementalEventCondition )
            	    {
            	    // InternalDsl.g:135:5: (lv_additional_5_0= ruleElementalEventCondition )
            	    // InternalDsl.g:136:6: lv_additional_5_0= ruleElementalEventCondition
            	    {

            	    						newCompositeNode(grammarAccess.getModelAccess().getAdditionalElementalEventConditionParserRuleCall_3_2_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_additional_5_0=ruleElementalEventCondition();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModelRule());
            	    						}
            	    						add(
            	    							current,
            	    							"additional",
            	    							lv_additional_5_0,
            	    							"g37.Dsl.ElementalEventCondition");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // InternalDsl.g:154:3: ( (lv_action_6_0= ruleAction ) )
            // InternalDsl.g:155:4: (lv_action_6_0= ruleAction )
            {
            // InternalDsl.g:155:4: (lv_action_6_0= ruleAction )
            // InternalDsl.g:156:5: lv_action_6_0= ruleAction
            {

            					newCompositeNode(grammarAccess.getModelAccess().getActionActionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_6);
            lv_action_6_0=ruleAction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModelRule());
            					}
            					set(
            						current,
            						"action",
            						lv_action_6_0,
            						"g37.Dsl.Action");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDsl.g:173:3: (this_WS_7= RULE_WS )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_WS) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalDsl.g:174:4: this_WS_7= RULE_WS
            	    {
            	    this_WS_7=(Token)match(input,RULE_WS,FOLLOW_7); 

            	    				newLeafNode(this_WS_7, grammarAccess.getModelAccess().getWSTerminalRuleCall_5());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            // InternalDsl.g:179:3: ( (lv_arg_8_0= RULE_THE_REST ) )
            // InternalDsl.g:180:4: (lv_arg_8_0= RULE_THE_REST )
            {
            // InternalDsl.g:180:4: (lv_arg_8_0= RULE_THE_REST )
            // InternalDsl.g:181:5: lv_arg_8_0= RULE_THE_REST
            {
            lv_arg_8_0=(Token)match(input,RULE_THE_REST,FOLLOW_2); 

            					newLeafNode(lv_arg_8_0, grammarAccess.getModelAccess().getArgTHE_RESTTerminalRuleCall_6_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getModelRule());
            					}
            					setWithLastConsumed(
            						current,
            						"arg",
            						lv_arg_8_0,
            						"g37.Dsl.THE_REST");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleElementalEventCondition"
    // InternalDsl.g:201:1: entryRuleElementalEventCondition returns [EObject current=null] : iv_ruleElementalEventCondition= ruleElementalEventCondition EOF ;
    public final EObject entryRuleElementalEventCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElementalEventCondition = null;


        try {
            // InternalDsl.g:201:64: (iv_ruleElementalEventCondition= ruleElementalEventCondition EOF )
            // InternalDsl.g:202:2: iv_ruleElementalEventCondition= ruleElementalEventCondition EOF
            {
             newCompositeNode(grammarAccess.getElementalEventConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleElementalEventCondition=ruleElementalEventCondition();

            state._fsp--;

             current =iv_ruleElementalEventCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElementalEventCondition"


    // $ANTLR start "ruleElementalEventCondition"
    // InternalDsl.g:208:1: ruleElementalEventCondition returns [EObject current=null] : ( (this_WS_0= RULE_WS )* otherlv_1= '(' (this_WS_2= RULE_WS )* ( (lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES ) ) (this_WS_4= RULE_WS )+ ( (lv_propName_5_0= rulePropertyNameOrVal ) ) (this_WS_6= RULE_WS )+ ( ( (lv_negation_7_0= ruleOpUnary ) ) (this_WS_8= RULE_WS )+ )? ( ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) ) | ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) ) ) (this_WS_15= RULE_WS )* otherlv_16= ')' (this_WS_17= RULE_WS )* ) ;
    public final EObject ruleElementalEventCondition() throws RecognitionException {
        EObject current = null;

        Token this_WS_0=null;
        Token otherlv_1=null;
        Token this_WS_2=null;
        Token lv_devUrl_3_0=null;
        Token this_WS_4=null;
        Token this_WS_6=null;
        Token this_WS_8=null;
        Token this_WS_10=null;
        Token this_WS_13=null;
        Token lv_propValue_14_0=null;
        Token this_WS_15=null;
        Token otherlv_16=null;
        Token this_WS_17=null;
        AntlrDatatypeRuleToken lv_propName_5_0 = null;

        AntlrDatatypeRuleToken lv_negation_7_0 = null;

        AntlrDatatypeRuleToken lv_opStrComp_9_0 = null;

        AntlrDatatypeRuleToken lv_propValue_11_0 = null;

        AntlrDatatypeRuleToken lv_opFloatComp_12_0 = null;



        	enterRule();

        try {
            // InternalDsl.g:214:2: ( ( (this_WS_0= RULE_WS )* otherlv_1= '(' (this_WS_2= RULE_WS )* ( (lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES ) ) (this_WS_4= RULE_WS )+ ( (lv_propName_5_0= rulePropertyNameOrVal ) ) (this_WS_6= RULE_WS )+ ( ( (lv_negation_7_0= ruleOpUnary ) ) (this_WS_8= RULE_WS )+ )? ( ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) ) | ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) ) ) (this_WS_15= RULE_WS )* otherlv_16= ')' (this_WS_17= RULE_WS )* ) )
            // InternalDsl.g:215:2: ( (this_WS_0= RULE_WS )* otherlv_1= '(' (this_WS_2= RULE_WS )* ( (lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES ) ) (this_WS_4= RULE_WS )+ ( (lv_propName_5_0= rulePropertyNameOrVal ) ) (this_WS_6= RULE_WS )+ ( ( (lv_negation_7_0= ruleOpUnary ) ) (this_WS_8= RULE_WS )+ )? ( ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) ) | ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) ) ) (this_WS_15= RULE_WS )* otherlv_16= ')' (this_WS_17= RULE_WS )* )
            {
            // InternalDsl.g:215:2: ( (this_WS_0= RULE_WS )* otherlv_1= '(' (this_WS_2= RULE_WS )* ( (lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES ) ) (this_WS_4= RULE_WS )+ ( (lv_propName_5_0= rulePropertyNameOrVal ) ) (this_WS_6= RULE_WS )+ ( ( (lv_negation_7_0= ruleOpUnary ) ) (this_WS_8= RULE_WS )+ )? ( ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) ) | ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) ) ) (this_WS_15= RULE_WS )* otherlv_16= ')' (this_WS_17= RULE_WS )* )
            // InternalDsl.g:216:3: (this_WS_0= RULE_WS )* otherlv_1= '(' (this_WS_2= RULE_WS )* ( (lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES ) ) (this_WS_4= RULE_WS )+ ( (lv_propName_5_0= rulePropertyNameOrVal ) ) (this_WS_6= RULE_WS )+ ( ( (lv_negation_7_0= ruleOpUnary ) ) (this_WS_8= RULE_WS )+ )? ( ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) ) | ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) ) ) (this_WS_15= RULE_WS )* otherlv_16= ')' (this_WS_17= RULE_WS )*
            {
            // InternalDsl.g:216:3: (this_WS_0= RULE_WS )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_WS) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalDsl.g:217:4: this_WS_0= RULE_WS
            	    {
            	    this_WS_0=(Token)match(input,RULE_WS,FOLLOW_4); 

            	    				newLeafNode(this_WS_0, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_0());
            	    			

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_1=(Token)match(input,10,FOLLOW_8); 

            			newLeafNode(otherlv_1, grammarAccess.getElementalEventConditionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalDsl.g:226:3: (this_WS_2= RULE_WS )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_WS) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalDsl.g:227:4: this_WS_2= RULE_WS
            	    {
            	    this_WS_2=(Token)match(input,RULE_WS,FOLLOW_8); 

            	    				newLeafNode(this_WS_2, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_2());
            	    			

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalDsl.g:232:3: ( (lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES ) )
            // InternalDsl.g:233:4: (lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES )
            {
            // InternalDsl.g:233:4: (lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES )
            // InternalDsl.g:234:5: lv_devUrl_3_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES
            {
            lv_devUrl_3_0=(Token)match(input,RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES,FOLLOW_6); 

            					newLeafNode(lv_devUrl_3_0, grammarAccess.getElementalEventConditionAccess().getDevUrlSEQ_NO_WS_NO_QUOTES_NO_PARENTHESESTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getElementalEventConditionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"devUrl",
            						lv_devUrl_3_0,
            						"g37.Dsl.SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES");
            				

            }


            }

            // InternalDsl.g:250:3: (this_WS_4= RULE_WS )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_WS) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalDsl.g:251:4: this_WS_4= RULE_WS
            	    {
            	    this_WS_4=(Token)match(input,RULE_WS,FOLLOW_9); 

            	    				newLeafNode(this_WS_4, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_4());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);

            // InternalDsl.g:256:3: ( (lv_propName_5_0= rulePropertyNameOrVal ) )
            // InternalDsl.g:257:4: (lv_propName_5_0= rulePropertyNameOrVal )
            {
            // InternalDsl.g:257:4: (lv_propName_5_0= rulePropertyNameOrVal )
            // InternalDsl.g:258:5: lv_propName_5_0= rulePropertyNameOrVal
            {

            					newCompositeNode(grammarAccess.getElementalEventConditionAccess().getPropNamePropertyNameOrValParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_6);
            lv_propName_5_0=rulePropertyNameOrVal();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
            					}
            					set(
            						current,
            						"propName",
            						lv_propName_5_0,
            						"g37.Dsl.PropertyNameOrVal");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDsl.g:275:3: (this_WS_6= RULE_WS )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_WS) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalDsl.g:276:4: this_WS_6= RULE_WS
            	    {
            	    this_WS_6=(Token)match(input,RULE_WS,FOLLOW_10); 

            	    				newLeafNode(this_WS_6, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_6());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            // InternalDsl.g:281:3: ( ( (lv_negation_7_0= ruleOpUnary ) ) (this_WS_8= RULE_WS )+ )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==16) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDsl.g:282:4: ( (lv_negation_7_0= ruleOpUnary ) ) (this_WS_8= RULE_WS )+
                    {
                    // InternalDsl.g:282:4: ( (lv_negation_7_0= ruleOpUnary ) )
                    // InternalDsl.g:283:5: (lv_negation_7_0= ruleOpUnary )
                    {
                    // InternalDsl.g:283:5: (lv_negation_7_0= ruleOpUnary )
                    // InternalDsl.g:284:6: lv_negation_7_0= ruleOpUnary
                    {

                    						newCompositeNode(grammarAccess.getElementalEventConditionAccess().getNegationOpUnaryParserRuleCall_7_0_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_negation_7_0=ruleOpUnary();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
                    						}
                    						set(
                    							current,
                    							"negation",
                    							true,
                    							"g37.Dsl.OpUnary");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDsl.g:301:4: (this_WS_8= RULE_WS )+
                    int cnt9=0;
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==RULE_WS) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalDsl.g:302:5: this_WS_8= RULE_WS
                    	    {
                    	    this_WS_8=(Token)match(input,RULE_WS,FOLLOW_10); 

                    	    					newLeafNode(this_WS_8, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_7_1());
                    	    				

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt9 >= 1 ) break loop9;
                                EarlyExitException eee =
                                    new EarlyExitException(9, input);
                                throw eee;
                        }
                        cnt9++;
                    } while (true);


                    }
                    break;

            }

            // InternalDsl.g:308:3: ( ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) ) | ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) ) )
            int alt13=2;
            alt13 = dfa13.predict(input);
            switch (alt13) {
                case 1 :
                    // InternalDsl.g:309:4: ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) )
                    {
                    // InternalDsl.g:309:4: ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) )
                    // InternalDsl.g:310:5: ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) )
                    {
                    // InternalDsl.g:310:5: ( (lv_opStrComp_9_0= ruleOpStrCompare ) )
                    // InternalDsl.g:311:6: (lv_opStrComp_9_0= ruleOpStrCompare )
                    {
                    // InternalDsl.g:311:6: (lv_opStrComp_9_0= ruleOpStrCompare )
                    // InternalDsl.g:312:7: lv_opStrComp_9_0= ruleOpStrCompare
                    {

                    							newCompositeNode(grammarAccess.getElementalEventConditionAccess().getOpStrCompOpStrCompareParserRuleCall_8_0_0_0());
                    						
                    pushFollow(FOLLOW_6);
                    lv_opStrComp_9_0=ruleOpStrCompare();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
                    							}
                    							set(
                    								current,
                    								"opStrComp",
                    								lv_opStrComp_9_0,
                    								"g37.Dsl.OpStrCompare");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalDsl.g:329:5: (this_WS_10= RULE_WS )+
                    int cnt11=0;
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==RULE_WS) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalDsl.g:330:6: this_WS_10= RULE_WS
                    	    {
                    	    this_WS_10=(Token)match(input,RULE_WS,FOLLOW_9); 

                    	    						newLeafNode(this_WS_10, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_8_0_1());
                    	    					

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt11 >= 1 ) break loop11;
                                EarlyExitException eee =
                                    new EarlyExitException(11, input);
                                throw eee;
                        }
                        cnt11++;
                    } while (true);

                    // InternalDsl.g:335:5: ( (lv_propValue_11_0= rulePropertyNameOrVal ) )
                    // InternalDsl.g:336:6: (lv_propValue_11_0= rulePropertyNameOrVal )
                    {
                    // InternalDsl.g:336:6: (lv_propValue_11_0= rulePropertyNameOrVal )
                    // InternalDsl.g:337:7: lv_propValue_11_0= rulePropertyNameOrVal
                    {

                    							newCompositeNode(grammarAccess.getElementalEventConditionAccess().getPropValuePropertyNameOrValParserRuleCall_8_0_2_0());
                    						
                    pushFollow(FOLLOW_11);
                    lv_propValue_11_0=rulePropertyNameOrVal();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
                    							}
                    							set(
                    								current,
                    								"propValue",
                    								lv_propValue_11_0,
                    								"g37.Dsl.PropertyNameOrVal");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalDsl.g:356:4: ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) )
                    {
                    // InternalDsl.g:356:4: ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) )
                    // InternalDsl.g:357:5: ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) )
                    {
                    // InternalDsl.g:357:5: ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) )
                    // InternalDsl.g:358:6: (lv_opFloatComp_12_0= ruleOpFloatCompare )
                    {
                    // InternalDsl.g:358:6: (lv_opFloatComp_12_0= ruleOpFloatCompare )
                    // InternalDsl.g:359:7: lv_opFloatComp_12_0= ruleOpFloatCompare
                    {

                    							newCompositeNode(grammarAccess.getElementalEventConditionAccess().getOpFloatCompOpFloatCompareParserRuleCall_8_1_0_0());
                    						
                    pushFollow(FOLLOW_6);
                    lv_opFloatComp_12_0=ruleOpFloatCompare();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getElementalEventConditionRule());
                    							}
                    							set(
                    								current,
                    								"opFloatComp",
                    								lv_opFloatComp_12_0,
                    								"g37.Dsl.OpFloatCompare");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalDsl.g:376:5: (this_WS_13= RULE_WS )+
                    int cnt12=0;
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==RULE_WS) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalDsl.g:377:6: this_WS_13= RULE_WS
                    	    {
                    	    this_WS_13=(Token)match(input,RULE_WS,FOLLOW_12); 

                    	    						newLeafNode(this_WS_13, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_8_1_1());
                    	    					

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt12 >= 1 ) break loop12;
                                EarlyExitException eee =
                                    new EarlyExitException(12, input);
                                throw eee;
                        }
                        cnt12++;
                    } while (true);

                    // InternalDsl.g:382:5: ( (lv_propValue_14_0= RULE_FLOAT ) )
                    // InternalDsl.g:383:6: (lv_propValue_14_0= RULE_FLOAT )
                    {
                    // InternalDsl.g:383:6: (lv_propValue_14_0= RULE_FLOAT )
                    // InternalDsl.g:384:7: lv_propValue_14_0= RULE_FLOAT
                    {
                    lv_propValue_14_0=(Token)match(input,RULE_FLOAT,FOLLOW_11); 

                    							newLeafNode(lv_propValue_14_0, grammarAccess.getElementalEventConditionAccess().getPropValueFLOATTerminalRuleCall_8_1_2_0());
                    						

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getElementalEventConditionRule());
                    							}
                    							setWithLastConsumed(
                    								current,
                    								"propValue",
                    								lv_propValue_14_0,
                    								"g37.Dsl.FLOAT");
                    						

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalDsl.g:402:3: (this_WS_15= RULE_WS )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_WS) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalDsl.g:403:4: this_WS_15= RULE_WS
            	    {
            	    this_WS_15=(Token)match(input,RULE_WS,FOLLOW_11); 

            	    				newLeafNode(this_WS_15, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_9());
            	    			

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            otherlv_16=(Token)match(input,11,FOLLOW_13); 

            			newLeafNode(otherlv_16, grammarAccess.getElementalEventConditionAccess().getRightParenthesisKeyword_10());
            		
            // InternalDsl.g:412:3: (this_WS_17= RULE_WS )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_WS) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalDsl.g:413:4: this_WS_17= RULE_WS
            	    {
            	    this_WS_17=(Token)match(input,RULE_WS,FOLLOW_13); 

            	    				newLeafNode(this_WS_17, grammarAccess.getElementalEventConditionAccess().getWSTerminalRuleCall_11());
            	    			

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElementalEventCondition"


    // $ANTLR start "entryRuleAction"
    // InternalDsl.g:422:1: entryRuleAction returns [String current=null] : iv_ruleAction= ruleAction EOF ;
    public final String entryRuleAction() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAction = null;


        try {
            // InternalDsl.g:422:46: (iv_ruleAction= ruleAction EOF )
            // InternalDsl.g:423:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalDsl.g:429:1: ruleAction returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'POKE' | kw= 'EMAIL' ) ;
    public final AntlrDatatypeRuleToken ruleAction() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDsl.g:435:2: ( (kw= 'POKE' | kw= 'EMAIL' ) )
            // InternalDsl.g:436:2: (kw= 'POKE' | kw= 'EMAIL' )
            {
            // InternalDsl.g:436:2: (kw= 'POKE' | kw= 'EMAIL' )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==12) ) {
                alt16=1;
            }
            else if ( (LA16_0==13) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalDsl.g:437:3: kw= 'POKE'
                    {
                    kw=(Token)match(input,12,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getActionAccess().getPOKEKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDsl.g:443:3: kw= 'EMAIL'
                    {
                    kw=(Token)match(input,13,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getActionAccess().getEMAILKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleOpBinary"
    // InternalDsl.g:452:1: entryRuleOpBinary returns [String current=null] : iv_ruleOpBinary= ruleOpBinary EOF ;
    public final String entryRuleOpBinary() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpBinary = null;


        try {
            // InternalDsl.g:452:48: (iv_ruleOpBinary= ruleOpBinary EOF )
            // InternalDsl.g:453:2: iv_ruleOpBinary= ruleOpBinary EOF
            {
             newCompositeNode(grammarAccess.getOpBinaryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOpBinary=ruleOpBinary();

            state._fsp--;

             current =iv_ruleOpBinary.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpBinary"


    // $ANTLR start "ruleOpBinary"
    // InternalDsl.g:459:1: ruleOpBinary returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'AND' | kw= 'OR' ) ;
    public final AntlrDatatypeRuleToken ruleOpBinary() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDsl.g:465:2: ( (kw= 'AND' | kw= 'OR' ) )
            // InternalDsl.g:466:2: (kw= 'AND' | kw= 'OR' )
            {
            // InternalDsl.g:466:2: (kw= 'AND' | kw= 'OR' )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==14) ) {
                alt17=1;
            }
            else if ( (LA17_0==15) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalDsl.g:467:3: kw= 'AND'
                    {
                    kw=(Token)match(input,14,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpBinaryAccess().getANDKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDsl.g:473:3: kw= 'OR'
                    {
                    kw=(Token)match(input,15,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpBinaryAccess().getORKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpBinary"


    // $ANTLR start "entryRuleOpUnary"
    // InternalDsl.g:482:1: entryRuleOpUnary returns [String current=null] : iv_ruleOpUnary= ruleOpUnary EOF ;
    public final String entryRuleOpUnary() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpUnary = null;


        try {
            // InternalDsl.g:482:47: (iv_ruleOpUnary= ruleOpUnary EOF )
            // InternalDsl.g:483:2: iv_ruleOpUnary= ruleOpUnary EOF
            {
             newCompositeNode(grammarAccess.getOpUnaryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOpUnary=ruleOpUnary();

            state._fsp--;

             current =iv_ruleOpUnary.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpUnary"


    // $ANTLR start "ruleOpUnary"
    // InternalDsl.g:489:1: ruleOpUnary returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'NOT' ;
    public final AntlrDatatypeRuleToken ruleOpUnary() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDsl.g:495:2: (kw= 'NOT' )
            // InternalDsl.g:496:2: kw= 'NOT'
            {
            kw=(Token)match(input,16,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getOpUnaryAccess().getNOTKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpUnary"


    // $ANTLR start "entryRuleOpStrCompare"
    // InternalDsl.g:504:1: entryRuleOpStrCompare returns [String current=null] : iv_ruleOpStrCompare= ruleOpStrCompare EOF ;
    public final String entryRuleOpStrCompare() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpStrCompare = null;


        try {
            // InternalDsl.g:504:52: (iv_ruleOpStrCompare= ruleOpStrCompare EOF )
            // InternalDsl.g:505:2: iv_ruleOpStrCompare= ruleOpStrCompare EOF
            {
             newCompositeNode(grammarAccess.getOpStrCompareRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOpStrCompare=ruleOpStrCompare();

            state._fsp--;

             current =iv_ruleOpStrCompare.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpStrCompare"


    // $ANTLR start "ruleOpStrCompare"
    // InternalDsl.g:511:1: ruleOpStrCompare returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '==' | kw= 'contains' ) ;
    public final AntlrDatatypeRuleToken ruleOpStrCompare() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDsl.g:517:2: ( (kw= '==' | kw= 'contains' ) )
            // InternalDsl.g:518:2: (kw= '==' | kw= 'contains' )
            {
            // InternalDsl.g:518:2: (kw= '==' | kw= 'contains' )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==17) ) {
                alt18=1;
            }
            else if ( (LA18_0==18) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalDsl.g:519:3: kw= '=='
                    {
                    kw=(Token)match(input,17,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpStrCompareAccess().getEqualsSignEqualsSignKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDsl.g:525:3: kw= 'contains'
                    {
                    kw=(Token)match(input,18,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpStrCompareAccess().getContainsKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpStrCompare"


    // $ANTLR start "entryRuleOpFloatCompare"
    // InternalDsl.g:534:1: entryRuleOpFloatCompare returns [String current=null] : iv_ruleOpFloatCompare= ruleOpFloatCompare EOF ;
    public final String entryRuleOpFloatCompare() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpFloatCompare = null;


        try {
            // InternalDsl.g:534:54: (iv_ruleOpFloatCompare= ruleOpFloatCompare EOF )
            // InternalDsl.g:535:2: iv_ruleOpFloatCompare= ruleOpFloatCompare EOF
            {
             newCompositeNode(grammarAccess.getOpFloatCompareRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOpFloatCompare=ruleOpFloatCompare();

            state._fsp--;

             current =iv_ruleOpFloatCompare.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpFloatCompare"


    // $ANTLR start "ruleOpFloatCompare"
    // InternalDsl.g:541:1: ruleOpFloatCompare returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '>' | kw= '<' | kw= '>=' | kw= '<=' | kw= '==' ) ;
    public final AntlrDatatypeRuleToken ruleOpFloatCompare() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDsl.g:547:2: ( (kw= '>' | kw= '<' | kw= '>=' | kw= '<=' | kw= '==' ) )
            // InternalDsl.g:548:2: (kw= '>' | kw= '<' | kw= '>=' | kw= '<=' | kw= '==' )
            {
            // InternalDsl.g:548:2: (kw= '>' | kw= '<' | kw= '>=' | kw= '<=' | kw= '==' )
            int alt19=5;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt19=1;
                }
                break;
            case 20:
                {
                alt19=2;
                }
                break;
            case 21:
                {
                alt19=3;
                }
                break;
            case 22:
                {
                alt19=4;
                }
                break;
            case 17:
                {
                alt19=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // InternalDsl.g:549:3: kw= '>'
                    {
                    kw=(Token)match(input,19,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getGreaterThanSignKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDsl.g:555:3: kw= '<'
                    {
                    kw=(Token)match(input,20,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getLessThanSignKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalDsl.g:561:3: kw= '>='
                    {
                    kw=(Token)match(input,21,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getGreaterThanSignEqualsSignKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalDsl.g:567:3: kw= '<='
                    {
                    kw=(Token)match(input,22,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getLessThanSignEqualsSignKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalDsl.g:573:3: kw= '=='
                    {
                    kw=(Token)match(input,17,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpFloatCompareAccess().getEqualsSignEqualsSignKeyword_4());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpFloatCompare"


    // $ANTLR start "entryRulePropertyNameOrVal"
    // InternalDsl.g:582:1: entryRulePropertyNameOrVal returns [String current=null] : iv_rulePropertyNameOrVal= rulePropertyNameOrVal EOF ;
    public final String entryRulePropertyNameOrVal() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulePropertyNameOrVal = null;


        try {
            // InternalDsl.g:582:57: (iv_rulePropertyNameOrVal= rulePropertyNameOrVal EOF )
            // InternalDsl.g:583:2: iv_rulePropertyNameOrVal= rulePropertyNameOrVal EOF
            {
             newCompositeNode(grammarAccess.getPropertyNameOrValRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePropertyNameOrVal=rulePropertyNameOrVal();

            state._fsp--;

             current =iv_rulePropertyNameOrVal.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyNameOrVal"


    // $ANTLR start "rulePropertyNameOrVal"
    // InternalDsl.g:589:1: rulePropertyNameOrVal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES | this_SNGLQUOTED_NON_EMPTY_SEQ_1= RULE_SNGLQUOTED_NON_EMPTY_SEQ ) ;
    public final AntlrDatatypeRuleToken rulePropertyNameOrVal() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0=null;
        Token this_SNGLQUOTED_NON_EMPTY_SEQ_1=null;


        	enterRule();

        try {
            // InternalDsl.g:595:2: ( (this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES | this_SNGLQUOTED_NON_EMPTY_SEQ_1= RULE_SNGLQUOTED_NON_EMPTY_SEQ ) )
            // InternalDsl.g:596:2: (this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES | this_SNGLQUOTED_NON_EMPTY_SEQ_1= RULE_SNGLQUOTED_NON_EMPTY_SEQ )
            {
            // InternalDsl.g:596:2: (this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES | this_SNGLQUOTED_NON_EMPTY_SEQ_1= RULE_SNGLQUOTED_NON_EMPTY_SEQ )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES) ) {
                alt20=1;
            }
            else if ( (LA20_0==RULE_SNGLQUOTED_NON_EMPTY_SEQ) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalDsl.g:597:3: this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0= RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES
                    {
                    this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0=(Token)match(input,RULE_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES,FOLLOW_2); 

                    			current.merge(this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0);
                    		

                    			newLeafNode(this_SEQ_NO_WS_NO_QUOTES_NO_PARENTHESES_0, grammarAccess.getPropertyNameOrValAccess().getSEQ_NO_WS_NO_QUOTES_NO_PARENTHESESTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDsl.g:605:3: this_SNGLQUOTED_NON_EMPTY_SEQ_1= RULE_SNGLQUOTED_NON_EMPTY_SEQ
                    {
                    this_SNGLQUOTED_NON_EMPTY_SEQ_1=(Token)match(input,RULE_SNGLQUOTED_NON_EMPTY_SEQ,FOLLOW_2); 

                    			current.merge(this_SNGLQUOTED_NON_EMPTY_SEQ_1);
                    		

                    			newLeafNode(this_SNGLQUOTED_NON_EMPTY_SEQ_1, grammarAccess.getPropertyNameOrValAccess().getSNGLQUOTED_NON_EMPTY_SEQTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyNameOrVal"

    // Delegated rules


    protected DFA13 dfa13 = new DFA13(this);
    static final String dfa_1s = "\5\uffff";
    static final String dfa_2s = "\1\21\1\4\2\uffff\1\4";
    static final String dfa_3s = "\1\26\1\4\2\uffff\1\10";
    static final String dfa_4s = "\2\uffff\1\1\1\2\1\uffff";
    static final String dfa_5s = "\5\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\1\2\4\3",
            "\1\4",
            "",
            "",
            "\1\4\1\uffff\1\2\1\3\1\2"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA13 extends DFA {

        public DFA13(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 13;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "308:3: ( ( ( (lv_opStrComp_9_0= ruleOpStrCompare ) ) (this_WS_10= RULE_WS )+ ( (lv_propValue_11_0= rulePropertyNameOrVal ) ) ) | ( ( (lv_opFloatComp_12_0= ruleOpFloatCompare ) ) (this_WS_13= RULE_WS )+ ( (lv_propValue_14_0= RULE_FLOAT ) ) ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000210L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000410L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000000F000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000150L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000007F0010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000810L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000090L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000012L});

}