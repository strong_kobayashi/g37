package sendTestStr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Send {

	public static void main(String[] args) {
		// This method sends a string to the device as identified by its device account.
			String reply = null;
			String host = "127.0.0.1";
			int portNumber = 8999;
			String strToSend = "";
			
			for (String s : args) strToSend = strToSend + "\"" + s + "\"";
			System.out.println(strToSend);
			
			try (
		            Socket socket = new Socket(host, portNumber);
		            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
		            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		        ) {
		                out.println(strToSend);
		                reply = in.readLine();
		        } catch (UnknownHostException e) {
		            reply = "Error - host not found, " + e.getMessage();
		        } catch (IOException e) {
		            reply = "Error - no connection, " + e.getMessage();
		        } 
			System.out.println("Reply: "+reply);
		}
}
