/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.ServiceProvider;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Service Provider</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ServiceProviderTest extends UserAccountTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ServiceProviderTest.class);
	}

	/**
	 * Constructs a new Service Provider test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceProviderTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Service Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ServiceProvider getFixture() {
		return (ServiceProvider)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(G37_ControllerFactory.eINSTANCE.createServiceProvider());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ServiceProviderTest
