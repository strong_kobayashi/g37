/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.DeviceAccountsManager;
import G37.G37_Controller.G37_ControllerFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Device Accounts Manager</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeviceAccountsManagerTest extends TestCase {

	/**
	 * The fixture for this Device Accounts Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceAccountsManager fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DeviceAccountsManagerTest.class);
	}

	/**
	 * Constructs a new Device Accounts Manager test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceAccountsManagerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Device Accounts Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DeviceAccountsManager fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Device Accounts Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceAccountsManager getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(G37_ControllerFactory.eINSTANCE.createDeviceAccountsManager());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DeviceAccountsManagerTest
