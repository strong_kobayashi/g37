/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.SupplierSubscription;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Supplier Subscription</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SupplierSubscriptionTest extends GenericSubscriptionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SupplierSubscriptionTest.class);
	}

	/**
	 * Constructs a new Supplier Subscription test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SupplierSubscriptionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Supplier Subscription test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SupplierSubscription getFixture() {
		return (SupplierSubscription)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(G37_ControllerFactory.eINSTANCE.createSupplierSubscription());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SupplierSubscriptionTest
