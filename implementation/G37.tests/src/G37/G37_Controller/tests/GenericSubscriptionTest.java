/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.GenericSubscription;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Generic Subscription</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class GenericSubscriptionTest extends TestCase {

	/**
	 * The fixture for this Generic Subscription test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GenericSubscription fixture = null;

	/**
	 * Constructs a new Generic Subscription test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericSubscriptionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Generic Subscription test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(GenericSubscription fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Generic Subscription test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GenericSubscription getFixture() {
		return fixture;
	}

} //GenericSubscriptionTest
