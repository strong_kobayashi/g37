/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.G37C;
import G37.G37_Controller.G37_ControllerFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>G37C</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class G37CTest extends TestCase {

	/**
	 * The fixture for this G37C test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected G37C fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(G37CTest.class);
	}

	/**
	 * Constructs a new G37C test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public G37CTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this G37C test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(G37C fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this G37C test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected G37C getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(G37_ControllerFactory.eINSTANCE.createG37C());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //G37CTest
