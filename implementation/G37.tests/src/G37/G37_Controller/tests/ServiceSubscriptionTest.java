/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.ServiceSubscription;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Service Subscription</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ServiceSubscriptionTest extends GenericSubscriptionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ServiceSubscriptionTest.class);
	}

	/**
	 * Constructs a new Service Subscription test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceSubscriptionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Service Subscription test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ServiceSubscription getFixture() {
		return (ServiceSubscription)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(G37_ControllerFactory.eINSTANCE.createServiceSubscription());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ServiceSubscriptionTest
