/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.UserAccount;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>User Account</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class UserAccountTest extends TestCase {

	/**
	 * The fixture for this User Account test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserAccount fixture = null;

	/**
	 * Constructs a new User Account test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserAccountTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this User Account test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UserAccount fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this User Account test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserAccount getFixture() {
		return fixture;
	}

} //UserAccountTest
