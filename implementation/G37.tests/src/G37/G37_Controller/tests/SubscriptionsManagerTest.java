/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.G37_ControllerFactory;
import G37.G37_Controller.SubscriptionsManager;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Subscriptions Manager</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SubscriptionsManagerTest extends TestCase {

	/**
	 * The fixture for this Subscriptions Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubscriptionsManager fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SubscriptionsManagerTest.class);
	}

	/**
	 * Constructs a new Subscriptions Manager test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubscriptionsManagerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Subscriptions Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(SubscriptionsManager fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Subscriptions Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubscriptionsManager getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(G37_ControllerFactory.eINSTANCE.createSubscriptionsManager());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SubscriptionsManagerTest
