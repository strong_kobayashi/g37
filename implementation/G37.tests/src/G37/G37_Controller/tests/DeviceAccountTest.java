/**
 */
package G37.G37_Controller.tests;

import G37.G37_Controller.DeviceAccount;
import G37.G37_Controller.G37_ControllerFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Device Account</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeviceAccountTest extends TestCase {

	/**
	 * The fixture for this Device Account test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceAccount fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DeviceAccountTest.class);
	}

	/**
	 * Constructs a new Device Account test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceAccountTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Device Account test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DeviceAccount fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Device Account test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceAccount getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(G37_ControllerFactory.eINSTANCE.createDeviceAccount());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DeviceAccountTest
